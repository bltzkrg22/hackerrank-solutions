# HackerRank Solutions

While there are no direct links to problem descripions, the appropriate link can be inferred from the filename (which was automatically generated by HackerRank). Examples:

[`bltzkrg22_the-quickest-way-up_solution.cs`](https://gitlab.com/bltzkrg.22/hackerrank-solutions/-/blob/main/Solutions/bltzkrg22_the-quickest-way-up_solution.cs) leads to: [`https://www.hackerrank.com/challenges/the-quickest-way-up`](https://www.hackerrank.com/challenges/the-quickest-way-up)  
[`bltzkrg22_journey-to-the-moon_solution.cs`](https://gitlab.com/bltzkrg.22/hackerrank-solutions/-/blob/main/Solutions/bltzkrg22_journey-to-the-moon_solution.cs) leads to: [`https://www.hackerrank.com/challenges/journey-to-the-moon`](https://www.hackerrank.com/challenges/journey-to-the-moon)

Or just use the search bar on <https://www.hackerrank.com/dashboard>, and try to guess the problem name from the source code filename. It *usually* works.
