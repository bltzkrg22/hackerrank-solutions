using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;


/*
* This solution is suboptimal and times out on the most complex test cases!
*/

class Result
{
    static HashSet<string> failedCandidates = new HashSet<string>();
    public static string abbreviation(string a, string b)
    {
        failedCandidates = new HashSet<string>();
        var aList = a.ToList();
        bool status = RecursiveRoutine(aList, b);

        return status ? "YES" : "NO";
    }


    public static bool RecursiveRoutine(List<char> mutated, string immobile)
    {
        if (failedCandidates.Contains(CharListToString(mutated)))
        {
            return false;
        }

        bool success = false;
        int firstLowercase = FirstLowercaseCharacterIndex(mutated);

        if (mutated.Count < immobile.Length)
        {
            success = false;
        }
        else if (mutated.Count == immobile.Length)
        {
            success = (CharListToString(mutated).ToUpper() == immobile) ? true : false;
        }
        else if (firstLowercase == -1)
        {
            success = (CharListToString(mutated) == immobile) ? true : false;
        }
        else if (firstLowercase > 0 && firstLowercase > immobile.Length)
        {
            success = false;
        }
        else if (firstLowercase > 0 && (immobile.Substring(0, firstLowercase) != CharListToString(mutated.GetRange(0, firstLowercase))))
        {
            success = false;
        }
        else
        {
            success = RecursiveRoutine(CapitalizeFirstLowercase(mutated), immobile) ||
                        RecursiveRoutine(RemoveFirstLowercase(mutated), immobile);
        }

        if (success == false)
        {
            failedCandidates.Add(CharListToString(mutated));
        }

        return success;
    }

    public static List<char> RemoveFirstLowercase(List<char> input)
    {
        var output = new List<char>(input);

        output.RemoveAt(FirstLowercaseCharacterIndex(input));

        return output;
    }

    public static List<char> CapitalizeFirstLowercase(List<char> input)
    {
        var output = new List<char>(input);

        output[FirstLowercaseCharacterIndex(input)] = Char.ToUpper(input[FirstLowercaseCharacterIndex(input)]);

        return output;
    }


    public static int FirstLowercaseCharacterIndex(List<char> input)
    {
        for (int i = 0; i < input.Count; i++)
        {
            if (Char.IsLower(input[i]))
            {
                return i;
            }
        }

        // Return -1 if string contains no lowercase letters
        return -1;
    }

    public static string CharListToString(List<char> input)
    {
        var output = new StringBuilder();
        foreach (var character in input)
        {
            output.Append(character);
        }
        return output.ToString();
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            string a = Console.ReadLine();

            string b = Console.ReadLine();

            string result = Result.abbreviation(a, b);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
