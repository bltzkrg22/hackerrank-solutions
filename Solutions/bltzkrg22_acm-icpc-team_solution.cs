using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static List<int> acmTeam(List<string> topic)
    {
        int maxScore = int.MinValue;
        int maxFrequency = 0;

        for (int i = 0; i < topic.Count; i++)
        {
            for (int j = i + 1; j < topic.Count; j++)
            {
                int currentTeamScore = CalculateScore(MergeAttendees(topic[i], topic[j]));
                if (currentTeamScore > maxScore)
                {
                    maxScore = currentTeamScore;
                    maxFrequency = 1;
                }
                else if (currentTeamScore == maxScore)
                {
                    maxFrequency += 1;
                }
            }
        }

        return new List<int>(){ maxScore, maxFrequency };
    }

    private static int CalculateScore(string scoreAsString)
    {
        int score = 0;
        foreach (var character in scoreAsString)
        {
            if (character == '1')
            {
                score++;
            }
        }
        return score;
    }

    private static string MergeAttendees(string attendeeA, string attendeeB)
    {
        // merging to a BitArray instead of a string could probably be faster, but whatever
        var merged = new StringBuilder(attendeeA.Length);

        for (int i = 0; i < attendeeA.Length; i++)
        {
            if (attendeeA[i] == '1' || attendeeB[i] == '1')
            {
                merged.Append('1');
            }
            else
            {
                merged.Append('0');
            }
        }

        return merged.ToString();
    }
}


class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int m = Convert.ToInt32(firstMultipleInput[1]);

        List<string> topic = new List<string>();

        for (int i = 0; i < n; i++)
        {
            string topicItem = Console.ReadLine();
            topic.Add(topicItem);
        }

        List<int> result = Result.acmTeam(topic);

        textWriter.WriteLine(String.Join("\n", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
