using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    const int OverMaxElement = 1000001;
    const int OverMinElement = -1;
    public static void almostSorted(List<int> arr)
    {
        /*
        * From data constraints we know that elements in arr are unique!
        */

        /*
        * (Step 0) Add sentinels to the input array. Note that answer assumes that array is one-indexed, so after adding the left
        * sentinel the actual index of element in the array is the same as the logical index expected on the output, no -1 offset!
        */

        arr.Insert(0, OverMinElement);
        arr.Add(OverMaxElement);

        /*
        * (Step 1a) Find the first element from left that is out of order. The left one is lower than
        * the lowest possible number in arr, and the right one is greater than greatest possible number in arr.
        */

        int leftOutOfOrder = -1;
        int rightOutOfOrder = arr.Count;

        for (int i = 1; i < arr.Count; i++)
        {
            if (arr[i] < arr[i-1])
            {
                leftOutOfOrder = i;
                break;
            }
        }

        // If array is already sorted, we print "yes".
        if (leftOutOfOrder == -1)
        {
            Console.WriteLine("yes");
            return;
        }

        /*
        * (Step 1b) Find the first element from right that is out of order.
        */
        for (int i = arr.Count - 2; i >= 0; i--)
        {
            if (arr[i] > arr[i+1])
            {
                rightOutOfOrder = i ;
                break;
            }
        }

        /*
        * (Step 2.-1) Degenerative case - leftOutOfOrder - rightOutOfOrder = -1. Notice the unintuitive overlap!
        * Consider five elements: a < b > c < d < e, where b > c is the only pair out of order.
        * Here: c => leftOutOfOrder, b => rightOutOfOrder!
        *
        * We can only try to swap pair (b,c). If we swap (b,d) or (b,e), than we will put d or e before c. If we swap (a,c),
        * then we will have b before a.
        *
        * So we swap (b,c) and check if a < c < b < d.
        */

        if ( leftOutOfOrder == rightOutOfOrder + 1 )
        {
            // Check if a < c < b < d
            if ( arr[rightOutOfOrder-1] < arr[leftOutOfOrder]  && arr[rightOutOfOrder] < arr[leftOutOfOrder+1])
            {
                Console.WriteLine("yes");
                Console.WriteLine($"swap {rightOutOfOrder} {leftOutOfOrder}");
                return;
            }
            else
            {
                Console.WriteLine("no");
                return;
            }
        }

        /*
        * (Step 2.0) Degenerative case - leftOutOfOrder = rightOutOfOrder. Consider five elements: a < b > c > d < e, where c
        * is the only element out of order. We will check three possible reorderings of (b, c, d). If one of them is correct,
        * then array is sortable.
        */

        if ( leftOutOfOrder == rightOutOfOrder )
        {
            // Case a, c, b, d, e after swap
            if (arr[leftOutOfOrder] < arr[leftOutOfOrder-1] && arr[leftOutOfOrder-1] < arr[leftOutOfOrder+1])
            {
                Console.WriteLine("yes");
                Console.WriteLine($"swap {leftOutOfOrder-1} {leftOutOfOrder}");
                return;
            }
            // Case a, b, d, c, e after swap
            else if (arr[leftOutOfOrder-1] < arr[leftOutOfOrder+1] && arr[leftOutOfOrder+1] < arr[leftOutOfOrder])
            {
                Console.WriteLine("yes");
                Console.WriteLine($"swap {leftOutOfOrder} {leftOutOfOrder+1}");
                return;
            }
            // Case a, d, c, b, e after swap
            else if (arr[leftOutOfOrder+1] < arr[leftOutOfOrder] && arr[leftOutOfOrder] < arr[leftOutOfOrder-1])
            {
                Console.WriteLine("yes");
                Console.WriteLine($"swap {leftOutOfOrder-1} {leftOutOfOrder+1}");
                return;
            }
            else
            {
                Console.WriteLine("no");
                return;
            }
        }


        /*
        * (Step 2.1) Case rightOutOfOrder - leftOutOfOrder == 1. Then we have six elements: a < b > c ? d > e < f, where
        * c is the first left out of order, and d is the first right.
        *
        * If c > d, i.e. b > c > d > e, then we can reverse whole segment (b,c,d,e) and get a sorted array.
        - A single swap would could never correct the order; note that we a swap must have at least one element from each pair:
        * (b,c), (b,d), (b,e), (c,d), (c,e) & (d,e), since we need to break all those wrong orders. It's easy to see that
        we can't find such two elements.
        *
        * If c < d, i.e. b > c < d > e, then reversing becomes impossible, since after reversing d > c. We can't swap (b,c), since
        * it leaves unsorted pair d > e; similarly we can'y swap (d,e). We can't swap (b,d), since it would put d in front of c, 
        * and d > c. We can't swap (c,e). So the only possible swap is (b,e), but then we have to check that e < c < d < b!
        */

        if ( leftOutOfOrder == rightOutOfOrder - 1 )
        {
            // Case b > c < d > e before swap
            if (arr[leftOutOfOrder] < arr[rightOutOfOrder] )
            {
                // Check if e < c < d < b
                if (arr[rightOutOfOrder+1] < arr[leftOutOfOrder] && arr[leftOutOfOrder-1] < arr[rightOutOfOrder])
                {
                    Console.WriteLine("yes");
                    Console.WriteLine($"swap {leftOutOfOrder-1} {rightOutOfOrder+1}");
                    return;
                }
                else
                {
                    Console.WriteLine("no");
                    return;
                }
            }
            // Case b > c > d > e before swap, e < d < c < b after swap
            else // if (arr[leftOutOfOrder] > arr[rightOutOfOrder] )
            {
                Console.WriteLine("yes");
                Console.WriteLine($"reverse {leftOutOfOrder-1} {rightOutOfOrder+1}");
                return;
            }
        }

                /*
        * (Step 2.2) Case rightOutOfOrder - leftOutOfOrder > 1. Here we have multiple elements: a < b > c ? ... ? d > e < f,
        * again c is the first left out of order, and d is the first right.
        *
        * If c > d, i.e. b > c > d > e, then again the only possibility is to reverse whole segment (b, c, ..., d, e). The logic
        * is the same as in Step 2.1. Here, we need to check that before reversing whole segment was in reverse order.
        *
        * If c < d, i.e. b > c < d > e, then again reversing is impossible, since after reversing d > c. Again, we can only swap
        * pair (b,e), and then we have to check that if the segment (e, c, ..., d, b) is ordered.!
        */

        // if ( leftOutOfOrder < rightOutOfOrder - 1 ) // This is the only possible outcome left.
        
        // Case b > c < d > e before swap
        if (arr[leftOutOfOrder] < arr[rightOutOfOrder] )
        {
            // Swap b and e and check if array is in order

            int buffer = arr[leftOutOfOrder - 1]; // b => buffer
            arr[leftOutOfOrder - 1] = arr[rightOutOfOrder + 1];
            arr[rightOutOfOrder + 1] = buffer;
            bool isOrdered = true;

            for (int i = leftOutOfOrder - 1; i < rightOutOfOrder + 1; i++)
            {
                if (arr[i] > arr[i+1])
                {
                    isOrdered = false;
                    break;
                }
            }

            if (isOrdered == true)
            {
                Console.WriteLine("yes");
                Console.WriteLine($"swap {leftOutOfOrder-1} {rightOutOfOrder+1}");
                return;
            }
            else
            {
                Console.WriteLine("no");
                return;
            }
        }
        // Case b > c > d > e, co we can only reverse!
        else // if (arr[leftOutOfOrder] > arr[rightOutOfOrder] )
        {
            bool isReverseOrdered = true;

            for (int i = leftOutOfOrder - 1; i < rightOutOfOrder + 1; i++)
            {
                if (arr[i] < arr[i+1])
                {
                    isReverseOrdered = false;
                    break;
                }
            }

            if (isReverseOrdered == true)
            {
                Console.WriteLine("yes");
                Console.WriteLine($"reverse {leftOutOfOrder-1} {rightOutOfOrder+1}");
                return;
            }
            else
            {
                Console.WriteLine("no");
                return;
            }
        }
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        Result.almostSorted(arr);
    }
}
