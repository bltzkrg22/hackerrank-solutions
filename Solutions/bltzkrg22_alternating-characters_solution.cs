using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'alternatingCharacters' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING s as parameter.
     */

    public static int alternatingCharacters(string s)
    {
        int deletionsRequired = 0;
        char currentChar = s[0];

        for (int i = 1; i < s.Length; i++)
        {
            if ( s[i] == currentChar )
            {
                deletionsRequired++;
            }
            else
            {
                currentChar = ToggleChar(currentChar);
            }
        }

        return deletionsRequired;
    }

    // if char is 'A', return 'B'
    // otherwise assume that char is 'B', and return 'A"
    private static char ToggleChar(char c) => c == 'A' ? 'B' : 'A';

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            string s = Console.ReadLine();

            int result = Result.alternatingCharacters(s);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
