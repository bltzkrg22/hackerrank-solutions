using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * Imagine that each number arr[i] from array is a building with height i.
    * Number of rounds played will be the same as number of building that you can see
    * from the beginning - imagine that a higher building obscures all lower ones
    * that stand behind it.
    * Example, [5,2,6,3,4]
    *
    *                    []
    *              []    []      
    *              []    []    []
    *              []    [] [] []
    *              [] [] [] [] []
    * beginning => [] [] [] [] [] 
    * 
    * Here we can see two buildings, first with height 5 and second with height 6.
    * There will be two rounds played.
    *
    * If number of rounds is even, Andy wins, if it's odd - Bob wins.
    */
    
    public static string gamingArray(List<int> arr)
    {
        int currentMax = arr[0];
        int counter = 1;

        for (int i = 0; i < arr.Count; i++)
        {
            if (arr[i] > currentMax)
            {
                counter++;
                currentMax = arr[i];
            }
        }

        return counter % 2 == 0 ? "ANDY" : "BOB";
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int g = Convert.ToInt32(Console.ReadLine().Trim());

        for (int gItr = 0; gItr < g; gItr++)
        {
            int arrCount = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

            string result = Result.gamingArray(arr);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
