using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static int anagram(string s)
    {
        // If lenght of s is not even, there is no solution
        // and we return -1.
        if (s.Length % 2 != 0)
        {
            return -1;
        }

        string first = s.Substring(0, s.Length / 2);
        string second = s.Substring(s.Length / 2, s.Length / 2);

        var characterFrequencies = new Dictionary<char, int>();

        // If a character is present in the first half (first word), we increment
        // its frequency in `characterFrequencies`
        foreach (var character in first)
        {
            if (characterFrequencies.ContainsKey(character))
            {
                characterFrequencies[character] += 1;
            }
            else
            {
                 characterFrequencies[character] = 1;
            }
        }

        // If a character is present in the second half, we decrement
        // its frequency in `characterFrequencies`
        foreach (var character in second)
        {
            if (characterFrequencies.ContainsKey(character))
            {
                characterFrequencies[character] += -1;
            }
            else
            {
                characterFrequencies[character] = -1;
            }
        }

        /*
        * At this point, if a value for some key (some chracter) in `characterFrequencies` is
        * a positive integer k, then such character occurs more times in the first word
        * (and analogously, if some value negative, then a character occurs more times is the 
        * second word).
        * 
        * If we then sum all positive values, then we will know how many characters in the first
        * word are not seen in the second word – if we change them all, we will get our anagram.
        */

        return characterFrequencies.Values.Where(freq => freq > 0).Sum();
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            string s = Console.ReadLine();

            int result = Result.anagram(s);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
