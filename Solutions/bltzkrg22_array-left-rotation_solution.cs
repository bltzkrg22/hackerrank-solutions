using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static List<int> rotateLeft(int rotation, List<int> arr)
    {
        var answer = new List<int>();
        for (int i = 0; i < arr.Count; i++)
        {
            answer.Add(arr[WrappedRotatedIndex(i, rotation, arr.Count)]);
        }

        return answer;
    }

    // Find the index, which after rotation will be moved to index from query
    // Example: [1,2,3,4,5] roated four times becomes [5,1,2,3,4], so
    // WrappedRotatedIndex(index:0, rotation:4, max:5) == 4
    // because the number that ends up at index 0 was originally at index 4;
    // WrappedRotatedIndex(index:3, rotation:4, max:5) == 2, etc.
    private static int WrappedRotatedIndex(int index, int rotation, int max)
    {
        index += rotation;
        index %= max;
        return index;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int d = Convert.ToInt32(firstMultipleInput[1]);

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        List<int> result = Result.rotateLeft(d, arr);

        textWriter.WriteLine(String.Join(" ", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
