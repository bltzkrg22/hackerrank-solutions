using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * The following properties will be declared as "global" for the Result class, so
    * that the Window class can just access them without passing them with parameters
    */

    /*
    * OverSteadyLetters are those letters, which are encountered in Gene more than n/4 times,
    * where n is Gene.Length. TotalOverSteadyCount is how many times those letters are encountered
    * over the expected, steady n/4 count. For example:
    *
    * If Gene = "GGAATAAA", then:
    * OverSteadyLetters = {'A'} - this is the only letter that is encountered more than 8/4 = 2 times
    * TotalOverSteadyCount = ('A', 3) - A is encoutered 5 times, which is 3 times more than 8/4 = 2
    *
    * If Gene = "TGATGCCGTCCCCTCAACTTGAGTGCTCCTAATGCGTTGC", then:
    * OverSteadyLetters = {'T', 'C'} - gene has 40 letters, 'C' is encountered 13 times, 'T' - 12 times
    * TotalOverSteadyCount = ('T', 3), ('C', 2)
    */
    internal static string Gene {get; set;}
    internal static HashSet<char> OverSteadyLetters = new HashSet<char>(); 
    internal static Dictionary<char, int> TotalOverSteadyCount {get; set;}



    /*
    * The optimal window that we will cut and replace has following properties:
    * a) it starts and ends with a letter from OverSteadyLetters
    * b) it contains exactly TotalOverSteadyCount letters from OverSteadyLetters
    *
    * However, it is possible that Gene does not contain a consecutive substring of length TotalOverSteadyCount
    * with the optimal count for OverSteadyLetters. Then we find the first window that satisfies the requirements.
    */

    internal class Window
    {
        internal int WindowStartIndex {get; set;}
        internal int WindowEndIndex {get; set;}
        internal int WindowWidth() => WindowEndIndex - WindowStartIndex + 1;

        private Dictionary<char, int> windowOverSteadyCount;
        internal Dictionary<char, int> GetWindowOverSteadyCount => windowOverSteadyCount;
        

        internal bool ShiftWindowStart()
        {
            char startLetter = Gene[WindowStartIndex];
            char nextLetter;
            int shiftOffset = 1;

            while (WindowStartIndex + shiftOffset <= Gene.Length - 1)
            {
                nextLetter = Gene[WindowStartIndex + shiftOffset];
                if (OverSteadyLetters.Contains(nextLetter))
                {
                    WindowStartIndex += shiftOffset;
                    windowOverSteadyCount[startLetter] -= 1;
                    return true;
                }
                else
                {
                    shiftOffset++;
                }
            }

            return false;
        }

        internal bool ShiftWindowEnd()
        {
            char nextLetter;
            int shiftOffset = 1;

            while (WindowEndIndex + shiftOffset <= Gene.Length - 1)
            {
                nextLetter = Gene[WindowEndIndex + shiftOffset];
                if (OverSteadyLetters.Contains(nextLetter))
                {
                    WindowEndIndex += shiftOffset;
                    windowOverSteadyCount[nextLetter] += 1;
                    return true;
                }
                else
                {
                    shiftOffset++;
                }
            }

            return false;
        }

        internal bool WindowContainsAllRequiredLetters()
        {
            bool condition = true;
            foreach (var letter in OverSteadyLetters)
            {
                if (windowOverSteadyCount[letter] < TotalOverSteadyCount[letter])
                {
                    condition = false;
                    break;
                }
            }

            return condition;
        }
        internal Window(int minWidth)
        {
            int firstOverSteadyIndex = 0;
            while (!OverSteadyLetters.Contains(Gene[firstOverSteadyIndex]))
            {
                firstOverSteadyIndex++;
            }

            WindowStartIndex = firstOverSteadyIndex;
            WindowEndIndex = WindowStartIndex + minWidth - 1;

            windowOverSteadyCount = new Dictionary<char, int>();
            foreach (var letter in OverSteadyLetters)
            {
                windowOverSteadyCount[letter] = 0;
            }     


            for (int i = WindowStartIndex; i <= WindowEndIndex; i++)
            {
                if (OverSteadyLetters.Contains(Gene[i]))
                {
                    windowOverSteadyCount[Gene[i]] += 1;
                }
            }
        }
    }

    public static int steadyGene(string gene)
    {
        Gene = gene;
        TotalOverSteadyCount = new Dictionary<char, int>();

        // Each letter is supposed to be encountered steadyNumber times.
        int steadyNumber = gene.Length / 4;

        var frequencyTable = new Dictionary<char, int>(){ {'A', 0}, {'C', 0}, {'G', 0}, {'T', 0} };

        // Count the frequency for each letter
        foreach (var character in gene)
        {
            frequencyTable[character] += 1;
        }

        // Find out which letters are encountered more that n/4 times. Having both a hashset and a dictionary
        // is a leftover from previous attempt, which I didn't fully refactor. Hashset unnecessarily duplicates the information.
        foreach (var pairLetterFreq in frequencyTable)
        {
            if (pairLetterFreq.Value > steadyNumber)
            {
                OverSteadyLetters.Add(pairLetterFreq.Key);
                TotalOverSteadyCount[pairLetterFreq.Key] = pairLetterFreq.Value - steadyNumber;
            }
        }

        // Maybe the gene is already steady?
        if (TotalOverSteadyCount.Count == 0)
        {
            return 0;
        }

        // Create an initial cutting window. We don't check if the window is actually valid at this point, i.e. if it includes
        // each letter from OverSteadyLetters at least required number of times.
        var cuttingWindow = new Window(TotalOverSteadyCount.Values.Sum());

        // Now we correct the window, so that in includes each letter from OverSteadyLetters at least required number of times.
        bool shiftStatus = true;
        while (!cuttingWindow.WindowContainsAllRequiredLetters() && shiftStatus == true)
        {
            shiftStatus = cuttingWindow.ShiftWindowEnd();
        }

        int currentWidth = cuttingWindow.WindowWidth();


        /*
        * Windows shifting algorithm:
        a) get a valid first window and remember its width (this is already done immediately before),
        b) shift the beginning; if new window is valid, check if its width is lower than current solution
        c) if shifting the beginning no longer gives a valid widnow, shift the end until the window is valid or end is reached
        d) return the soution
        */

        while (shiftStatus == true)
        {
            currentWidth = Math.Min(currentWidth, cuttingWindow.WindowWidth());

            // try shifting the start of window
            cuttingWindow.ShiftWindowStart();

            // check if window is valid, if it is try to save its width
            if (cuttingWindow.WindowContainsAllRequiredLetters())
            {
                currentWidth = Math.Min(currentWidth, cuttingWindow.WindowWidth());
            }
            // if window is invalid, shift its end until the window is valid or gene end is reached
            else
            {
                while (!cuttingWindow.WindowContainsAllRequiredLetters() && shiftStatus == true)
                {
                    shiftStatus = cuttingWindow.ShiftWindowEnd();
                }
            }
        }

        return currentWidth;
    }
}


class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        string gene = Console.ReadLine();

        int result = Result.steadyGene(gene);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
