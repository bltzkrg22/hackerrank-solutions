using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * We are looking for a minimum number of steps required to remove
    * all occurences of `010`. With a single step, we always break one ocurrence,
    * like that: `010` => `000`.
    * 
    * *Sometimes* we can break two; if the input string contains a substring `01010`,
    * then it is better to do: `01010` => `01110` instead of `01010` => `00010` => `00000`.
    * 
    * We also can never break more that two `010` blocks in one step. So we first break
    * all `01010` that we can, and than break any remaining `010`.
    */
    public static int beautifulBinaryString(string b)
    {
        int longMatchesCount = Regex.Matches(b, "01010").Count;

        string bIntermediate = Regex.Replace(b, "01010", "01110");

        int shortMatchesCount = Regex.Matches(bIntermediate, "010").Count;

        return longMatchesCount + shortMatchesCount;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        string b = Console.ReadLine();

        int result = Result.beautifulBinaryString(b);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
