using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static int beautifulPairs(List<int> A, List<int> B)
    {
        // Ideally I’d like to have some kind of multiset here, but a frequency
        // dictionary can do as well
        var BfreqDict = B.GroupBy(value => value)
                         .ToDictionary(grpng => grpng.Key, grpng => grpng.Count());

        bool unmatchedNumberInAExists = false;

        int beautifulPairCounter = 0;

        foreach (var numberFromA in A)
        {
            if (BfreqDict.ContainsKey(numberFromA) && BfreqDict[numberFromA] > 0)
            {
                beautifulPairCounter++;
                BfreqDict[numberFromA] -= 1;
            }
            else
            {
                unmatchedNumberInAExists = true;
            }
        }

        if (unmatchedNumberInAExists)
        {
            beautifulPairCounter++;
        }
        // We are *forced* to change one value in B. So if *all* numbers are matched,
        // we must break up one pair.
        else
        {
            beautifulPairCounter--;
        }


        return beautifulPairCounter;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> A = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(ATemp => Convert.ToInt32(ATemp)).ToList();

        List<int> B = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(BTemp => Convert.ToInt32(BTemp)).ToList();

        int result = Result.beautifulPairs(A, B);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
