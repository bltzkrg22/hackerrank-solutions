using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static int beautifulTriplets(int d, List<int> arr)
    {
        // In first pass, we will count the frequencies of all numbers
        var frequencyDictionary = arr.GroupBy(value => value)
                                     .ToDictionary(frq => frq.Key, frq => frq.Count());

        // In second pass, we will find all triplets. Note that a triple is found
        // always from the point of view of the middle number, so each triplet
        // will be counted only once.
        int tripletCounter = 0;

        for (int i = 0; i < arr.Count; i++)
        {
            // tripletCounter += frequencyDictionary.GetValueOrDefault(arr[i] - d)
            //                    * frequencyDictionary.GetValueOrDefault(arr[i] + d);

            bool lowerFound = frequencyDictionary.TryGetValue(arr[i] - d, out int lowerCount);
            bool higherFound = frequencyDictionary.TryGetValue(arr[i] + d, out int higherCount);

            if (lowerFound && higherFound)
            {
                tripletCounter += lowerCount * higherCount;
            }
        }

        return tripletCounter;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int d = Convert.ToInt32(firstMultipleInput[1]);

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        int result = Result.beautifulTriplets(d, arr);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
