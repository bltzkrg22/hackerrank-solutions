using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;


class Result
{
    // A simple data model - workaround for the old version of .NET on HackerRank.
    // In .NET 5 it should be possible to just use a (int sumOfDigits, int number) tuple
    private class MyTuple
    {
        public int SumOfDigits { get; set; }
        public int Number { get; set; }
    }
    public static int BestDivisor(int n)
    {
        // Generate a list of all divisors of n
        var divisors = Divisors(n);

        // For each divisor, calculate its sum of digits and pack the answer
        // to MyTuple object, and sort the objects
        var digitSums = divisors.Select(div => new MyTuple() { SumOfDigits = SumOfDigits(div), Number = div })
                                .OrderBy( pair => pair, new MyComparer() );

        // 
        return digitSums.First().Number;
    }

    // Sort the objects according to problem requirements, i.e.:
    // (1) first order by sum of digits descending
    // (2) in case of a tie, sort by number ascending
    private class MyComparer : Comparer<MyTuple>
    {
        public override int Compare(MyTuple A, MyTuple B)
        {
            if (A.SumOfDigits != B.SumOfDigits)
            {
                return B.SumOfDigits - A.SumOfDigits;
            }
            else
            {
                return A.Number - B.Number;
            }
        }
    }

    // Borrowed from https://rosettacode.org/wiki/Isqrt_(integer_square_root)_of_X#C.23
    // Calculates the largest integer that is equal to or lower than square root of number
    private static int IntegerSquareRoot(int number)
    {
        int q = 1;
        int r = 0;
        int t;

        while (q <= number)
        { 
            q <<= 2; 
        }

        while (q > 1)
        {
            q >>= 2;
            t = number - r - q;
            r >>= 1;
            if (t >= 0)
            { 
                number = t;
                r += q;
            }
        }
        return r;
    }

    private static List<int> Divisors(int number)
    {
        int intsqrt = IntegerSquareRoot(number);
        var firstHalf = Enumerable.Range(1, intsqrt)
                         .Where(divisor => number % divisor == 0).ToList();

        var secondHalf = firstHalf.Select(x => number / x).OrderBy(x => x).ToList();

        // Special case to avoid duplication of intsqrt in case input number
        // is a perferct square.
        if (intsqrt * intsqrt == number)
        {
            firstHalf.Remove(intsqrt);
        }

        firstHalf.AddRange(secondHalf);
        return firstHalf;
    }

    private static int SumOfDigits(int number)
    {
        int answer = 0;
        while (number > 0)
        {
            answer += number % 10;
            number /= 10;
        }
        return answer;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine().Trim());

        Console.WriteLine(Result.BestDivisor(n));
    }
}
