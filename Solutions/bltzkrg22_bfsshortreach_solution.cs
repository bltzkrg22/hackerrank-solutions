using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private const int EdgeDistance = 6;
    
    public static List<int> bfs(int n, int m, List<List<int>> edges, int s)
    {
        // Technically we want a SortedDictionary - but since we initialize the
        // values to -1, we can just insert the keys in sorted order
        Dictionary<int, List<int>> adjacencyList = new Dictionary<int,List<int>>();
        for (int i = 1; i <= n; i++)
        {
            adjacencyList[i] = new List<int>();
        }

        // We initialize all values to -1; we will overwrite if a node is reachable
        Dictionary<int, int> distanceToSelectedNode = new Dictionary<int,int>();

        for (int i = 1; i <= n; i++)
        {
            distanceToSelectedNode[i] = -1;
        }
        // Per problem description we omit the starting node from method output
        distanceToSelectedNode.Remove(s);


        for (int i = 0; i < edges.Count; i++)
        {
            adjacencyList[edges[i][0]].Add(edges[i][1]);
            adjacencyList[edges[i][1]].Add(edges[i][0]);
        }

        // We seed currentTurnNodes and alreadyVisitedNodes with starting node s
        HashSet<int> alreadyVisitedNodes = new HashSet<int>() { s };
        HashSet<int> currentTurnNodes = new HashSet<int>() { s };

        HashSet<int> nextTurnNodes;
        int currentTurnDistance = 0;

        while (currentTurnNodes.Count > 0)
        {
            nextTurnNodes = new HashSet<int>();

            foreach (var node in currentTurnNodes)
            {
                foreach (var neighbour in adjacencyList[node])
                {
                    if (!alreadyVisitedNodes.Contains(neighbour))
                    {
                        distanceToSelectedNode[neighbour] = EdgeDistance + currentTurnDistance;
                        alreadyVisitedNodes.Add(neighbour);
                        nextTurnNodes.Add(neighbour);
                    }
                }
            }

            currentTurnDistance += EdgeDistance;
            currentTurnNodes = nextTurnNodes;
        }

        return distanceToSelectedNode.Values.ToList();
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int n = Convert.ToInt32(firstMultipleInput[0]);

            int m = Convert.ToInt32(firstMultipleInput[1]);

            List<List<int>> edges = new List<List<int>>();

            for (int i = 0; i < m; i++)
            {
                edges.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(edgesTemp => Convert.ToInt32(edgesTemp)).ToList());
            }

            int s = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> result = Result.bfs(n, m, edges, s);

            textWriter.WriteLine(String.Join(" ", result));
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
