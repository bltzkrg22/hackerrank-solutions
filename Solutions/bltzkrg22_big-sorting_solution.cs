using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static List<string> bigSorting(List<string> unsorted)
    {
        unsorted.Sort(new NumberAsStringComparer());
        return unsorted;
    }

    private class NumberAsStringComparer : Comparer<string>
    {
        public override int Compare(string A, string B)
        {
            if (A.Length != B.Length)
            {
                return A.Length - B.Length;
            }
            else
            {
                for (int i = 0; i < A.Length ; i++)
                {
                    if (A[i] != B[i])
                    {
                        // Note that we subtract chars, but the difference will actually
                        // be the same as if we subtracted actual numbers
                        return A[i] - B[i];
                    }
                }
                return 0;
            }
        }
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<string> unsorted = new List<string>();

        for (int i = 0; i < n; i++)
        {
            string unsortedItem = Console.ReadLine();
            unsorted.Add(unsortedItem);
        }

        List<string> result = Result.bigSorting(unsorted);

        textWriter.WriteLine(String.Join("\n", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
