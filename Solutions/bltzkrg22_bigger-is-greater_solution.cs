using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'biggerIsGreater' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING w as parameter.
     */

    public static string biggerIsGreater(string w)
    {
        var newWord = new StringBuilder(w.Length);
        int index = w.Length - 1;
        bool found = false;
        char swap = '0';
        

        // Find first index from the end of string, where char is greater than previous char
        while (!found && index >= 1)
        {
            if( w[index] > w[index-1] )
            {
                found = true;
                swap = w[index - 1];
                break;
            }
            index--;
        }

        if(index == 0)
        {
            return "no answer";
        }
        else
        {
            // Hackerrank's version of C# does not support array range operator (..)
            // newWord.Append(w[0..(index - 1)]);
            for(int i = 0; i<= index - 2; i++)
            {
                newWord.Append(w[i]);
            }
            
            // Hackerrank's version of C# does not support array range operator (..)
            //var deck = new List<char>(w[(index - 1)..].ToCharArray());
            var deck = new List<char>();            
            for(int i = index - 1; i<= w.Length - 1; i++)
            {
                deck.Add(w[i]);
            }

            deck.Sort();
            char next = deck.Where( c => c > swap ).First();
            deck.Remove(next);
            

            newWord.Append(next);
            foreach (var character in deck)
            {
                newWord.Append(character);
            }

            return newWord.ToString();
        }
    }


}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int T = Convert.ToInt32(Console.ReadLine().Trim());

        for (int TItr = 0; TItr < T; TItr++)
        {
            string w = Console.ReadLine();

            string result = Result.biggerIsGreater(w);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
