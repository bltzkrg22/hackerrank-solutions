using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static List<int> BreakingRecords(IReadOnlyList<int> scores)
    {
        int currentMinRecord = scores[0];
        int currentMaxRecord = scores[0];

        int minCounter = 0;
        int maxCounter = 0;

        for (int i = 1; i < scores.Count; i++)
        {
            if (scores[i] < currentMinRecord)
            {
                minCounter++;
                currentMinRecord = scores[i];
            }

            if (scores[i] > currentMaxRecord)
            {
                maxCounter++;
                currentMaxRecord = scores[i];
            }
        }

        return new List<int>() { maxCounter, minCounter };
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> scores = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(scoresTemp => Convert.ToInt32(scoresTemp)).ToList();

        List<int> result = Result.BreakingRecords(scores);

        textWriter.WriteLine(String.Join(" ", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
