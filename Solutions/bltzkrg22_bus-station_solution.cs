using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static List<int> BusStation(List<int> a)
    {
        var prefixSumArray = new HashSet<int>();
        int currentPrefixSum = 0;
        for (int i = 0; i < a.Count; i++)
        {
            currentPrefixSum += a[i];
            prefixSumArray.Add(currentPrefixSum);
        }

        // One possible approach is checking all possible sizes from prefixSumArray,
        //          foreach (var candidateSize in prefixSumArray)
        // but it's too slow and times out half of the cases
        
        // So we check all integer divisors of the total sum of passagers

        var answer = new List<int>();
        int totalSum = prefixSumArray.Last();
        foreach (var candidateSize in Divisors(totalSum))
        {
            //bool candidateInvalidated = false;
            int nextMultiple = candidateSize;

            while (nextMultiple <= totalSum)
            {
                if (prefixSumArray.Contains(nextMultiple) == false)
                {
                    //candidateInvalidated = true;
                    goto CandidateInvalid;
                }
                else
                {
                    nextMultiple += candidateSize;
                }
            }

            answer.Add(candidateSize);

            CandidateInvalid: ;
        }

        return answer;
    }

    private static List<int> Divisors(int number)
    {
        int intsqrt = IntegerSquareRoot(number);
        var firstHalf = Enumerable.Range(1, intsqrt)
                         .Where(divisor => number % divisor == 0).ToList();

        var secondHalf = firstHalf.Select(x => number / x).OrderBy(x => x).ToList();

        // Special case to avoid duplication of intsqrt in case input number
        // is a perferct square.
        if (intsqrt * intsqrt == number)
        {
            firstHalf.Remove(intsqrt);
        }

        firstHalf.AddRange(secondHalf);
        return firstHalf;
    }

    // Borrowed from https://rosettacode.org/wiki/Isqrt_(integer_square_root)_of_X#C.23
    // Calculates the largest integer that is equal to or lower than square root of number
    private static int IntegerSquareRoot(int number)
    {
        int q = 1;
        int r = 0;
        int t;

        while (q <= number)
        { 
            q <<= 2; 
        }

        while (q > 1)
        {
            q >>= 2;
            t = number - r - q;
            r >>= 1;
            if (t >= 0)
            { 
                number = t;
                r += q;
            }
        }
        return r;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int aCount = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> a = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(aTemp => Convert.ToInt32(aTemp)).ToList();

        List<int> result = Result.BusStation(a);

        textWriter.WriteLine(String.Join(" ", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
