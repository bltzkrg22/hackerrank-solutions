using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static string caesarCipher(string s, int k)
    {
        var inputAsArray = s.ToCharArray();
        for (int i = 0; i < inputAsArray.Length; i++)
        {
            inputAsArray[i] = RotateCharacter(inputAsArray[i], k);
        }
        return new String(inputAsArray);
    }

    // 65-90 == UPPERCASE
    // 97-122 == lowercase
    private static char RotateCharacter(char character, int offset)
    {
        if ('A' <= character && character <= 'Z')
        {
            // Uppercase branch
            int newNumber = character + offset % 26;
            if (newNumber > 'Z')
            {
                newNumber -= 26;
            }
            return (char)newNumber;
        }
        else if ('a' <= character && character <= 'z')
        {
            // Lowercase branch
            int newNumber = character + offset % 26;
            if (newNumber > 'z')
            {
                newNumber -= 26;
            }
            return (char)newNumber;
        }

        // We don't rotate characters outside of range [A-Za-z]!
        return character;
    }

}


class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        string s = Console.ReadLine();

        int k = Convert.ToInt32(Console.ReadLine().Trim());

        string result = Result.caesarCipher(s, k);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
