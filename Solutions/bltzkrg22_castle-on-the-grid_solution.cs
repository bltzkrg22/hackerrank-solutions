using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    static readonly List<(int x, int y)> directions = new List<(int x, int y)>(){ (1,0), (0,1), (-1,0), (0,-1) };

    private const int ConstWall = -1;
    private const int ConstStart = -2;
    private const int ConstEnd = -3;

    public static int minimumMoves(List<string> grid, int startX, int startY, int goalX, int goalY)
    {
        /*
        * Let's change the storage of grid from a list of strings into an int array.
        * -1 will be an obstacle, -2 starting point, -3 ending point.
        */

        int gridSize = grid.Count;

        int[,] gridArray = new int[gridSize,gridSize];

        for (int y = 0; y < gridSize; y++)
        {
            for (int x = 0; x < gridSize; x++)
            {
                if (grid[y][x] == 'X')
                {
                    gridArray[y,x] = ConstWall;
                }
            }
        }
        
        gridArray[startX,startY] = ConstStart;
        gridArray[goalX,goalY] = ConstEnd;


        (int x, int y) startingPoint = (startX, startY);
        //(int x, int y) endingPoint = (goalX, goalY);

        /*
        * Simple flood fill.
        *
        * Before entering the loop will populate the hashset with the starting point coordinates.
        * Then we will fill all points that we can reach in a single step from start with 1,
        * and remember all points that we flooded.
        *
        * In each turn we will consider all points that we flooded in previous turn, and then flood
        * all previously unflooded points with a number one greater. This will mean that we can reach
        * those points with one more jump.
        *
        * Loop ends when the goal point is flooded. Number with which the goal is flooded is number of\
        * jumps that is required to reach the goal from the start point.
        */


        bool endReached = false;

        var currentTurnPoints = new HashSet<(int x, int y)>(  ){  startingPoint };

        while (endReached == false)
        {
            var nextTurnPoints = new HashSet<(int x, int y)>(  ){ };

            foreach (var point in currentTurnPoints)
            {
                var neighbours = FloodNeighbours(gridArray, gridSize, point);
                nextTurnPoints.UnionWith(neighbours);
            }

            currentTurnPoints = nextTurnPoints;

            if (gridArray[goalX,goalY] > 0)
            {
                endReached = true;
            }
        }

        return gridArray[goalX,goalY];
    }

    static HashSet<(int x, int y)> FloodNeighbours( int[,] grid, int gridSize, (int x, int y) point )
    {
        int currentDistance;
        var nextTurnPoints = new HashSet<(int x, int y)>();

        if (grid[point.x, point.y] > 0)
        {
            currentDistance = grid[point.x, point.y] + 1;
        }
        else if (grid[point.x, point.y] == ConstStart)
        {
            currentDistance = 1;
        }
        else
        {
            throw new ApplicationException($"{nameof(FloodNeighbours)} initiated from a wall or from the goal.");
        }

        /*
        * Consider each possible direction. We will keep flooding until we reach:
        * a) an edge of the grid,
        * b) an obstacle
        * c) or the goal.
        * If we encounter a square with a number lower than current flooding number, we don't change it (it means
        * that such square is reachable in fewer number of jumps). In case of chess rook, we actually can only
        * encounter a square with a number one fewer than current flooding number, but it does not matter for
        * the algorithm.
        */
        foreach (var direction in directions)
        {
            bool directionStop = false;
            (int x, int y) evaluatedPoint = point;
            while (directionStop == false)
            {
                /*
                * Move one step further according to current direction.
                */
                evaluatedPoint = (evaluatedPoint.x + direction.x, evaluatedPoint.y + direction.y );

                // Case" Edge of grid reached.
                if ( evaluatedPoint.x < 0 || evaluatedPoint.x > gridSize - 1 || evaluatedPoint.y < 0 || evaluatedPoint.y > gridSize - 1)
                {
                    directionStop = true;
                }
                // Case: Obstacle reached.
                else if (grid[evaluatedPoint.x,evaluatedPoint.y] == ConstWall)
                {
                    directionStop = true;
                }
                // Case: End reached. Flood it and stop the flooding in current direction.
                else if (grid[evaluatedPoint.x,evaluatedPoint.y] == ConstEnd)
                {
                    grid[evaluatedPoint.x,evaluatedPoint.y] = currentDistance;
                    directionStop = true;
                }
                // Case: Previously unvisited square reached. Flood it.
                else if (grid[evaluatedPoint.x,evaluatedPoint.y] == 0)
                {
                    grid[evaluatedPoint.x,evaluatedPoint.y] = currentDistance;
                    nextTurnPoints.Add(evaluatedPoint);
                }
            }

        }
        return nextTurnPoints;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<string> grid = new List<string>();

        for (int i = 0; i < n; i++)
        {
            string gridItem = Console.ReadLine();
            grid.Add(gridItem);
        }

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int startX = Convert.ToInt32(firstMultipleInput[0]);

        int startY = Convert.ToInt32(firstMultipleInput[1]);

        int goalX = Convert.ToInt32(firstMultipleInput[2]);

        int goalY = Convert.ToInt32(firstMultipleInput[3]);

        int result = Result.minimumMoves(grid, startX, startY, goalX, goalY);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
