using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private const char CavityCharacter = 'X';
    private static readonly List<(int x, int y)> Directions = new List<(int x, int y)>() { (1, 0), (-1, 0), (0, 1), (0, -1) };

    public static List<string> cavityMap(List<string> grid)
    {
        var cavitiesDetected = new List<(int x, int y)>();

        for (int j = 1; j <= grid.Count - 2; j++)
        {
            for (int i = 1; i <= grid.Count - 2; i++)
            {
                (int x, int y) point = (i, j);
                var value = GetValueAtCoordinates(point, grid);
                foreach (var neighbour in GetNeighbours(point))
                {
                    // We break out of foreach loop if even on neighbour is "deeper".
                    // goto is used to "skip" the line which adds the point to `cavitiesDetected`
                    // - we could also make a new bool flag `isCavity` or whatever
                    if (GetValueAtCoordinates(neighbour, grid) >= value)
                    {
                        goto EndOfLoop;
                    }
                }
                // If we did not break out, then current point is a cavity
                cavitiesDetected.Add(point);

            EndOfLoop:;
            }
        }

        // This could be made faster - if there are multiple cavities in a single row,
        // we could replace them all at once.
        foreach (var point in cavitiesDetected)
        {
            SetValueAtCoordinates(point, grid, CavityCharacter);
        }

        return grid;
    }

    private static char GetValueAtCoordinates((int x, int y) point, List<string> grid)
        => grid[point.y][point.x];

    private static void SetValueAtCoordinates((int x, int y) point, List<string> grid, char value)
    {
        var buffer = new StringBuilder(grid[point.y]);
        buffer[point.x] = value;
        grid[point.y] = buffer.ToString();
    }

    private static List<(int x, int y)> GetNeighbours( (int x, int y) point )
    {
        var answer = new List<(int x, int y)>();

        foreach (var neighbour in Directions)
        {
            answer.Add((point.x + neighbour.x, point.y + neighbour.y));
        }

        return answer;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<string> grid = new List<string>();

        for (int i = 0; i < n; i++)
        {
            string gridItem = Console.ReadLine();
            grid.Add(gridItem);
        }

        List<string> result = Result.cavityMap(grid);

        textWriter.WriteLine(String.Join("\n", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
