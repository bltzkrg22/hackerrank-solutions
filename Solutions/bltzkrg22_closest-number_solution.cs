using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    public static int closestNumber(int a, int b, int x)
    {
        /*
        * Edge cases:
        * b = 0 => then a^b = 1, and closes multiple of x will be 1 if x == 1, or 0 otherwise.
        * a = 1 => then a^b = 1, same case as above
        * b < 0 and a >= 2 => then 0 < a^b <= 0.5, and the closes multiple of x will always be 0 
        */

        if (b == 0 || a == 1)
        {
            if (x == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else if (b < 0) // since 1 <= a <= 10^9 from problem description, we silently assume that a >= 2
        {
            return 0;
        }

        /*
        * In a non-degenerating case we can simply calculate a^b. There will be two candidates for the closest multiple:
        * a^b - a^b % x   OR   a^b - a^b % x + x
        * Both are multiples of x; first is guaranteed to equal or less than a^b, and second is guarantedd to be greater than a^b
        */

        int power = Convert.ToInt32( Math.Pow( Convert.ToDouble(a), Convert.ToDouble(b) ) );

        int multiple1 = power - power % x;
        int multiple2 = power - power % x + x;

        /*
        * Both difference1 and difference2 are guaranteed to be non-negative.
        */

        int difference1 = power - multiple1;
        int difference2 = multiple2 - power;

        /*
        * We construct the condition is such a way, that if differences are equal, mulitle1 (which is smaller) is returned.
        */

        return difference1 <= difference2 ? multiple1 : multiple2;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int a = Convert.ToInt32(firstMultipleInput[0]);

            int b = Convert.ToInt32(firstMultipleInput[1]);

            int x = Convert.ToInt32(firstMultipleInput[2]);

            int result = Result.closestNumber(a, b, x);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
