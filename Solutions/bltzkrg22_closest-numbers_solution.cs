using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static List<int> closestNumbers(List<int> arr)
    {
        arr.Sort();

        int minDifference = Int32.MaxValue;
        var minDifferencePairs = new List<int>();

        for (int i = 1; i < arr.Count; i++)
        {
            if (arr[i] - arr[i-1] < minDifference)
            {
                minDifference = arr[i] - arr[i-1];
                minDifferencePairs.Clear();
                minDifferencePairs.Add(arr[i-1]);
            }
            else if (arr[i] - arr[i-1] == minDifference)
            {
                minDifferencePairs.Add(arr[i-1]);
            }
        }

        var answer = new List<int>();
        foreach (var lower in minDifferencePairs)
        {
            answer.Add(lower);
            answer.Add(lower + minDifference);
        }

        return answer;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        List<int> result = Result.closestNumbers(arr);

        textWriter.WriteLine(String.Join(" ", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
