using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static List<int> compareTriplets(List<int> a, List<int> b)
    {
        int aScore = 0;
        int bScore = 0;

        if (a.Count != b.Count)
        {
            throw new ArgumentException($"Input lists to method {nameof(compareTriplets)} must have the same count.");
        }


        for (int i = 0; i < a.Count; i++)
        {
            if (a[i] > b[i])
            {
                aScore++;
            }
            else if (a[i] < b[i])
            {
                bScore++;
            }
        }

        return new List<int>(){ aScore, bScore };
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        List<int> a = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(aTemp => Convert.ToInt32(aTemp)).ToList();

        List<int> b = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(bTemp => Convert.ToInt32(bTemp)).ToList();

        List<int> result = Result.compareTriplets(a, b);

        textWriter.WriteLine(String.Join(" ", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
