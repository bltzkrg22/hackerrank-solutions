

    // Complete the CompareLists function below.

    /*
     * For your reference:
     *
     * SinglyLinkedListNode {
     *     int data;
     *     SinglyLinkedListNode next;
     * }
     *
     */
    static bool CompareLists(SinglyLinkedListNode head1, SinglyLinkedListNode head2)
    {
        var fstListNode = head1;
        var sndListNode = head2;
        
        while(fstListNode != null && sndListNode != null)
        {
            if(fstListNode.data == sndListNode.data)
            {
                fstListNode = fstListNode.next;
                sndListNode = sndListNode.next;
                continue;
            }
            else
            {
                return false;
            }
        }
        
        if((fstListNode == null) && (sndListNode == null))
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }

