using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    internal static List<(int, int)> directions = new List<(int, int)>(){ (1,0), (0,1), (0,-1), (-1,0), (1,1), (1,-1), (-1,1), (-1,-1) };
    internal static HashSet<(int, int)> alreadyEvaluated = new HashSet<(int, int)>();
    internal static Dictionary<(int, int), int> regions = new Dictionary<(int, int), int>(); // Key is the first point from a region, Value is the count of all points in region

    public static int connectedCell(List<List<int>> matrix)
    {
        int rowsNumber = matrix.Count;
        int colsNumber = matrix[0].Count;

        /*
        * Algorithm:
        * 1. Find the next unevaluated node (until the end of matrix is reached). This will be the marker point for a region.
        * 2. Evaluate the node by recursively calling evaluate function for all connected neighbours. All called neighbours will share the region marker.
        *    With each evaluated node increase the count for all squares in current region. Count is stored in `regions` dictionary.
        * 3. The problem answer, i.e. the maximal number of squares in a single region, will be the maximal value stored in `regions`.
        */

        for (int y = 0; y < rowsNumber; y++)
        {
            for (int x = 0; x < colsNumber; x++)
            {
                if (GetValueAtXyPosition(x, y, matrix) == 1 && !alreadyEvaluated.Contains((x, y)))
                {
                    regions[(x,y)] = 0;
                    EvaluateNode( x, y, (x, y), matrix);
                }
            }
        }
        
        return regions.Values.Max();
    }

    internal static int GetValueAtXyPosition(int x, int y, List<List<int>> matrix)
        => matrix[y][x];

    
    internal static List<(int, int)> GetNonZeroNeighbours( int x, int y, List<List<int>> matrix)
    {
        /*
        * This method returns all neighbours of point (x,y) that are 1 in the input matrix. It does not check if those
        * neighbours were already counted, this is handled elsewhere.
        */
        var neighbours = new List<(int, int)>();

        foreach (var direction in directions)
        {
            try
            {
                if (GetValueAtXyPosition( x + direction.Item1, y + direction.Item2, matrix ) == 1)
                {   
                    neighbours.Add( (x + direction.Item1, y + direction.Item2) );
                }
            }
            // I actually don't remember which exception is appropriate here.
            catch (IndexOutOfRangeException)
            {
                // Do nothing, if index out of range that this is not a valid neighbour!
                // throw;
            }
            catch (ArgumentOutOfRangeException)
            {
                // Do nothing, if index out of range that this is not a valid neighbour!
                // throw;
            }

        }

        return neighbours;
    }

    internal static void EvaluateNode( int x, int y, (int, int) regionMarker, List<List<int>> matrix)
    {
        /*
        * If the EvaluateNode method was called, than current point (x,y) is a new point that belongs to the region. The "key", or
        * abstraction class than uniquely describes the region is the first point of the region that was evaluated, i.e. (int, int) regionMarker.
        */
        alreadyEvaluated.Add((x, y));
        regions[ regionMarker ] += 1;

        var neighbours = GetNonZeroNeighbours(x, y, matrix);

        /*
        * If point has no neighbours, stop evaluation. I don't remember if calling a foreach loop when neighbours collection is empty
        * throws an exception (which we don't want) or just does nothing (which we want), so I included this check.
        */
        if (neighbours.Count == 0)
        {
            return;
        }
        else
        {
            foreach (var neighbour in neighbours)
            {
                // We only want to count a node once, so don't evaluate neighbour squares that were already evaluated.
                if (!alreadyEvaluated.Contains(neighbour))
                {
                    EvaluateNode( neighbour.Item1, neighbour.Item2, regionMarker, matrix );
                }
            }
        }
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        int m = Convert.ToInt32(Console.ReadLine().Trim());

        List<List<int>> matrix = new List<List<int>>();

        for (int i = 0; i < n; i++)
        {
            matrix.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(matrixTemp => Convert.ToInt32(matrixTemp)).ToList());
        }

        int result = Result.connectedCell(matrix);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
