using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private const int Modulo = 1234567;

    // Note that according to input constraints, next <= 1000, therefore
    // (total * next) <= 1234567000 and can never overflow an Int32
    public static int connectingTowns(int n, List<int> routes)
        => routes.Aggregate(1, (total, next) => (total * next) % Modulo);

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> routes = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(routesTemp => Convert.ToInt32(routesTemp)).ToList();

            int result = Result.connectingTowns(n, routes);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
