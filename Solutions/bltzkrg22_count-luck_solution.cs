using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    static readonly List<(int x, int y)> directions = new List<(int x, int y)>(){ (1,0), (0,1), (-1,0), (0,-1) };

    private const int ConstWall = -1;
    private const int ConstStart = -2;
    private const int ConstEnd = -3;


    public static string countLuck(List<string> matrix, int k)
    {
        /*
        * Since I want to be able to modify elements of the labirynth matrix one char at a time,
        * List of strings is not going to be very efficient. Since n and m are less than 100, I'll just
        * create a new 2d Array int[n,m].
        */

        int numberOfRows = matrix.Count;
        int numberOfColumns = matrix[0].Length;

        var labirynth = new int[numberOfColumns,numberOfRows];

        /*
        * We'll use the following convention to map special squares in the labirynth:
        * -1 = wall, 'X'
        * -2 = starting point, 'M'
        * -3 = ending point, '*'
        * Since we'll be using a simple algorithm, positive number in a cell will be distance from the starting point,
        * and 0 will mean that a square was not evaluated yet.
        */



        (int x, int y) startingPoint = (-1, -1);
        (int x, int y) endingPoint = (-1, -1);

        for (int i = 0; i < numberOfRows; i++)
        {
            for (int j = 0; j < numberOfColumns; j++)
            {
                switch (matrix[i][j])
                {
                    case 'X':
                        labirynth[j,i] = ConstWall;
                        break;
                    case 'M':
                        labirynth[j,i] = ConstStart;
                        startingPoint = (j, i);
                        break;
                    case '*':
                        labirynth[j,i] = ConstEnd;
                        endingPoint = (j, i);
                        break;
                    default:
                        labirynth[j,i] = 0;
                        break;
                }
            }            
        }



        /*
        * Now perform a simple flood fill to find the shortest path in the maze.
        * This is a navive breadth first search, no optimization was necessary for such small input range.
        * In each step we evaluate all squares that were filled in previous step, and find their neigbours
        * which were not filled yet. Loop repeats until the maze end was found.
        */

        bool endReached = false;
        int currentPathLength = 1;

        var currentTurnPoints = new List<(int x, int y)>(  ){  startingPoint };

        while (endReached == false)
        {
            var nextTurnPoints = new List<(int x, int y)>(  ){ };
            var currentlyEvaluatedNeighbours = new List<(int x, int y)>(  );

            foreach (var point in currentTurnPoints)
            {
                currentlyEvaluatedNeighbours.AddRange( GetNonWallNeighbours( labirynth, numberOfRows, numberOfColumns, point ) );
            }

            foreach (var point in currentlyEvaluatedNeighbours)
            {
                if (labirynth[point.x, point.y] == 0)
                {
                    labirynth[point.x, point.y] = currentPathLength;
                    nextTurnPoints.Add( point );
                }
                else if (labirynth[point.x, point.y] == ConstEnd)
                {
                    labirynth[point.x, point.y] = currentPathLength;
                    endReached = true;
                }
            }

            currentTurnPoints = nextTurnPoints;
            currentPathLength++;
        }


        // Subtract one, since we are still incrementing the counter in the loop even after the maze end was reached.
        currentPathLength--;

        // Since we will be modifying currentPathLength variable, store it's current value (even if we never use it).
        int pathLenth = currentPathLength;




        /*
        * Now travese the path from maze end to maze start. In each step, we will move from current square A to such a square B,
        * that the number we filled B with is one smaller than number of A. Since we know there is exactly one shortest path
        * in the maze, there will be only one square B that satisfies the condition.
        *
        * Afterwards, we consider square B, and check how many neighbours of B have a number one greater than B (A will be one such
        * square, but possibly there will be more than one). If we find more than neighbour of B with a number exactly one greater,
        * this means than there was a possible branch at square B, and we increment the branch counter.
        *
        * Loop repeats until we reach the starting point. Note that the branch-check from point B is initialized when
        * currentPoint = point A, therefore the final check for any branches from the starting point is made as well by
        * this loop.
        */

        int branchCounter = 0;
        (int x, int y) currentPoint = endingPoint;
        (int x, int y) nextPoint = (-1, -1);

        // (currentPoint != startingPoint) syntax for tuplpes was introduced in 
        // a newer version of C# than the one used currently on HackerRank
        while (currentPoint.x != startingPoint.x || currentPoint.y != startingPoint.y)
        {
            var neighbours = GetNonWallNeighbours( labirynth, numberOfRows, numberOfColumns, currentPoint );

            foreach (var neighbour in neighbours)
            {
                if (labirynth[neighbour.x,neighbour.y] == currentPathLength - 1)
                {
                    nextPoint = neighbour;
                    break;
                }
                else if (labirynth[neighbour.x,neighbour.y] == ConstStart)
                {
                    nextPoint = neighbour;
                    break;
                }
            }

            var backtrackNeighbours = GetNonWallNeighbours( labirynth, numberOfRows, numberOfColumns, nextPoint );
            if (backtrackNeighbours.Where( point => labirynth[point.x, point.y] == currentPathLength  ).Count() > 1)
            {
                branchCounter++;
            }

            currentPathLength--;
            currentPoint = nextPoint;
        }

        /*
        * Now check if the actual number of branches was correctly guessed (i.e. whether it is equal to k).
        */

        return branchCounter == k ? "Impressed" : "Oops!";
    }

    static List<(int x, int y)> GetNonWallNeighbours( int[,] labirynth, int rows, int columns, (int x, int y) point )
    {
        var nonWallNeighbours = new List<(int x, int y)>();

        foreach (var direction in directions)
        {
            (int x, int y) checkedPoint = (  point.x + direction.x, point.y + direction.y  );

            if (checkedPoint.x >= 0 && checkedPoint.x <= columns - 1 && checkedPoint.y >= 0 && checkedPoint.y <= rows - 1 &&
                                       labirynth[checkedPoint.x,checkedPoint.y] != ConstWall )
            {
                nonWallNeighbours.Add(checkedPoint);
            }
        }

        return nonWallNeighbours;
    }

}


class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int n = Convert.ToInt32(firstMultipleInput[0]);

            int m = Convert.ToInt32(firstMultipleInput[1]);

            List<string> matrix = new List<string>();

            for (int i = 0; i < n; i++)
            {
                string matrixItem = Console.ReadLine();
                matrix.Add(matrixItem);
            }

            int k = Convert.ToInt32(Console.ReadLine().Trim());

            string result = Result.countLuck(matrix, k);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
