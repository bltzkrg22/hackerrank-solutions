using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution
{

    // Complete the countTriplets function below.
    static long countTriplets(List<long> arr, long r)
    {
        var singleDict = new Dictionary<long, long>(); // number, counter
        var doubleDict = new Dictionary<(long, long), long>(); // pair of numbers (x, x*r), counter
        long tripleCounter = 0;

        if (r == 1)
        {
            foreach (var number in arr)
            {
                if (singleDict.ContainsKey(number))
                {
                    singleDict[number] += 1;
                }
                else
                {
                    singleDict[number] = 1;
                }
            }


            foreach (var pairKeyValue in singleDict)
            {
                if (pairKeyValue.Value >= 3)
                {
                    tripleCounter += pairKeyValue.Value * (pairKeyValue.Value - 1) / 2 * (pairKeyValue.Value - 2) / 3; // possible overflow?
                }
            }
            return tripleCounter;
        }


        foreach (var number in arr)
        {
            if (singleDict.ContainsKey(number))
            {
                singleDict[number] += 1;
            }
            else
            {
                singleDict[number] = 1;
            }

            if (number % r == 0)
            {
                var divided = number / r;
                if (doubleDict.ContainsKey((divided, number)) && singleDict.ContainsKey(divided))
                {
                    doubleDict[(divided, number)] += singleDict[divided];
                }
                else if (singleDict.ContainsKey(divided))
                {
                    doubleDict[(divided, number)] = singleDict[divided];
                }


                if (divided % r == 0)
                {
                    var doubleDivided = divided / r;
                    if (doubleDict.ContainsKey((doubleDivided, divided)))
                    {
                        tripleCounter += doubleDict[(doubleDivided, divided)];
                    }
                }
            }


        }


        return tripleCounter;


    }

    static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] nr = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(nr[0]);

        long r = Convert.ToInt64(nr[1]);

        List<long> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt64(arrTemp)).ToList();

        long ans = countTriplets(arr, r);

        textWriter.WriteLine(ans);

        textWriter.Flush();
        textWriter.Close();
    }
}
