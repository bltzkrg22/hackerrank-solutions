using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;
using System.Numerics;

class Result
{
    public static string counterGame(long n)
    {
        /*
        * Let's define two variables:
        * a) bitCount, which will be the number of ones in the binary representation of n
        * b) trailingZeroes, which will be the numbers of consecutive least significant bits that are zeroes
        *
        * Example: 134 = 128 + 4 + 2 = 0b010000110, so bitcount = 3 and trailingZeroes = 1
        *
        * It's easy to see that the game from problem description can be written down equivalently as:
        * (1) as long as bitcount > 1, cross out (subtract) the most significant one from the current number
        * (2) when there is only single one left (i.e. the number is a power of 2), cross out the least significant zero (shift bits one position to the right)
        *
        * So the number of turns played will be (bitCount - 1) + trailingZeroes
        * If that number is odd, player one (Louise) wins, when it's even - player two (Richard) wins.
        * (The actual method rewrites "(bitCount - 1) + trailingZeroes is even" to "bitCount + trailingZeroes is odd").
        *
        * Note, that .NET 5 has access to methods BitOperations.PopCount and BitOperations.TrailingZeroCount,
        * that scream to be used here - they are not available on HackerRank's version
        * of .NET though :(
        */
        int bitCount = PopCount(Convert.ToUInt64(n));
        int trailingZeroes = TrailingZeroCount(Convert.ToUInt64(n));

        return (bitCount + trailingZeroes) % 2 == 1 ? "Richard" : "Louise";
    }
    
    private static int PopCount(ulong n)
    {
        int counter = 0;
        while(n > 0)
        {
            if(n % 2 == 1)
            {
                counter++;
            }
            n >>= 1;
        }
        return counter;
    }
    
    private static int TrailingZeroCount(ulong n)
    {
        if(n == 0)
        {
            return 64;
        }
        else
        {
            int counter = 0;
            
            while (n % 2 == 0)
            {
                counter++;
                n >>= 1;
            }
            return counter;
        }
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            long n = Convert.ToInt64(Console.ReadLine().Trim());

            string result = Result.counterGame(n);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
