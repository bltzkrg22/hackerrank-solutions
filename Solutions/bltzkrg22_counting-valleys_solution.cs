using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static int CountingValleys(int steps, string path)
    {
        int valleyCount = 0;

        int currentAltitude;
        int previousAltitude = 0;

        foreach (var step in path)
        {
            currentAltitude = previousAltitude + ConvertStepToAltitudeDiff(step);
            if (previousAltitude == 0 && currentAltitude < 0)
            {
                valleyCount++;
            }

            previousAltitude = currentAltitude;
        }

        return valleyCount;
    }

    private static int ConvertStepToAltitudeDiff(char step)
    {
        switch (step)
        {
            case 'U':
                return 1;
            case 'D':
                return -1;
            default:
                throw new ApplicationException(@"Only D and U characters are expected in the input string.");
        }
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int steps = Convert.ToInt32(Console.ReadLine().Trim());

        string path = Console.ReadLine();

        int result = Result.CountingValleys(steps, path);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
