using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * This is plain cheating. Instead of implementing a sorting algorithm that will be stable, we will use
    * selectsort and append an additional key to the data, representing the original position, and use it
    * as the tiebreaker in a custom comparator.
    */
    class Triplet
    {        
        internal int Id { get; set; }
        internal int Number { get; set; }
        internal string ReplacementText { get;  } // no set!

        internal Triplet(int id, int number, string text, bool replace)
        {
            Id = id;
            Number = number;
            ReplacementText = replace ? "-" : text;
        }
    }

    private class TripletComparer : IComparer<Triplet> 
    {
        public int Compare(Triplet x, Triplet y)
        {
            if (x.Number != y.Number)
            {
                return (x.Number).CompareTo(y.Number);
            }
            else
            {
                return (x.Id).CompareTo(y.Id);
            }
        }
    }





    public static void countSort(List<List<string>> arr)
    {
        var tripletList = new List<Triplet>();

        for (int i = 0; i < arr.Count / 2; i++)
        {
            tripletList.Add( new Triplet(i, int.Parse(arr[i][0]), arr[i][1], true ) );
        }
        for (int i = arr.Count / 2; i < arr.Count; i++)
        {
            tripletList.Add( new Triplet(i, int.Parse(arr[i][0]), arr[i][1], false ) );
        }

        tripletList.Sort(new TripletComparer());


        var answer = new StringBuilder();
        foreach (var triplet in tripletList)
        {
            answer.Append(triplet.ReplacementText);
            answer.Append(" ");
        }
        answer.Remove(answer.Length - 1, 1);

        Console.WriteLine(answer.ToString());
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<List<string>> arr = new List<List<string>>();

        for (int i = 0; i < n; i++)
        {
            arr.Add(Console.ReadLine().TrimEnd().Split(' ').ToList());
        }

        Result.countSort(arr);
    }
}
