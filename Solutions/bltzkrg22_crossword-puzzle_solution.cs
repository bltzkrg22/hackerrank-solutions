using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    internal enum Direction {vertical, horizontal}
    internal class emptyLine
    {
        internal int X {get; set;}
        internal int Y {get; set;}
        internal int Size {get; set;}
        internal Direction lineDirection {get; set;}
        internal string Content {get; set;} = String.Empty;

        internal emptyLine( int x, int y, int size, Direction dir )
        {
            X = x;
            Y = y;
            Size = size;
            lineDirection = dir;
        }

        internal bool IsPointInside( int x, int y, out int position )
        {
            if ( lineDirection == Direction.vertical && (x == this.X) && (y - this.Y >= 0) && (y - this.Y <= this.Size - 1) )
            {
                position = y - this.Y;
                return true;
            }
            if ( lineDirection == Direction.horizontal && (y == this.Y) && (x - this.X >= 0) && (x - this.X <= this.Size - 1) )
            {
                position = x - this.X;
                return true;
            }
            else
            {
                position = -1;
                return false;
            }
        }

        internal char CharAtIndex(int index)
        {
            char c;

            if (String.IsNullOrEmpty( this.Content ))
            {
                c = '?';
                return c;
            }


            try
            {
                c = Content[index];
            }
            catch (IndexOutOfRangeException)
            {
                c = '?';
                // throw;
            }

            return c;
        }
    }
    

    public static List<string> crosswordPuzzle(List<string> crossword, string words)
    {
        int crosswordHeight = crossword.Count;
        int crosswordLength = crossword[0].Length;

        var emptySpaces = new List<emptyLine>();

        /*
        * (Step 1) Find all empty lines in crossword grid. While number of empty line should
        * be equal to number of words, we won't use that assumption here.
        *
        * Each empty line will be described by a starting point, direction and length.
        * Technically, we will use a regular expression to find all groups containig
        * character '-' reapeated at least twice.
        */

        // Scan horizontal lines

        string pattern = @"-{2,}";

        for (int i = 0; i < crosswordHeight; i++)
        {
            var horizontalLine = crossword[i];
            MatchCollection matches = Regex.Matches(horizontalLine, pattern);

            foreach (Match match in matches)
            {
                emptySpaces.Add( new emptyLine(match.Index, i, match.Length, Direction.horizontal) );
            }
        }

        // Scan vertical lines.

        for (int i = 0; i < crosswordLength; i++)
        {
            var verticalLineBuilder = new StringBuilder(crosswordHeight);

            for (int j = 0; j < crosswordHeight; j++)
            {
                verticalLineBuilder.Append( crossword[j][i] );
            }

            string verticalLine = verticalLineBuilder.ToString();

            MatchCollection matches = Regex.Matches(verticalLine, pattern);

            foreach (Match match in matches)
            {
                emptySpaces.Add( new emptyLine(i, match.Index, match.Length, Direction.vertical) );
            }
        }


        /*
        * (Step 2) Find all points that are common for two lines (notice that a point in a crossword can never
        * be common for more than two lines). Each common point will be described as a pair of two pairs:
        * CommonPoint: ( Word1: ( index of Word1 in emptySpaces list, position of common letter inside Word1 ),
        *                Word2: ( index of Word2 in emptySpaces list, position of common letter inside Word2 ) );
        * To avoid duplicates, we require that "index of Word1 in emptySpaces list" < "index of Word2 in emptySpaces list".
        */

        var commonPoints = new Dictionary<(int, int), (int, int)>();
        
        for (int i = 0; i < emptySpaces.Count; i++)
        {
            for (int j = 0; j < emptySpaces[i].Size; j++)
            {
                int coordX;
                int coordY;
                if (emptySpaces[i].lineDirection == Direction.vertical)
                {
                    coordX = emptySpaces[i].X;
                    coordY = emptySpaces[i].Y + j;
                }
                else
                {
                    coordX = emptySpaces[i].X + j;
                    coordY = emptySpaces[i].Y;
                }

                for (int k = i + 1; k < emptySpaces.Count; k++)
                {
                    if (emptySpaces[k].IsPointInside( coordX, coordY, out int positionK ))
                    {
                        commonPoints.Add( ( i, j ), (k, positionK )  );
                    }
                }
            }
        }

        /*
        * (Step 3) Permutate the words that we have to put inside empty spaces in the crossword. Match this permutation to
        * the list of empty spaces created in Step 1. If the length of permutation[i] (i.e. word at index i in current
        * permutation) does not match the length of emptySpaces[i], than we know for sure that this permutation is incorrect.
        *
        * Then check if the character at each common point is the same for both words. For example, if we know that the 3rd letter of
        * empty space 0 is common with the 1st letter of empty space 4, than we need to check if 3rd letter of permutation[0] matches
        * 1st letter of permutation[4].
        *
        * If all common letters match each other, then we found the solution.
        */

        var wordList = words.Split(';').ToList();

        foreach (var permutation in GetPermutations(wordList, wordList.Count))
        {
            bool errorFound = false;
            var currentPermutationList = permutation.ToList();
            for (int i = 0; i < wordList.Count; i++)
            {
                emptySpaces[i].Content = currentPermutationList[i];
                if (emptySpaces[i].Size != emptySpaces[i].Content.Length)
                {
                    errorFound = true;
                    break;
                }
            }

            if (errorFound == true)
            {
                continue;
            }

            // If lengths match, check common letters

            foreach (var commonPoint in commonPoints)
            {
                int firstWord = commonPoint.Key.Item1;
                char firstWordCharacter =  emptySpaces[firstWord].CharAtIndex( commonPoint.Key.Item2 );

                int secondWord = commonPoint.Value.Item1;
                char secondWordCharacter =  emptySpaces[secondWord].CharAtIndex( commonPoint.Value.Item2 );

                if (firstWordCharacter != secondWordCharacter)
                {
                    errorFound = true;
                    break;
                }
                else if( firstWordCharacter == '?' || secondWordCharacter == '?' )
                {
                    throw new ApplicationException($"Tried to access a character out of range for current string.");
                }
            }

            if (errorFound == false)
            {
                /*
                * If at this point there was no error, then we found a solution. We don't check if there are more possiblities.
                * For each horizontal empty space, we just need to read the string and replace '-' characters.
                * For each vertical empty space, we have to read all strings (which are horizontal lines in the crossword),
                * and replace a single character in each of those strings.
                */
                foreach (var line in emptySpaces)
                {
                    if (line.lineDirection == Direction.horizontal)
                    {
                        var chararray = crossword[line.Y].ToCharArray();
                        for (int i = 0; i < line.Content.Length; i++)
                        {
                            chararray[i + line.X] = line.Content[i];
                        }
                        crossword[line.Y] = new String(chararray);
                    }
                    else // if (line.lineDirection == Direction.vertical)
                    {
                        for (int i = 0; i < line.Content.Length; i++)
                        {
                            var chararray = crossword[line.Y + i].ToCharArray();
                            chararray[line.X] = line.Content[i];
                            crossword[line.Y + i] = new String(chararray);
                        }
                    }
                }
                break;
            }
        }
        return crossword;
    }


    /*
    * A recursive method that returns all possible permutations of IEnumerable. For example, if we call
    * GetPermutations<T>(new List<string>(){"LONDON", "DELHI", "ICELAND"}, 3), method should return a list
    * of lists:
    * {
    *     {"LONDON", "DELHI", "ICELAND"},
    *     {"LONDON", "ICELAND", "DELHI"},
    *     {"DELHI", "LONDON", "ICELAND"},
    *     {"DELHI", "ICELAND", "LONDON"},
    *     {"ICELAND", "LONDON", "DELHI"},
    *     {"ICELAND", "DELHI", "LONDON"},
    * }
    */
    static IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<T> list, int length)
    {
        if (length == 1) return list.Select(t => new T[] { t });

        return GetPermutations(list, length - 1)
            .SelectMany(t => list.Where(e => !t.Contains(e)),
                (t1, t2) => t1.Concat(new T[] { t2 }));
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        List<string> crossword = new List<string>();

        for (int i = 0; i < 10; i++)
        {
            string crosswordItem = Console.ReadLine();
            crossword.Add(crosswordItem);
        }

        string words = Console.ReadLine();

        List<string> result = Result.crosswordPuzzle(crossword, words);

        textWriter.WriteLine(String.Join("\n", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
