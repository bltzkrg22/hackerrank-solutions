using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{



    public static void countSwaps(List<int> a)
    {
        int howMany = a.Count;
        ulong swapCount = 0;
        
        int swapBuffer;
        
        for(int i = 0; i < howMany; i++)
        {
            for (int j = 0; j < howMany - 1; j++)
            {
                if (a[j] > a[j + 1])
                {
                    swapCount++;
                    
                    swapBuffer = a[j];
                    a[j] = a[j+1];
                    a[j+1] = swapBuffer;
                }
            }
        }
        
        
        
        Console.WriteLine($"Array is sorted in {swapCount} swaps.");
        Console.WriteLine($"First Element: {a.First()}");
        Console.WriteLine($"Last Element: {a.Last()}");
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> a = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(aTemp => Convert.ToInt32(aTemp)).ToList();

        Result.countSwaps(a);
    }
}
