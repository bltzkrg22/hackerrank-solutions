using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'makeAnagram' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. STRING a
     *  2. STRING b
     */

    public static int makeAnagram(string a, string b)
    {
        var frequencyDict = new Dictionary<char, int>();

        foreach (var character in a)
        {
            if (frequencyDict.ContainsKey(character))
            {
                frequencyDict[character] += 1;
            }
            else
            {
                frequencyDict[character] = 1;
            }
        }

        foreach (var character in b)
        {
            if (frequencyDict.ContainsKey(character))
            {
                frequencyDict[character] += -1;
            }
            else
            {
                frequencyDict[character] = -1;
            }
        }


        // There is actually no reason to filter out zeros, since adding a zero
        // does not change the sum. I guess that LINQ filter will take more time
        // than just calculating Math.Abs(0) and adding it to the sum.
        
        /*return deletions = frequencyDict.Where( x => x.Value != 0 )
                                     .Select( x => Math.Abs(x.Value) )
                                     .Sum();*/
            
        int deletions =  0;                        
        foreach (var frequency in frequencyDict.Values)
        {
            deletions += Math.Abs(frequency);
        }

        return deletions;                             
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string a = Console.ReadLine();

        string b = Console.ReadLine();

        int res = Result.makeAnagram(a, b);

        textWriter.WriteLine(res);

        textWriter.Flush();
        textWriter.Close();
    }
}
