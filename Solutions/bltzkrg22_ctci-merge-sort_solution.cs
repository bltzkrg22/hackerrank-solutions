using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    public static long inversionCount = 0;

    public static long countInversions(List<int> arr)
    {
        inversionCount = 0;
        
        MergeSort(arr, 0, arr.Count - 1);
        
        return inversionCount;
    }
    
    
    public static void Merge(List<int> listToSort, int leftIndex, int middleIndex, int rightIndex)
        {
            // middleIndex is included in the LEFT list

            var tempLeft = new Queue<int>(listToSort.GetRange(leftIndex, middleIndex - leftIndex + 1));
            var tempRight = new Queue<int>(listToSort.GetRange(middleIndex + 1, rightIndex - middleIndex));

            int currentSortIndex = leftIndex;

            while (tempLeft.Count > 0 && tempRight.Count > 0)
            {
                if (tempLeft.Peek() <= tempRight.Peek())
                {
                    listToSort[currentSortIndex] = tempLeft.Dequeue();
                }
                else
                {
                    listToSort[currentSortIndex] = tempRight.Dequeue();
                    Result.inversionCount = Result.inversionCount + tempLeft.Count;
                }
                currentSortIndex++;
            }

            if (tempLeft.Count == 0)
            {
                while (tempRight.Count > 0)
                {
                    listToSort[currentSortIndex] = tempRight.Dequeue();
                    currentSortIndex++;
                }
            }
            else
            {
                while (tempLeft.Count > 0)
                {
                    listToSort[currentSortIndex] = tempLeft.Dequeue();
                    currentSortIndex++;
                }
            }
        }

        public static void MergeSort(List<int> listToSort, int leftIndex, int rightIndex)
        {
            if (leftIndex == rightIndex)
            {
                return;
            }

            int middleIndex = (leftIndex + rightIndex) / 2;

            MergeSort(listToSort, leftIndex, middleIndex);
            MergeSort(listToSort, middleIndex + 1, rightIndex);

            Merge(listToSort, leftIndex, middleIndex, rightIndex);
        }
    

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

            long result = Result.countInversions(arr);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
