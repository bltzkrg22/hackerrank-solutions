using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'checkMagazine' function below.
     *
     * The function accepts following parameters:
     *  1. STRING_ARRAY magazine
     *  2. STRING_ARRAY note
     */

    public static void checkMagazine(List<string> magazine, List<string> note)
    {
        bool wordNotInMagazine = false;

        magazine.Sort();
        note.Sort();

        int magCounter = 0;
        for (int noteCounter = 0; noteCounter < note.Count; noteCounter++)
        {
            try
            {
                while (  String.Compare(note[noteCounter], magazine[magCounter]) > 0 )
                {
                    magCounter++;
                }
            }
            catch (System.ArgumentOutOfRangeException)
            {
                wordNotInMagazine = true;
                break;
                //throw;
            }
            if (String.Compare(note[noteCounter], magazine[magCounter]) == 0)
            {
                magCounter++;
            }
            else
            {
                wordNotInMagazine = true;
                break;
            }

        }

        string message = wordNotInMagazine ? "No" : "Yes";

        System.Console.WriteLine(message);
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int m = Convert.ToInt32(firstMultipleInput[0]);

        int n = Convert.ToInt32(firstMultipleInput[1]);

        List<string> magazine = Console.ReadLine().TrimEnd().Split(' ').ToList();

        List<string> note = Console.ReadLine().TrimEnd().Split(' ').ToList();

        Result.checkMagazine(magazine, note);
    }
}
