


    static bool hasCycle(SinglyLinkedListNode head)
    {
        var slowPointer = head;
        var fastPointer = head.next;

        while (slowPointer != null && fastPointer != null && fastPointer.next != null)
        {
            if (!Object.ReferenceEquals(slowPointer, fastPointer))
            {
                slowPointer = slowPointer.next;
                fastPointer = (fastPointer.next).next;
            }
            else
            {
                return true;
            }
        }
        return false;
    }

