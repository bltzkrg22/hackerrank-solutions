using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private const string NoSolution = "NO";
    private const string SolutionExists = "YES";
    public static string JugGame(int firstCapacity, int secondCapacity, int goalVolume)
    {
        /*
        * The solution exists, if at least one jug has greater capacity than the goal,
        * and if goal is a multiple of GCD(firstCapacity, secondCapacity)
        */
       
        if (goalVolume > firstCapacity && goalVolume > secondCapacity)
        {
            return NoSolution;
        }

        int gcd = (int)GreatestCommonDivisor((uint)firstCapacity, (uint)secondCapacity);

        return goalVolume % gcd == 0 ? SolutionExists : NoSolution;
    }

    private static uint GreatestCommonDivisor(uint a, uint b)
    {
        while (a != 0 && b != 0)
        {
            if (a > b)
            {
                a %= b;
            }
            else
            {
                b %= a;
            }
        }

        return a | b;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int a = Convert.ToInt32(firstMultipleInput[0]);

            int b = Convert.ToInt32(firstMultipleInput[1]);

            int c = Convert.ToInt32(firstMultipleInput[2]);

            string result = Result.JugGame(a, b, c);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
