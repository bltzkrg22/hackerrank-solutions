using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static int solve(int d, int p)
    {
        /*
        * Initially we will assume that a >= b. We are looking for such a pair (a,b),
        * that will satisfy the system of equations:
        *        a - b = d
        *        a * b = p
        * 
        * The solution for this equation systems are:
        * 1) b_1 = - d / 2 - sqrt{ p + d^2 / 4 }, a_1 = b_1 + d
        * 2) b_2 = - d / 2 + sqrt{ p + d^2 / 4 }, a_2 = b_2 + d
        * 
        * We just need to check if the solutions are integers. We will do so by checking
        * if 4 * p + d^2 is a square of an integer!
        * 
        * Also, since we assumed a >= b, we must also include inverted pairs. Note, that
        * when a == b (i.e. d == 0), there inverted pair is not a new solution!
        */


        var bees = new List<double>();

        if ((long)4 * p + (long)d * d > 0)
        {
            if (!IsPerfectSquare((long)p * 4 + (long)d * d, out long squareRoot))
            {
                return 0;
            }

            double sqrtOfDelta = squareRoot / 2.0;

            double b1 = sqrtOfDelta - (double)d / 2.0;
            double b2 = - sqrtOfDelta - (double)d / 2.0;

            //double a1 = b1 + d;
            //double a2 = b2 + d;

            if (IsInteger(b1))
            {
                bees.Add(Convert.ToInt32(b1));
            }
            if (IsInteger(b2))
            {
                bees.Add(Convert.ToInt32(b2));
            }
        }
        else if ((long)4 * p + (long)d * d == 0)
        {
            double b = - (double)d / 2.0;

            //double a = b + d;

            if (IsInteger(b))
            {
                bees.Add(Convert.ToInt32(b));
            }
        }

        if (d == 0)
        {
            return bees.Count;
        }
        else if (d > 0)
        {
            return 2 * bees.Count;
        }
        else
        {
            return 0;
        }
    }

    private const double Eps = 10E-12;
    
    private static bool IsInteger(double d) => Math.Abs(d - Math.Round(d)) < Eps;

    private static bool IsPerfectSquare(long input, out long closestRoot)
    {
        closestRoot = (long) Math.Sqrt(input);
        return input == closestRoot * closestRoot;
    }
}


class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(Console.OpenStandardOutput());

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int d = Convert.ToInt32(firstMultipleInput[0]);

            int p = Convert.ToInt32(firstMultipleInput[1]);

            int result = Result.solve(d, p);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
