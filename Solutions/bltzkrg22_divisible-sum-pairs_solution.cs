using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static int DivisibleSumPairs(int divisor, List<int> numbers)
    {
        int count = 0;
        
        // Frequency of each remainder of `divisor` in the input array
        Dictionary<int, int> remaindersFreqs = numbers.GroupBy(n => n % divisor)
            .ToDictionary(grp => grp.Key, grp => grp.Count());

        // First case: sum of two numbers with remainder 0 is divisible by `divisor`
        count += NOverTwo(remaindersFreqs.GetValueOrZero(0));

        bool isDivisorEven = divisor % 2 == 0;

        // Second case: if `divisor` is even, then sum of two numbers with remainder
        // `divisor`/2 is divisible
        if (isDivisorEven)
        {
            count += NOverTwo(remaindersFreqs.GetValueOrZero(divisor / 2));
        }

        // Greatest integer which is less than (decimal)divisor / 2;
        int loopStopper = isDivisorEven ? (divisor - 1) / 2 : divisor / 2;

        // General case: sum of two numbers with remainders x and `divisor`-x is
        // divisble by `divisor`
        for (int i = 1; i <= loopStopper; i++)
        {
            count += remaindersFreqs.GetValueOrZero(i)
                * remaindersFreqs.GetValueOrZero(divisor - i);
        }

        return count;
    }

    private static int NOverTwo(int n) => n * (n-1) / 2;
}

internal static class DictionaryExtension
{
    internal static int GetValueOrZero(this Dictionary<int, int> dict, int key) 
        => dict.ContainsKey(key) ? dict[key] : 0;
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int k = Convert.ToInt32(firstMultipleInput[1]);

        List<int> ar = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arTemp => Convert.ToInt32(arTemp)).ToList();

        int result = Result.DivisibleSumPairs(k, ar);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
