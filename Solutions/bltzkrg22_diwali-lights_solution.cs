using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private const int Modulo = 100000;
    /*
    * Each light bulb can be in one of two states - on/off.
    * If there are n bulbs, then there are 2^n possible setups.
    * We also must exclude the setup in which all bulbs are off.
    * 
    * The answer is therefore 2^n - 1 (Modulo 100000).
    */

    public static long lights(int n)
    {
        var answer = Power(2, n) - 1L;
        if (answer == -1L)
        {
            answer = Modulo - 1;
        }
        return answer;
    }

    private static long Power(long powBase, int powExponent)
    {
        powBase %= Modulo;

        var answer = 1L;
        while (powExponent > 0)
        {
            if ((powExponent & 1) == 0)
            {
                powBase *= powBase;
                powBase %= Modulo;
                powExponent >>= 1;
            }
            else
            {
                answer *= powBase;
                answer %= Modulo;
                powExponent--;
            }
        }

        return answer % Modulo;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            long result = Result.lights(n);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
