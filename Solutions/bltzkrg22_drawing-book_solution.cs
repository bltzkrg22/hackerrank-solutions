using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static int PageCount(int totalPageCount, int expectedPage)
        => Math.Min(DistanceFromStart(expectedPage), DistanceFromEnd(expectedPage, totalPageCount));

    private static int DistanceFromStart(int pageNumber) 
        => (NextOddNumber(pageNumber) - 1) / 2;

    private static int DistanceFromEnd(int pageNumber, int totalPageCount) 
        => (NextOddNumber(totalPageCount) - NextOddNumber(pageNumber)) / 2;

    private static int NextOddNumber(int n) => n | 1;
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        int p = Convert.ToInt32(Console.ReadLine().Trim());

        int result = Result.PageCount(n, p);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
