using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;


class Result
{
    public static int EasyGCD(List<int> list, int max)
    {
        /*
        * Solution strategy:
        * (1) Find common prime factors of all the numbers from list.
        * (2) We know that the solution is a multiple of some prime numer. So for each
        *     common prime, we find the greatest multiple not greater from the given
        *     max value, and choose the maximal from those multiples.
        */


        // Optimally we want to find a number with the smallest count of distinct prime
        // factors. As a heuristic, we choose the smallest number from list overall.
        int smallest = list.Min();

        // We initialize the common prime factor set to prime factorization of `smallest`
        HashSet<int> candidatePrimes = PrimeFactors(smallest);


        // We check which of the primes are common for all numbers from list
        foreach (var number in list)
        {
            HashSet<int> rejected = new HashSet<int>();
            foreach (var prime in candidatePrimes)
            {
                if (number % prime != 0)
                {
                    rejected.Add(prime);
                }
            }
            candidatePrimes.ExceptWith(rejected);
        }

        // We find the greatest number smaller or equal to `max`, which is a multiple of
        // some common prime factor
        int largestCandidateMultiple = int.MinValue;
        foreach (var prime in candidatePrimes)
        {
            int currentMultiple = max - max % prime;
            largestCandidateMultiple = Math.Max(largestCandidateMultiple, currentMultiple);
        }

        return largestCandidateMultiple;
    }


    private static HashSet<int> PrimeFactors(int n)
    {
        var answer = new HashSet<int>();

        PrimeFactorRoutine(ref n, 2, answer);
        PrimeFactorRoutine(ref n, 3, answer);

        int k = 1;
        int lesserCandidate = 6 * k - 1;
        int greaterCandidate = 6 * k + 1;

        int lastCheck = IntegerSquareRoot(n);

        while (lesserCandidate <= lastCheck)
        {
            PrimeFactorRoutine(ref n, lesserCandidate, answer);
            PrimeFactorRoutine(ref n, greaterCandidate, answer);

            k++;
            lesserCandidate = 6 * k - 1;
            greaterCandidate = 6 * k + 1;
        }

        if (n > 1)
        {
            answer.Add(n);
        }
        
        return answer;
    }

    private static void PrimeFactorRoutine(ref int n, int prime, ICollection<int> answer)
    {
        while (n % prime == 0)
        {
            n /= prime;
            answer.Add(prime);
        }
    }


    // Borrowed from https://rosettacode.org/wiki/Isqrt_(integer_square_root)_of_X#C.23
    // Calculates the largest integer that is equal to or lower than square root of number
    private static int IntegerSquareRoot(int number)
    {
        int q = 1;
        int r = 0;
        int t;

        while (q <= number)
        { 
            q <<= 2; 
        }

        while (q > 1)
        {
            q >>= 2;
            t = number - r - q;
            r >>= 1;
            if (t >= 0)
            { 
                number = t;
                r += q;
            }
        }
        return r;
    }
}


class Solution
{
    public static void Main(string[] args)
    {
        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int k = Convert.ToInt32(firstMultipleInput[1]);

        List<int> A = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(ATemp => Convert.ToInt32(ATemp)).ToList();

        int answer = Result.EasyGCD(A, k);

        Console.WriteLine(answer);
    }
}
