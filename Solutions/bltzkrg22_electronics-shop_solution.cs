using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class Solution 
{
    static int CalculateMoneySpent(List<int> keyboards, List<int> drives, int budget) 
    {
        // Initialize to the value returned when no solution is found
        int answer = -1;

        keyboards.Sort();
        drives.Sort();

        int keyboardIndex = 0;
        int driveIndex = drives.Count - 1;

        while(keyboardIndex <= keyboards.Count - 1 && driveIndex >= 0)
        {
            int potentialSum = keyboards[keyboardIndex] + drives[driveIndex];
            if (potentialSum <= budget)
            {
                answer = Math.Max(potentialSum, answer);
                keyboardIndex++;
            }
            else
            {
                driveIndex--;
            }
        }

        return answer;
    }

    static void Main(string[] args) 
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] bnm = Console.ReadLine().Split(' ');

        int b = Convert.ToInt32(bnm[0]);

        int n = Convert.ToInt32(bnm[1]);

        int m = Convert.ToInt32(bnm[2]);

        List<int> keyboards = Console.ReadLine().Split(' ').Select(s => Int32.Parse(s)).ToList();

        List<int> drives = Console.ReadLine().Split(' ').Select(s => Int32.Parse(s)).ToList();

        int moneySpent = CalculateMoneySpent(keyboards, drives, b);

        textWriter.WriteLine(moneySpent);

        textWriter.Flush();
        textWriter.Close();
    }
}
