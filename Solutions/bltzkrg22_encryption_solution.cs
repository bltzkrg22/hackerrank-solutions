using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static string encryption(string s)
    {

        //Remove spaces from s, and convert the result to a char array.
        var noSpaces = String.Join("", s.Split(' ')).ToCharArray();

        int numberOfCols;
        int numberOfRows = Convert.ToInt32(Math.Floor(Math.Sqrt(Convert.ToDouble(noSpaces.Length))));

        /*
        * Notice that Floor(L) = Ceil(L) - 1 if L is not an integer, or Floor(L) = Ceil(L) if L is an integer.
        * To find the required numberOfRows and numberOfCols in the array, we will just check pairs
        * (x,x), (x,x+1), (x+1,x+1)
        * where x is Floor(Sqrt(noSpaces.Length)), and choose the first "rectangle" that can fit whole noSpaces string.
        * Also (x+1)*(x+1), i.e. the final rectangle is guaranteed to be large enough!
        */
        if (numberOfRows * numberOfRows >= noSpaces.Length)
        {
            numberOfCols = numberOfRows;
        }
        else if (numberOfRows * (numberOfRows + 1) >= noSpaces.Length)
        {
            numberOfCols = numberOfRows + 1;
        }
        else // if ( (numberOfRows + 1) * (numberOfRows + 1) >= noSpaces.Length )
        {
            numberOfCols = numberOfRows + 1;
            numberOfRows = numberOfRows + 1;
        }


        var answer = new StringBuilder(s.Length);

        /*
        * Take all characters from column 0 in all rows and append them to the answer. Then add a space. Then
        * take all characters from column 1 in all rows etc.
        *
        * If the try to access a character at index greater than noSpaces.Length, then skip (this will happen
        * if the final row has empty spaces).
        *
        * Character at column x and row y of the "encryption matrix" is the character at index [x + y * numberOfCols]
        * in the noString char array!
        */
        for (int i = 0; i < numberOfCols; i++)
        {
            for (int j = 0; j < numberOfRows; j++)
            {
                try
                {
                    answer.Append( noSpaces[i + j * numberOfCols] );
                }
                catch (IndexOutOfRangeException)
                {
                    // Do nothing. If we access a character outside of noSpaces char array,
                    // it is supposed to be an empty space.
                    // throw;
                }
            }

            answer.Append(" ");
        }

        // Remove the final space!
        answer.Remove( answer.Length - 1 ,1);

        return answer.ToString();
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s = Console.ReadLine();

        string result = Result.encryption(s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
