using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static int equalizeArray(List<int> arr)
    {
        if (arr.Count == 0)
        {
            return 0;
        }

        // The frequency of most frequent number. We don't actually care about
        // the actual number, only its frequency!
        int highestFrequency = arr.GroupBy(value => value)
                                  .OrderByDescending(grouping => grouping.Count())
                                  .Select(grouping => grouping.Count())
                                  .First();

        return arr.Count - highestFrequency;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        int result = Result.equalizeArray(arr);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
