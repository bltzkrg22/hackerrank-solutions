using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

// https://en.wikipedia.org/wiki/Euler%27s_criterion

class Result
{
    public static string solve(int a, int m)
    {
        if (m == 2 || a == 0)
        {
            return "YES";
        }

        return LegendreSymbol(a, m) == 1 ? "YES" : "NO";
    }

    private static int LegendreSymbol(int a, int prime)
    {
        // It is assumeed that input is prime! There is no validation.

        return FastExponentiationModulo(a, (prime - 1) / 2, prime);
    }

    private static int FastExponentiationModulo(int @base, int exponent, int mod)
    {   
        long result = 1; 
        long currentPower = @base;

        while (exponent > 0)
        {
            if ((exponent & 1) == 1)
            {
                result *= currentPower;
                result %= mod;
            }

            currentPower *= currentPower;
            currentPower %= mod;
            exponent >>= 1;
        }

        return (int)result;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int a = Convert.ToInt32(firstMultipleInput[0]);

            int m = Convert.ToInt32(firstMultipleInput[1]);

            string result = Result.solve(a, m);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
