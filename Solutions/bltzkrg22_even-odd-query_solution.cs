using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static List<string> EvenOddQuery(List<int> arr, List<List<int>> queries)
    {
        const string Even = "Even";
        const string Odd = "Odd";

        var answer = new List<string>();

        foreach (var query in queries)
        {
            int x = query[0];
            int y = query[1];


            // Input logically is one-indexed, hence -1 below
            int arrayElement = arr[x - 1];
            
            // We don't want to take a value from outside of array; arrayNext is 
            // initialized to a value that is never expected as a valid input.
            int arrayNext = -1;
            try { arrayNext = arr[x]; }
            catch (ArgumentOutOfRangeException) { /* Do nothing */}

            /*
            * If the next number in array is 0 and x != y, then the answer will be equal to 
            * arrayElement^0 == 1 which is odd. 
            * 
            * But if the next number is 0 AND x != y, then condition `if(x>y) return 1;`
            * is evaluated first and we the answer is equal to arrayElement^1.
            * 
            * If the next number is not 0, then the answer will be just arrayElement
            * to some power, where exponent is a positive integer. In that case 
            * the parity of the answer will be the same as parity of arrayElement.
            */

            if (arrayNext == 0 && x != y)
            {
                answer.Add(Odd);
            }
            else if (arrayElement % 2 == 0)
            {
                answer.Add(Even);
            }
            else
            {
                answer.Add(Odd);
            }
        }

        return answer;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int arrCount = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        List<List<int>> queries = new List<List<int>>();

        for (int i = 0; i < q; i++)
        {
            queries.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(queriesTemp => Convert.ToInt32(queriesTemp)).ToList());
        }

        List<string> result = Result.EvenOddQuery(arr, queries);

        textWriter.WriteLine(String.Join("\n", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
