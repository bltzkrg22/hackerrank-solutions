using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * We don't care about exact number of loaves a person posesses, but the remainder of
    * than number mod 2. 0 will mean an even number, and 1 will mean an odd number.
    * 
    * Consider such a starting array: [0 1 0 0 0 1 1 1].
    * We want to change it so that it contains only zeroes. With one operation we can
    * toggle (flip) two adjacent numbers, for example we can flip the first and second:
    * [1 0 0 0 0 1 1 1].
    * 
    * In each step we can:
    * a) change two adjacent ones to zeroes: [... 1 1 ...] => [... 0 0 ...]
    * b) change two adjacent zeroes to ones: [... 0 0 ...] => [... 1 1 ...]
    * c) "swap" positions of adjacent one and zero: [... 0 1 ...] => [... 1 0 ...]
    * 
    * I won't provide a proof that such solution is minimal - but the algorithm is:
    * 1. We scan the input array from left to right, and find the first one.
    * 2. We change the internal status flag from closed to open.
    * 3. We find the next one in the array.
    * 4. We calculate the distance d between those ones. We can change those ones to zeroes 
    *    in d operations (i.e. giving out 2*d loaves), like that:
    *    [1 0 0 1] => [0 1 0 1] => [0 0 1 1] => [0 0 0 0]
    *    Initial distance was 3, in 3 operations we change two ones to zeroes.
    * 5. We change the internal status flag from open to closed, and repeat.
    * 
    * If after scanning whole array status flag is closed, we return the number of loaves
    * given out. However, if the status flag is open, then it means that it is impossible
    * to make all numbers even, so we return "NO".
    */
    private const string FailureMessage = "NO";
   
    // "Readable" names instead of true/false for a two-state variable
    private const bool Open = true;
    private const bool Closed = false;

    public static string fairRations(List<int> B)
    {
        int loavesGivenOut = 0;

        // Initial status is closed (you can also think about it as "not open")
        var status = Closed;
        int lastOpenPosition = -1;

        for (int currentPosition = 0; currentPosition < B.Count; currentPosition++)
        {
            int remainder = B[currentPosition] % 2;

            if (remainder == 1)
            {
                if (status == Closed)
                {
                    status = Open;
                    lastOpenPosition = currentPosition;
                }
                else // (status == OpenCloseStatus.Open)
                {
                    status = Closed;
                    loavesGivenOut += 2 * (currentPosition - lastOpenPosition);
                    // lastOpenPosition = -1;
                }
            }

            // If remainder == 0, we do nothing
        }

        return status == Closed ? loavesGivenOut.ToString() : FailureMessage;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int N = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> B = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(BTemp => Convert.ToInt32(BTemp)).ToList();

        string result = Result.fairRations(B);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
