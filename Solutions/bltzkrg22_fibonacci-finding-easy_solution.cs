using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    const int Modulo = 1000000007;

    public static int solve(int a, int b, int n)
    {
        if (n == 0)
        {
            return a;
        }
        else if (n == 1)
        {
            return b;
        }

        /*
        *  [ F_{ n } ]   [ 1   1 ]   [ F_{n-1} ]   [ 1   1 ]^(n-1)    [ F_{ 1 } ] 
        *  [ F_{n-1} ] = [ 1   0 ] * [ F_{n-2} ] = [ 1   0 ]       *  [ F_{ 0 } ] 
        *
        *  F_{n} = a[1,1] * F_{1} + a[1,2] * F_{0}
        */

        var fiboMatrix = new int[2,2];

        fiboMatrix = MatrixExponent( n - 1 );

        return (int)( ( (long)fiboMatrix[0,0] * b  +  (long)fiboMatrix[0,1] * a  )  % Modulo);

    }


    public static int[,] MatrixExponent( int exponent )
    {
        // Method is supposed to be called only when exponent >= 1. There is no validation later on.

        var fiboBaseMatrix = new int[2,2];
        fiboBaseMatrix[0,0] = 1;
        fiboBaseMatrix[0,1] = 1;
        fiboBaseMatrix[1,0] = 1;
        fiboBaseMatrix[1,1] = 0;

        /*
        * The fastest way to calculate n-th power of a number or a matrix is to represent n as a sum of powers of 2,
        * for example: 14 = 8 + 4 + 2. Then we calculate and memorize: A, A^2, A^4, A^8, and finally multiply
        * A^8 * A^4 * A^2 to obtain A^14.
        */

        int greatestBaseTwoFactor = 0;
        int power = 1; // 2^0

        /*
        * Here we "count" find the how many bits have the binary representation of exponent, which will also be the highest
        * power of 2 in the factorization.
        */

        while (power <= exponent)
        {
            greatestBaseTwoFactor++;
            power = power << 1;
        }
        greatestBaseTwoFactor--;

        /*
        * Now create a list, which will include A at index 0, A^2 at index 1, A^4 at index 2 etc.
        * A is here equal to base Fibonacci matrix, which we get from observation:
        *                       [ F_{n+2} ]   [ 1   1 ]   [ F_{n+1} ]
        *                       [ F_{n+1} ] = [ 1   0 ] * [ F_{ n } ]
        */

        // The following lines are probably inefficient, but somehow they create a *deep*
        // copy of fiboBaseMatrix, which is all that we need
        var matrices = new List<int[,]>() { new int[2,2] { {0,0}, {0,0} } } ;
        matrices[0] = fiboBaseMatrix.Clone() as int[,];

        for (int i = 1; i <= greatestBaseTwoFactor; i++)
        {
            matrices.Add(  MatrixProduct(matrices[i-1], matrices[i-1]) );
        }


        /*
        * Now we calculate the A^n. We check each bit of binary representation of exponent n, and use it to determine which
        * elements from list should be included in multiplication, for example for n = 13 = 0b01101, therefore 13 = 1 + 4 + 8
        * and we multiply matrices[0] = A^1, matrices[2] = A^4, matrices[3] = A^8
        */

        int exponentCopy = exponent;

        var powerMatrix = new int[2,2] { {1,0}, {0,1} }; // Initialize to unit matrix (ones on main diagonal)

        for (int i = 0; i <= greatestBaseTwoFactor; i++)
        {
            if (exponentCopy % 2 == 1)
            {
                /*
                * Note that in MatrixProduct method all elements of output matrix are wrapped modulo 1000000007!
                */
                powerMatrix = MatrixProduct(powerMatrix, matrices[i] );
            }
            exponentCopy = exponentCopy >> 1;
        }

        return powerMatrix;
    }

    public static int[,] MatrixProduct(int[,] matrA, int[,] matrB)
    {
        var resultMatrix = new int[2,2];

        /*
        * Since the problem statement says that we want the answer modulo 1000000007, we wrap the result of
        * matrix multiplication. 
        */

        resultMatrix[0,0] = (int)(((long) matrA[0,0] * matrB[0,0] + (long) matrA[0,1] * matrB[1,0]) % Modulo);
        resultMatrix[0,1] = (int)(((long) matrA[0,0] * matrB[0,1] + (long) matrA[0,1] * matrB[1,1]) % Modulo);
        resultMatrix[1,0] = (int)(((long) matrA[1,0] * matrB[0,0] + (long) matrA[1,1] * matrB[1,0]) % Modulo);
        resultMatrix[1,1] = (int)(((long) matrA[1,0] * matrB[0,1] + (long) matrA[1,1] * matrB[1,1]) % Modulo);

        return resultMatrix;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int a = Convert.ToInt32(firstMultipleInput[0]);

            int b = Convert.ToInt32(firstMultipleInput[1]);

            int n = Convert.ToInt32(firstMultipleInput[2]);

            int result = Result.solve(a, b, n);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
