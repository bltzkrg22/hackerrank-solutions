using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;
using System.Numerics;

class Result
{
    public static BigInteger fibonacciModified(int t1, int t2, int n)
    {
        BigInteger answer = new BigInteger();
        BigInteger oneBehind = new BigInteger(t2);
        BigInteger twoBehind = new BigInteger(t1);

        while (n > 2)
        {
            // Calculate the next number in sequence
            answer = twoBehind + oneBehind * oneBehind;
            n--;

            // Prepare the dynamic variables for the next loop iteration
            twoBehind = oneBehind;
            oneBehind = answer;
        }
        
        return answer;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int t1 = Convert.ToInt32(firstMultipleInput[0]);

        int t2 = Convert.ToInt32(firstMultipleInput[1]);

        int n = Convert.ToInt32(firstMultipleInput[2]);

        BigInteger result = Result.fibonacciModified(t1, t2, n);

        textWriter.WriteLine(result.ToString());

        textWriter.Flush();
        textWriter.Close();
    }
}
