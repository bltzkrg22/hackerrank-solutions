using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static long FillingJars(int n, List<List<int>> operations)
    {
        decimal averageCounter = 0;

        foreach (var operation in operations)
        {
            int firstIndex = operation[0];
            int lastIndex = operation[1];
            int incrementValue = operation[2];
            averageCounter += (decimal)(lastIndex - firstIndex + 1) * incrementValue / n;
        }

        return (long)Math.Floor(averageCounter);
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int m = Convert.ToInt32(firstMultipleInput[1]);

        List<List<int>> operations = new List<List<int>>();

        for (int i = 0; i < m; i++)
        {
            operations.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(operationsTemp => Convert.ToInt32(operationsTemp)).ToList());
        }

        long result = Result.FillingJars(n, operations);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
