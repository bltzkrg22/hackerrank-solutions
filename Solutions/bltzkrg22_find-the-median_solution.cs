using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private const int MaxElement = 10_000;
    private const int MinElement = -10_000;

    public static int findMedian(List<int> arr)
    {
        var frequencyTable = new Dictionary<int, int>(){};

        for (int i = MinElement; i <= MaxElement; i++)
        {
            frequencyTable[i] = 0;
        }

        int medianIndex = (arr.Count + 1) / 2;

        for (int i = 0; i < arr.Count; i++)
        {
            frequencyTable[ arr[i] ] += 1;
        }

        int currentTableIndex = MinElement;
        int samplesCounted = 0;
        while (samplesCounted < medianIndex)
        {
            samplesCounted += frequencyTable[currentTableIndex];
            currentTableIndex++;
        }
        currentTableIndex--;

        return currentTableIndex;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        int result = Result.findMedian(arr);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
