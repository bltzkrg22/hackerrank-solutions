using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution {

    /* BEGINNING OF SOLUTION */
    
    static int findShortest(int graphNodes, int[] graphFrom, int[] graphTo, long[] ids, long val) 
    {
        // Create the adjacency list for each node
        var adjacencyList = new Dictionary<int, HashSet<int>>();
        for (int i = 0; i < graphFrom.Length; i++)
        {
            int nodeA = graphFrom[i];
            int nodeB = graphTo[i];
            AddNodePairToAdjacencyList(nodeA, nodeB, adjacencyList);
        }

        // Startup the loop with all nodes of color val
        var distanceToColor = new Dictionary<int, int>(); //<int nodeID, int distance>

        var currentlyAnalyzedNodes = new HashSet<int>( 
            ids.Select((c, i) => new {Color = i, Index = i}).Where(pair => pair.Color == val).Select(pair => pair.Index)
        );
        foreach (var node in currentlyAnalyzedNodes)
        {
            distanceToColor[node] = 0;
        }

        HashSet<int> nextAnalyzedNodes;

        // Now we do the classic flood-fill
        int currentDistance = 0;

        while (currentlyAnalyzedNodes.Count > 0)
        {
            currentDistance += 1;
            nextAnalyzedNodes = new HashSet<int>();


            foreach (var node in currentlyAnalyzedNodes)
            {
                if (!adjacencyList.ContainsKey(node))
                {
                    continue;
                }
                foreach (var neighbour in adjacencyList[node])
                {
                    if (distanceToColor.ContainsKey(neighbour))
                    {
                        continue;
                    }
                    else
                    {
                        distanceToColor[neighbour] = currentDistance;
                        nextAnalyzedNodes.Add(neighbour);
                        if (GetColor(neighbour, ids) == val)
                        {
                            return currentDistance;
                        }
                    }
                }
            }

            currentlyAnalyzedNodes = nextAnalyzedNodes;
        }

        return -1;
    }
    
    private static void AddNodePairToAdjacencyList(int nodeA, int nodeB, Dictionary<int, HashSet<int>> adjacencyList)
    {
        if (!adjacencyList.ContainsKey(nodeA))
        {
            adjacencyList[nodeA] = new HashSet<int>(){ nodeB };
        }
        else
        {
            adjacencyList[nodeA].Add(nodeB);
        }

        if (!adjacencyList.ContainsKey(nodeB))
        {
            adjacencyList[nodeB] = new HashSet<int>(){ nodeA };
        }
        else
        {
            adjacencyList[nodeB].Add(nodeA);
        }
    }
    
    private static long GetColor(int nodeID, long[] ids) => ids[nodeID - 1];
    
    /* END OF SOLUTION */

    static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] graphNodesEdges = Console.ReadLine().Split(' ');
        int graphNodes = Convert.ToInt32(graphNodesEdges[0]);
        int graphEdges = Convert.ToInt32(graphNodesEdges[1]);

        int[] graphFrom = new int[graphEdges];
        int[] graphTo = new int[graphEdges];

        for (int i = 0; i < graphEdges; i++) {
            string[] graphFromTo = Console.ReadLine().Split(' ');
            graphFrom[i] = Convert.ToInt32(graphFromTo[0]);
            graphTo[i] = Convert.ToInt32(graphFromTo[1]);
        }

        long[] ids = Array.ConvertAll(Console.ReadLine().Split(' '), idsTemp => Convert.ToInt64(idsTemp))
        ;
        int val = Convert.ToInt32(Console.ReadLine());

        int ans = findShortest(graphNodes, graphFrom, graphTo, ids, val);

        textWriter.WriteLine(ans);

        textWriter.Flush();
        textWriter.Close();
    }
}
