using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution 
{
    private static int FlatlandSpaceStations(int numberOfCities, IReadOnlyList<int> stations) 
    {
        // Sort the stations by their coordinate.
        List<int> orderedStations = stations.OrderBy(pos => pos).ToList();

        int maxDistance;
        // Initialize the answer to distances from boundary cities
        // to their nearest stations.
        maxDistance = Math.Max(
            (numberOfCities - 1) - orderedStations.Last(),
            orderedStations.First() - 0
        );

        // Special degenerative case when there is only one station.
        if (stations.Count == 1)
        {
            return maxDistance;
        }

        // Calculate the max distance between every two consecutive stations, and then
        // the distance of the middle city between those stations.
        for (int i = 0; i < orderedStations.Count - 1; i++)
        {
            int candidateDistance = (orderedStations[i + 1] - orderedStations[i]) / 2;
            maxDistance = Math.Max(maxDistance, candidateDistance);
        }

        // Return the answer.
        return maxDistance;
    }

    static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] nm = Console.ReadLine().Split(' ');

        int n = Convert.ToInt32(nm[0]);

        int m = Convert.ToInt32(nm[1]);

        int[] c = Array.ConvertAll(Console.ReadLine().Split(' '), cTemp => Convert.ToInt32(cTemp))
        ;
        int result = FlatlandSpaceStations(n, c);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
