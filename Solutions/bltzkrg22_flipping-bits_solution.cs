using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private const long ThirtyTwoOnes = 4294967295; // 2^32 - 1, i.e. 1111...11 x32 times

    /*
    * Just remember that any bit XOR 1 = flipped bit, and any bit XOR 0 = unchanged bit.
    * So to get the result we need to XOR the input number with the number is represented
    * in binary by a sequence of 32 ones.
    */
    public static long flippingBits(long n) => n ^ ThirtyTwoOnes;
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            long n = Convert.ToInt64(Console.ReadLine().Trim());

            long result = Result.flippingBits(n);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
