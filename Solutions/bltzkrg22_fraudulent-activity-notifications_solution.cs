using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    public static decimal Median(int[] countTable, int elementCount)
    {
        int medianIndex;
        decimal medianValue;
        int samplesCounted = 0;
        int currentTableIndex = 0;

        // When the number of samles is odd the median is a single value
        if (elementCount % 2 == 1)
        {
            medianIndex = (elementCount + 1) / 2;

            while (samplesCounted < medianIndex)
            {
                samplesCounted += countTable[currentTableIndex];
                currentTableIndex++;
            }
            currentTableIndex--;

            medianValue = currentTableIndex;
        }
        // When the number of samles is even the median is an average of two values
        else
        {
            // This is the index of the smaller of two numbers we are looking for
            medianIndex = (elementCount / 2);

            while (samplesCounted < medianIndex)
            {
                samplesCounted += countTable[currentTableIndex];
                currentTableIndex++;
            }
            currentTableIndex--;
            if (samplesCounted > medianIndex)
            {
                medianValue = currentTableIndex;
            }
            else
            {
                medianValue = currentTableIndex * 0.5M;

                // Find the next non zero entry in countTable

                currentTableIndex++;
                while (countTable[currentTableIndex] == 0)
                {
                    currentTableIndex++;
                }

                medianValue += currentTableIndex * 0.5M;
            }
        }

        return medianValue;
    }

    public static int activityNotifications(List<int> expenditure, int d)
    {
        int[] rollingData = new int[201];
        foreach (var number in expenditure.GetRange(0, d))
        {
            // From input constraints we know that expenditure is in range from 0 to 200
            rollingData[number] += 1;
        }


        decimal rollingMedian;

        int noticeCounter = 0;

        int currentDayIndex = d;

        while (currentDayIndex < expenditure.Count)
        {
            rollingMedian = Median(rollingData, d);
            if (expenditure[currentDayIndex] >= rollingMedian + rollingMedian)
            {
                noticeCounter++;
            }

            rollingData[expenditure[currentDayIndex]] += 1;
            rollingData[expenditure[currentDayIndex - d]] -= 1;
            currentDayIndex++;
        }

        return noticeCounter;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int d = Convert.ToInt32(firstMultipleInput[1]);

        List<int> expenditure = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(expenditureTemp => Convert.ToInt32(expenditureTemp)).ToList();

        int result = Result.activityNotifications(expenditure, d);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
