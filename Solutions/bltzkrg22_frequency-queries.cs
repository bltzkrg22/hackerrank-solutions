using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution {

    public static void SafeAddToDictionary( int number, Dictionary<int, int> dict, int[] frequencyCount )
    {
        if (dict.ContainsKey(number))
        {
            dict[number] += 1;
            frequencyCount[dict[number]] += 1;
            frequencyCount[dict[number]-1] -= 1;
        }
        else
        {
            dict[number] = 1;
            frequencyCount[dict[number]] += 1;
        }
    }

    public static void SafeRemoveFromDictionary( int number, Dictionary<int, int> dict, int[] frequencyCount )
    {
        if (dict.ContainsKey(number))
        {
            dict[number] -= 1;
            frequencyCount[dict[number] + 1] -= 1;
            frequencyCount[dict[number]] += 1;
            if (dict[number] == 0)
            {
                dict.Remove(number);
            }
        }
    }


    // Complete the freqQuery function below.
    static List<int> freqQuery(List<List<int>> queries) {
        var bufferDict = new Dictionary<int, int>();
        var frequencyCount = new int[100000 + 1];
        //frequencyCount[0] = 0;

        var outputList = new List<int>();

        foreach (var query in queries)
        {
            switch (query[0])
            {
                case 1:
                    SafeAddToDictionary( query[1], bufferDict, frequencyCount);
                    break;
                case 2:
                    SafeRemoveFromDictionary( query[1], bufferDict, frequencyCount);
                    break;
                case 3:
                    int answer;
                    if (query[1] > 100000)
                    {
                        answer = 0;
                    }
                    else
                    {
                        answer = frequencyCount[ query[1] ] > 0 ? 1 : 0;
                    }
                    outputList.Add(answer);
                    break;
                default:
                    // this should never be reached with valid input data
                    break;
            }
        }

        return outputList;
    }
    

    static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        List<List<int>> queries = new List<List<int>>();

        for (int i = 0; i < q; i++) {
            queries.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(queriesTemp => Convert.ToInt32(queriesTemp)).ToList());
        }

        List<int> ans = freqQuery(queries);

        textWriter.WriteLine(String.Join("\n", ans));

        textWriter.Flush();
        textWriter.Close();
    }
}
