using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * String s will meet the "funny" requirement if *the list of absolute differences
    * is a palindrome*. So we check if the first difference is equal to the last; if the 
    * second is equal to the second to last, etc.
    */
    public static string funnyString(string s)
    {
        int checkRange = (s.Length - 1) / 2;

        for (int i = 1; i <= checkRange; i++)
        {
            int frontDifference = Math.Abs(s[i] - s[i-1]);
            int backDifference = Math.Abs(s[s.Length-i] - s[s.Length-i-1]);

            if (frontDifference != backDifference)
            {
                return "Not Funny";
            }
        }

        return "Funny";
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            string s = Console.ReadLine();

            string result = Result.funnyString(s);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
