using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

/*
* The following LINQ expression can be explained as:
* If there is at most one character, that is seen an odd number of times, then an 
* anagram which is a palindrome exists, and we print "YES". Otherwise, we print "NO".
*/

class Result
{
    public static string GameOfThrones(string s) 
        => s.GroupBy(@char => @char)
            .Select(grouping => grouping.Count())
            .Where(count => (count % 2) == 1)
            .Count() <= 1 ? "YES" : "NO";
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s = Console.ReadLine();

        string result = Result.GameOfThrones(s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
