using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    // fullSet is a hashset with all potential "gemstones" - all chars from a to z
    private static readonly HashSet<char> fullSet = new HashSet<char>
        ( Enumerable.Range('a', 26).Select(x => (char)x) );

    /*
    * A mineral will be a gemstone if and only if it is present in all rocks.
    * So we need to calculate the set intersection of all rocks.
    * https://en.wikipedia.org/wiki/Intersection_(set_theory)
    * https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.iset-1.intersectwith
    */ 
    public static int gemstones(List<string> arr)
    {
        var potentialGemstones = new HashSet<char>(fullSet);
        List<string> rocks = arr;

        // Special case: if the input is empty, there are no gemstones
        if (rocks.Count == 0)
        {
            return 0;
        }

        foreach (string rock in rocks)
        {
            var mienralsInRock = new HashSet<char>(rock.ToList());
            potentialGemstones.IntersectWith(mienralsInRock);
        }

        return potentialGemstones.Count;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<string> arr = new List<string>();

        for (int i = 0; i < n; i++)
        {
            string arrItem = Console.ReadLine();
            arr.Add(arrItem);
        }

        int result = Result.gemstones(arr);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
