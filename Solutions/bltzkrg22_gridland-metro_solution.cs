using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;


/*
* The method template has a wrong signature!
* By default it is: 
*   public static int gridlandMetro(int n, int m, int k, List<List<int>> track)
* even though the expected answer is of long type! :(
*/

class Result
{
    /*
    * MyTuple class is a workaround - in .NET 5 we could simply create such a list: List<(int, bool)>
    * Unfortunately, HackerRank's current .NET version throws an error.
    */
    public class MyTuple
    {
        public int Position {get; set;}
        public bool IsTrackStart {get; set;}
    }

    public static long gridlandMetro(int n, int m, int k, List<List<int>> track)
    {
        int rowCount = n;
        int colCount = m;
        var trackPoints = new Dictionary<int, List<MyTuple>>(); 
            // key = row number, value = list of points of interest (track starts/ends)


        /*
        * There can be 10^9 rows, but only 1000 tracks in whole grid.
        * We will create a dictionary `trackPoints`, that for each row with a track will hold a *sorted*
        * list of all points of interest, i.e. starts and ends of tracks. (Sorting will be done immediately
        * before counting free spaces).
        */


        foreach (var singleTrack in track)
        {
            int row = singleTrack[0];
            int start = singleTrack[1];
            int end = singleTrack[2];

            SafeAddToDictionary(row, new MyTuple() { Position = start, IsTrackStart = true }, trackPoints);
            SafeAddToDictionary(row, new MyTuple() { Position = end, IsTrackStart = false }, trackPoints);
        }

        long totalFreeSpaceCounter = 0;
        foreach (var rowNumber in trackPoints.Keys)
        {
            trackPoints[rowNumber].Sort( new MyComparer() );

            int freeSpacesInRow = CountFreeSpacesInRow(1, colCount, trackPoints[rowNumber]);

            totalFreeSpaceCounter += freeSpacesInRow;
        }

        totalFreeSpaceCounter += (long)(rowCount - trackPoints.Count) * colCount;
    
        return totalFreeSpaceCounter;
    }

    /// <summary>
    /// Counts the number of squares in a row, which are not covered by tracks. It is assumed that the tracks
    /// are "balanced", i.e. each track start has a corresponding end with a greater index,
    /// and there is an equal number of starts and ends.
    /// </summary>
    /// <param name="start">Index of first square in row (is row zero- or one- indexed?)</param>
    /// <param name="end">Index of last square in row</param>
    /// <param name="trackPoints">List of all track starts/ends</param>
    private static int CountFreeSpacesInRow(int start, int end, List<MyTuple> trackPoints)
    {
        /*
         * This is similar to the classic "balanced bracket pairs" problem, with a subtle difference
         * that while such thing is incorrect for brackets:
         * ( xxx [ xxx ) xxx ]
         * it is valid for the tracks.
        */

        int freeSpaceCounter = 0;
        int openedTracks = 0;

        // Imagine that immediately before the start of our row there is a square which is not
        // free, an imaginary "track end" - so called sentinel.
        int lastPoint = start - 1;
        int nextPoint;
        bool nextPointIsStart;

        for (int i = 0; i < trackPoints.Count; i++)
        {
            nextPoint = trackPoints[i].Position;
            nextPointIsStart = trackPoints[i].IsTrackStart;

            // If there are no opened tracks, then until the next point of interest
            // all sqaures are free!
            if (openedTracks == 0)
            {
                freeSpaceCounter += (nextPoint - 1) - lastPoint;
            }

            // Increment openedTracks value if the next point is a track start,
            // and decrement otherwise.
            openedTracks += nextPointIsStart ? 1 : -1;

            lastPoint = nextPoint;
        }

        // We need to include squares after the final track end as well.
        freeSpaceCounter += end - lastPoint;

        return freeSpaceCounter;
    }

    private static void SafeAddToDictionary<U,T>( U key, T value, Dictionary<U, List<T>> dict )
    {
        if (!dict.ContainsKey(key))
        {
            dict[key] = new List<T>(){ value };
        }
        else
        {
            dict[key].Add(value);
        }
    }

     
    // We want to sort the starts and ends of tracks by the column number, so we implement a custom
    // comparer for our "workaround" class.
    public class MyComparer : Comparer<MyTuple>
    {
        public override int Compare(MyTuple A, MyTuple B)
        {
            if (A.Position.CompareTo(B.Position) != 0)
            {
                return A.Position.CompareTo(B.Position);
            }
            else if (A.IsTrackStart == true && B.IsTrackStart == false)
            {
                return -1;
            }
            else if (A.IsTrackStart == false && B.IsTrackStart == true)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }

}


class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int m = Convert.ToInt32(firstMultipleInput[1]);

        int k = Convert.ToInt32(firstMultipleInput[2]);

        List<List<int>> track = new List<List<int>>();

        for (int i = 0; i < k; i++)
        {
            track.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(trackTemp => Convert.ToInt32(trackTemp)).ToList());
        }

        long result = Result.gridlandMetro(n, m, k, track);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
