using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * First of all, we obviously want the house list x to be sorted.
    *
    * The main idea of the soultion is:
    * The first transmitter *must* always cover the first house.
    * Also, the furthest we can place it (so that it still has the first house in range), 
    * the better - worst case it won't reach more houses, best case it will reach more houses in front.
    * 
    * After finding the furthest house that we can place a transmitter, that will still
    * have the first house in range, we find the next house up front that is not in range
    * of the previously placed transmitter.
    *
    * We repeat this loop until we reach the end of the houses list.
    */
    public static int hackerlandRadioTransmitters(List<int> x, int k)
    {
        x.Sort();

        // Queue is unnecessary, but it is super easy to grasp compared to
        // incrementing the house list index.
        var houses = new Queue<int>(x);
        var transmitters = new List<int>();

        while (houses.Count > 0)
        {
            // House locations start from 1, so if LastOrDefault return 0, we know 
            // that list is empty
            int lastTransmitterPlaced = transmitters.LastOrDefault();

            // Here we find the coordinates of the first house with no coverage
            // by checking the distance from the last transmitter placed
            int firstNoCoverage = houses.Dequeue();
            while (firstNoCoverage - lastTransmitterPlaced <= k && lastTransmitterPlaced != 0)
            {
                if (houses.Count == 0)
                {
                    // This means that all houses till the end are in range
                    // of the last transmitter, we may return the answer
                    return transmitters.Count;
                }
                else
                {
                    firstNoCoverage = houses.Dequeue();
                }
            }
            
            // Tentatively we place the next transmitter on the first house with no coverage
            // and then we will make a correction pass
            int nextTransmitterPlace = firstNoCoverage;

            // Set to true to kickstart while loop
            bool isNextInRange = true;
            while (isNextInRange)
            {
                if (houses.Count == 0)
                {
                    // If this is the last house with no coverage, we just
                    // plant a transmitter and finish the job
                    transmitters.Add(nextTransmitterPlace);
                    break;
                }
                else
                {
                    // If we placed the transmitter on the next house, would the first
                    // house with no coverage still be in range?
                    int nextHouse = houses.Peek();

                    // If yes, then it is a better place for our transmitter.
                    if (nextHouse - firstNoCoverage <= k)
                    {
                        nextTransmitterPlace = nextHouse;
                        _ = houses.Dequeue();
                        // At this point isNextInRange is still true
                    }
                    // If not, then the previous place was the optimal one.
                    else
                    {
                        transmitters.Add(nextTransmitterPlace);
                        isNextInRange = false;
                    }
                }

            }
        }

        return transmitters.Count;
    }
}


class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int k = Convert.ToInt32(firstMultipleInput[1]);

        List<int> x = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(xTemp => Convert.ToInt32(xTemp)).ToList();

        int result = Result.hackerlandRadioTransmitters(x, k);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
