using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static string hackerrankInString(string s)
    {
        // This should be implemented with a moving index - queues are 
        // simple to understand though.
        var HrQueue = new Queue<char>("hackerrank".ToCharArray());
        var SQueue = new Queue<char>(s.ToCharArray());

        while (HrQueue.Count > 0 && SQueue.Count > 0)
        {
            char nextToMatch = HrQueue.Peek();
            char nextInString = SQueue.Peek();

            if (nextToMatch == nextInString)
            {
                _ = HrQueue.Dequeue();
                _ = SQueue.Dequeue();
            }
            else
            {
                _ = SQueue.Dequeue();
            }
        }

        return HrQueue.Count == 0 ? "YES" : "NO";
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            string s = Console.ReadLine();

            string result = Result.hackerrankInString(s);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
