using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * We want to maximize the product of two positive integers a and b,
    * given a sum constraint: a + b = k.
    * 
    * Let's assume that a >= b. It can be proven that if there exists such positive 
    * delta, that: a - delta >= b + delta, than the product
    * (a - delta) * (b + delta) increases (compared to a * b),
    * and the sum stays the same - so (a - delta, b + delta) is a "better" solution.
    * 
    *  [(a - delta) * (b + delta)] - a * b = [a * b + delta * (a - b) - delta^2] - a * b
    *  [(a - delta) * (b + delta)] - a * b = delta * (a - b) - delta^2
    *  [(a - delta) * (b + delta)] - a * b = delta * (a - b - delta) 
    * 
    * Since delta is positive, and a - delta >= b + delta >= b, then it follows that
    * [(a - delta) * (b + delta)] - a * b = delta * (a - b - delta) >= 0, i.e.
    * [(a - delta) * (b + delta)] >= a * b
    * 
    * Now, when such delta exists? When the difference between a and b is greater than 1.
    * So optimally, values of a and b must be as close as possible:
    * a = b in case k is even; or a = b + 1 in case k is odd.
    * 
    * Therefore, the maximal product of two numbers that sum to k is equal to:
    * 1) (k / 2)^2, if k is even
    * 2) (k + 1) * (k - 1) / 4, if k is odd
    */

    public static long halloweenParty(int k) 
    {
        if (k % 2 == 0)
        {
            return (long)k / 2 * k / 2;
        }
        else // if (k % 2 == 1)
        {
            return (long)(k + 1) / 2 * (k - 1) / 2;
        }
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int k = Convert.ToInt32(Console.ReadLine().Trim());

            long result = Result.halloweenParty(k);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
