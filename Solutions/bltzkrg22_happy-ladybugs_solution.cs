using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static string happyLadybugs(string b)
    {
        var characterFrequencies = b.Where(character => character != '_')
                    .GroupBy(character => character)
                    .ToDictionary(charFreq => charFreq.Key, charFreq => charFreq.Count());

        // If there is a single ladybug, it will always be unhappy
        if (characterFrequencies.ContainsValue(1))
        {
            return "NO";
        }

        int emptySpaces = b.Where(character => character == '_').Count();

        // If there aren't single ladybugs and there is an empty space, we can always
        // make all ladybugs happy
        if (emptySpaces > 0)
        {
             return "YES";
        }

        // Finally, there is a possibility that there are no empty spaces and we cannot reorder
        // ladybugs, but they are already all happy in their initial order
        for (int i = 1; i < b.Length - 1; i++)
        {
            if (b[i] != b[i - 1] && b[i] != b[i + 1])
            {
                return "NO";
            }
        }

        if (b[0] != b[1] || b[b.Length - 1] != b[b.Length - 2])
        {
            return "NO";
        }

        return "YES";
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int g = Convert.ToInt32(Console.ReadLine().Trim());

        for (int gItr = 0; gItr < g; gItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            string b = Console.ReadLine();

            string result = Result.happyLadybugs(b);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
