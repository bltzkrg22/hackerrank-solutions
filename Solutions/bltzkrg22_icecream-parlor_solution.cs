using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{    public static List<int> icecreamParlor(int m, List<int> arr)
    {
        int money = m;
        var previousPrices = new Dictionary<int, int>() { };
            // key = price, value = index

        for (int index = 0; index < arr.Count; index++)
        {
            // int currentPrice = arr[i];
            int complementaryPrice = money - arr[index];
            if (previousPrices.ContainsKey(complementaryPrice))
            {
                // Note that internally arrays are zero-indexed, while in the expected output
                // arrays are one-indexed
                return new List<int>() { previousPrices[complementaryPrice] + 1, index + 1};            }
            else
            {
                previousPrices[arr[index]] = index;
            }
        }

        // According to problem description, the solution always exists though.
        throw new ApplicationException("Solution was not found.");
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int m = Convert.ToInt32(Console.ReadLine().Trim());

            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

            List<int> result = Result.icecreamParlor(m, arr);

            textWriter.WriteLine(String.Join(" ", result));
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
