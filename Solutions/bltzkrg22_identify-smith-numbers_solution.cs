using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private static List<int> PrimeFactors(int n)
    {
        var answer = new List<int>();

        while ((n & 1) == 0)
        {
            answer.Add(2);
            n >>= 1;
        }

        OddPrimeFactorRoutine(ref n, 3, answer);

        int k = 1;
        int lesserCandidate = 6 * k - 1;
        int greaterCandidate = 6 * k + 1;

        int lastCheck = IntegerSquareRoot(n);

        while (lesserCandidate <= lastCheck)
        {
            OddPrimeFactorRoutine(ref n, lesserCandidate, answer);
            OddPrimeFactorRoutine(ref n, greaterCandidate, answer);

            k++;
            lesserCandidate = 6 * k - 1;
            greaterCandidate = 6 * k + 1;
        }

        if (n > 1)
        {
            answer.Add(n);
        }
        
        return answer;
    }

    private static void OddPrimeFactorRoutine(ref int n, int prime, List<int> answer)
    {
        while (n % prime == 0)
        {
            n /= prime;
            answer.Add(prime);
        }
    }


    // Borrowed from https://rosettacode.org/wiki/Isqrt_(integer_square_root)_of_X#C.23
    // Calculates the largest integer that is equal to or lower than square root of number
    private static int IntegerSquareRoot(int number)
    {
        int q = 1;
        int r = 0;
        int t;

        while (q <= number)
        { 
            q <<= 2; 
        }

        while (q > 1)
        {
            q >>= 2;
            t = number - r - q;
            r >>= 1;
            if (t >= 0)
            { 
                number = t;
                r += q;
            }
        }
        return r;
    }


    private static int SumOfDigits(int n)
    {
        int sum = 0;
        while (n > 0)
        {
            sum += n % 10;
            n /= 10;
        }
        return sum;
    }


    public static int IsSmithNumber(int n)
    {
        List<int> primes = PrimeFactors(n);

        int sumNumber = SumOfDigits(n);

        int sumPrimeFactors = 0;
        foreach (var prime in primes)
        {
            sumPrimeFactors += SumOfDigits(prime);
        }

        return sumPrimeFactors == sumNumber ? 1 : 0;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        int result = Result.IsSmithNumber(n);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
