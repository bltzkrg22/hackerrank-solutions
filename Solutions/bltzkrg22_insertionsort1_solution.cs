using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    public static void insertionSort1(int n, List<int> arr)
    {
        int valueToStore = arr.Last();
        int comparedIndex = arr.Count - 2;
        bool loopEndCondition = false;

        while(loopEndCondition == false)
        {
            if (comparedIndex >= 0 && valueToStore < arr[comparedIndex] )
            {
                arr[comparedIndex + 1] = arr[comparedIndex];
                comparedIndex--;
                PrintArray(arr);
            }
            else
            {
                arr[comparedIndex + 1] = valueToStore;
                PrintArray(arr);
                loopEndCondition = true;
            }
        }
    }

    private static void PrintArray(List<int> arr)
    {
        // This works in .NET 5, but not on HackerRank's version 
        // var output = String.Join(' ', arr);
        
        var answer = new StringBuilder();
        answer.Append(arr[0]);
        for(int i = 1; i < arr.Count; i++)
        {
            answer.Append(' ');
            answer.Append(arr[i]);
        }
        
        System.Console.WriteLine(answer.ToString()); 
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        Result.insertionSort1(n, arr);
    }
}
