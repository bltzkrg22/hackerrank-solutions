using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static void insertionSort2(int n, List<int> arr)
    {
        for (int index = 1; index < arr.Count; index++)
        {
            SortSubroutine(arr, index);
            PrintArray(arr);
        }
    }

    private static void SortSubroutine(IList<int> arr, int rangeEndIndex)
    {
        // We assume that indices from 0 to (rangeEndIndex - 1) are already sorted
        
        int valueToStore = arr[rangeEndIndex];
        int comparedIndex = rangeEndIndex - 1;
        
        bool loopEndCondition = false;
        while(loopEndCondition == false)
        {
            if (comparedIndex >= 0 && valueToStore < arr[comparedIndex] )
            {
                arr[comparedIndex + 1] = arr[comparedIndex];
                comparedIndex--;
            }
            else
            {
                arr[comparedIndex + 1] = valueToStore;
                loopEndCondition = true;
            }
        }
    }

    private static void PrintArray(IReadOnlyList<int> arr)
    {       
        var answer = new StringBuilder();
        answer.Append(arr[0]);
        for(int i = 1; i < arr.Count; i++)
        {
            answer.Append(' ');
            answer.Append(arr[i]);
        }
        
        System.Console.WriteLine(answer.ToString()); 
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        Result.insertionSort2(n, arr);
    }
}
