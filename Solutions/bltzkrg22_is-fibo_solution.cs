using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private const long MaxInput = 10000000000000L; // 10^10
    private static bool _isFiboPrecomputed = false;

    // using HashSet for its fast lookup
    private static HashSet<long> _fibonacciSet;


    public static string isFibo(long n)
    {
        PrecomputeFibonacci();

        return _fibonacciSet.Contains(n) ? "IsFibo" : "IsNotFibo";
    }

    public static void PrecomputeFibonacci()
    {
        if (_isFiboPrecomputed == true)
        {
            return;
        }

        long prevFibo = 0;
        long nextFibo = 1;

        _fibonacciSet = new HashSet<long>() { prevFibo, nextFibo };

        // As we want to compute *all* Fibonacci numbers inside the range, there is no
        // point in using the fast matrix method
        while (nextFibo < MaxInput)
        {
            long buffer = nextFibo;
            nextFibo += prevFibo;
            prevFibo = buffer;
            _fibonacciSet.Add(nextFibo);
        }

        _isFiboPrecomputed = true;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            long n = Convert.ToInt64(Console.ReadLine().Trim());

            string result = Result.isFibo(n);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
