using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private static Dictionary<int, int> CosmonautToCountry = new Dictionary<int, int>();
    private static Dictionary<int, HashSet<int>> CountryToCosmonaut = new Dictionary<int, HashSet<int>>();

    public static long journeyToMoon(int n, List<List<int>> astronaut)
    {
        /*
        * We will create two maps:
        * a) CosmonautToCountry - each cosmonaut will be assigned to a country
        * b) CountryToCosmonaut - each country will be assigned a hashset with all cosmonauts from that country
        *
        * The country will be described by a single cosmonaut from that country.
        */

        foreach (var pair in astronaut)
        {
            if (CosmonautToCountry.ContainsKey(pair[0]) && CosmonautToCountry.ContainsKey(pair[1]))
            {
                /*
                * It is possible that a pair will create a "connection" between two groups, that we previously
                * didn't know that belong to the same country. In that case we merge those groups.
                */
                var country0 = CosmonautToCountry[pair[0]];
                var country1 = CosmonautToCountry[pair[1]];

                /*
                * However, it is possible that we already know that two cosmonauts are from the same country!
                * Consider input: [1,2] [1,3] [2,3] => the third pair [2,3] is redundant!
                * In that case we do nothing!
                */
                if (country0 == country1)
                {
                    continue;
                }

                // Change the country for each cosmonaut from country1 to country0
                foreach (var cosmonaut in CountryToCosmonaut[country1])
                {
                    CosmonautToCountry[cosmonaut] = country0;
                }

                // Remove country1 from dictionary CountryToCosmonaut, and reassign all cosmonauts from country1 to country0
                CountryToCosmonaut[country0].UnionWith(CountryToCosmonaut[country1]);
                CountryToCosmonaut.Remove(country1);
            }
            else if (CosmonautToCountry.ContainsKey(pair[0]) && !CosmonautToCountry.ContainsKey(pair[1]))
            {
                /*
                * If one cosmonaut from a pair already belongs to some country, and the other does not belong to any yet,
                * we assign them both to that country.
                */
                var country = CosmonautToCountry[pair[0]];
                CosmonautToCountry[pair[1]] = country;
                CountryToCosmonaut[country].Add(pair[1]);
            }
            else if (!CosmonautToCountry.ContainsKey(pair[0]) && CosmonautToCountry.ContainsKey(pair[1]))
            {
                /*
                * Same as above, swapped order.
                */
                var country = CosmonautToCountry[pair[1]];
                CosmonautToCountry[pair[0]] = country;
                CountryToCosmonaut[country].Add(pair[0]);
            }
            else
            {
                /*
                * Finally, if none of the cosmonauts has been already assigned to a country, create a new country for that pair.
                */
                CosmonautToCountry[pair[0]] = pair[0];
                CosmonautToCountry[pair[1]] = pair[0];
                CountryToCosmonaut[pair[0]] = new HashSet<int>(){pair[0], pair[1]};
            }
        }

        long answer = NewtonBinomial2(n); // n over 2

        /*
        * The are (n over 2) of all possible pairs of the cosmonauts. From that count we must subtract all pairs that do not
        * satisfy the criteria, i.e. in which both cosmonauts are from the same country.
        *
        * If there are c cosmonauts from a single country, then there are (c over 2) such pairs. We subtract (c_i over 2) for
        * each country from (n over 2) to get the answer.
        */

        foreach (var country in CountryToCosmonaut)
        {
            answer -= NewtonBinomial2(country.Value.Count);
        }

        return answer;
    }

    private static long NewtonBinomial2(int n) => (long)n * (n-1) / 2;
    
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int p = Convert.ToInt32(firstMultipleInput[1]);

        List<List<int>> astronaut = new List<List<int>>();

        for (int i = 0; i < p; i++)
        {
            astronaut.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(astronautTemp => Convert.ToInt32(astronautTemp)).ToList());
        }

        long result = Result.journeyToMoon(n, astronaut);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
