using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    public static int jumpingOnClouds(List<int> c)
    {
        int consecutiveZeros = 0;
        int jumpCounter = 0;

        /*
        * The list of clouds can be logically partitioned into consecutive 0s (normal clouds)
        * and singular instances of 1 inbetween - since it is always possible to reach the end,
        * there can't be two consecutive 1s.
        *
        * We always have to jump over a 1, so we are required to visit both 0s on each side of every 1:
        *                 ... 0 0 0  0  1  0  0 0 ...
        * we are forced to visit both 0s marked with round brackets:
        *                 ... 0 0 0 (0) 1 (0) 0 0 ...
        *
        * Consider the array: [0 0 0 0 1 0 0 0 0 0 1 0 0 0 1 0].
        * We can partition it as: [ [0000] 1 [00000] 1 [000] 1 [0] ].
        * To reach the end, we need to from the beginning to the end of each group of 0s, and jump over 1s.
        * The "shortest way" for a sequence of n zeros involves n / 2 jumps:
        * a) 1 zero:  [0] => no jumps
        * b) 2 zeros: [00] => 1 jump, [XX] (where X means a visited zero)
        * 3) 3 zeros: [000] => 1 jump, [X0X] 
        * 4) 4 zeros: [0000] => 2 jumps, [X0XX] 
        * 5) 5 zeros: [00000] => 2 jumps, [X0X0X] 
        * etc.
        *
        * So, to count the total number of jumps:
        * (1) count the consecutive zeros until digit 1 is encountered; we will require consecutiveZeros/2 jumps to go through the zeros
        * (2) on every digit 1 reset the zero counter, and increment jumpCounter (since we jump over digit 1)
        * repeat until the end of cloud array.
        */



        for (int i = 0; i < c.Count; i++)
        {
            if (c[i] == 1)
            {
                jumpCounter += consecutiveZeros / 2;
                consecutiveZeros = 0;
                jumpCounter++;
            }
            else // (c[i] == 0)
            {
                consecutiveZeros++;
            }
        }

        jumpCounter += consecutiveZeros / 2;

        return jumpCounter;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> c = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(cTemp => Convert.ToInt32(cTemp)).ToList();

        int result = Result.jumpingOnClouds(c);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
