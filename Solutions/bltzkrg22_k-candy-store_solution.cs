using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

// The answer is: 
// ⎛k + n - 1⎞
// ⎜         ⎟
// ⎝    k    ⎠

// The formula is explained here: 
// https://en.wikipedia.org/wiki/Multiset#Counting_multisets

class Result
{
    private const int Modulo = 1000000000;
    
    public static int KCandyStore(int n, int k)
        => (int)(TrimBigInteger(BinomialCoefficient(n + k - 1, k), Modulo));

    private static int TrimBigInteger(BigInteger n, int mod)
        => (int)( n % mod );

    // Method that calculates (p over q)
    private static BigInteger BinomialCoefficient(int p, int q)
    {
        // (p over q) = (p over p - q); we choose smaller from (q, p-q) to optimize
        if (q > p - q) 
        {
            q = p - q;
        }

        BigInteger result = 1;
        for (BigInteger i = 1; i <= q; i++)
        {
            result *= p - q + i;
            result /= i;
        }

        return result;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            int k = Convert.ToInt32(Console.ReadLine().Trim());

            int result = Result.KCandyStore(n, k);
            
            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
