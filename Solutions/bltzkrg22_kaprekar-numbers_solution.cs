using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static void kaprekarNumbers(int p, int q)
    {
        var answer = new StringBuilder();

        for (int i = p; i <= q; i++)
        {
            if (KaprekarTest(i) == true)
            {
                answer.Append($"{i.ToString()} ");
            }
        }

        if (answer.Length == 0)
        {
            Console.WriteLine("INVALID RANGE");
        }
        else
        {
            // Trim the final trailing space from the answer
            answer.Remove(answer.Length-1,1);
            Console.WriteLine(answer.ToString());
        }
    }

    private static bool KaprekarTest(int n)
    {
        /*
        * It may be proven that the remainder of any modified Kaprekar number must be equal to 0 or 1 modulo 9!
        * Notice that if we split n^2 into *any* two numbers and add them, the remainder modulo 9 will not change.
        * Therefore the necessary condition for any Kaprekar number is that n and n^2 are equal modulo 9, which
        * is true only for remainders 0 and 1.
        */ 
        if (n % 9 > 1)
        {
            return false;
        }

        long square = (long)n * n;

        int digitCount = DigitCount(square);

        int rightNumber = Convert.ToInt32(square % PowerOfTenWithDigits(digitCount - digitCount / 2));
        int leftNumber = Convert.ToInt32(square / PowerOfTenWithDigits(digitCount - digitCount / 2));

        if (leftNumber + rightNumber == n)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static int DigitCount(long n)
    {
        // Look silly, but this is the fasters method
        if (n < 10) return 1;
        if (n < 100) return 2;
        if (n < 1000) return 3;
        if (n < 10000) return 4;
        if (n < 100000) return 5;
        if (n < 1000000) return 6;
        if (n < 10000000) return 7;
        if (n < 100000000) return 8;
        if (n < 1000000000) return 9;
        if (n < 10000000000) return 10;
        if (n < 100000000000) return 11;
        if (n < 1000000000000) return 12;
        if (n < 10000000000000) return 13;
        if (n < 100000000000000) return 14;
        return 15;
    }

    public static long PowerOfTenWithDigits(int n)
    {
        // Look silly, but this is the fasters method
        switch (n)
        {
            case 1: return 10L;
            case 2: return 100L;
            case 3: return 1000L;
            case 4: return 10000L;
            case 5: return 100000L;
            case 6: return 1000000L;
            case 7: return 10000000L;
            case 8: return 100000000L;
            case 9: return 1000000000L;
            case 10: return 10000000000L;
            case 11: return 100000000000L;
            case 12: return 1000000000000L;
            case 13: return 10000000000000L;
            case 14: return 100000000000000L;
            default: throw new ApplicationException("This should never happen with correct input!");
        }
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        int p = Convert.ToInt32(Console.ReadLine().Trim());

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        Result.kaprekarNumbers(p, q);
    }
}
