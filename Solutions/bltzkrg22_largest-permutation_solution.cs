using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * I think the method could be optimized by using only `positionDictionary`, and not
    * modifying the contents of `arr`, but we would have to recreate the list for the
    * method output anyways. I guess it's acceptable.
    */
    public static List<int> largestPermutation(int k, List<int> arr)
    {
        var positionDictionary = arr.Select((value, index) => new { Value = value, Index = index })
                                    .ToDictionary(pair => pair.Value, pair => pair.Index);

        // Given the problem constraints, max value in `arr` is equal to its element count
        int nextElement = arr.Count;
        int numberOfSwaps = 0;

        // Actually, `currentArrayPosition` is always equal to arr.Count - 1 - nextElement,
        // but that way code is easier to follow
        int currentArrayPosition = 0;

        // `arr` is a permutation of [1,2,...,n]. We will use select sorting to sort the
        // initial elements of the array in descending order.
        while (numberOfSwaps < k && nextElement > 1)
        {
            if (arr[currentArrayPosition] == nextElement)
            // Number already on expected position, no swapping required. Note that with valid
            // inputs we will never reach outside of arr range, as we will hit nextElement == 1
            // condition first.
            {
                nextElement--;
                currentArrayPosition++;
            }
            else
            {
                int swapPosition = positionDictionary[nextElement];

                // First we reflect the change in positionDictionary
                positionDictionary[nextElement] = currentArrayPosition;
                positionDictionary[arr[currentArrayPosition]] = swapPosition;

                // Then we perform the swap in actual array
                SwapArrayIndexes(arr, currentArrayPosition, swapPosition);
                numberOfSwaps++;

                nextElement--;
                currentArrayPosition++;
            }
        }

        return arr;
    }

    private static void SwapArrayIndexes<T>(IList<T> list, int first, int second)
    {
        T buffer = list[first];
        list[first] = list[second];
        list[second] = buffer;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int k = Convert.ToInt32(firstMultipleInput[1]);

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        List<int> result = Result.largestPermutation(k, arr);

        textWriter.WriteLine(String.Join(" ", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
