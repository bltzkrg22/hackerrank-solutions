using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    public static string larrysArray(List<int> A)
    {
        /*
        * https://en.wikipedia.org/wiki/Inversion_(discrete_mathematics)
        * Observe that after a rotation the number of inversions in the array may:
        * a) be decreased by two:      [3,1,2] (2) => [1,2,3] (0)
        * b) stay unchanged:           [2,3,1] (2) => [3,1,2] (2)
        * c) be increased by two:      [1,2,3] (0) => [2,3,1] (2)
        *
        * This is because by changing [a,b,c] => [b,c,a], we reorder two pairs of elements: (a,b) and (a,c)
        * become (b,a) and (c,a). Each such reordering either adds a single inversion, or removes a single inversion.
        *
        * (Note that the numbers in array are unique! Previous statement is not true if some elements may be equal,
        * because when a == b, neither pair (a,b) not (b,a) may be an inversion.)
        *
        * And if we add two elements from set {-1 , 1}, then possible results are {-2, 0, 2}.
        * Conclusion: rotation does not change the parity of number of inversions!
        */

        /*
        * If the initial array has an odd number of inversions, than it can never be sorted, because
        * any sorted array has from definition exactly zero inversions.
        */

        /*
        * Now let's prove that any array with an even number of inversions may be sorted.
        * Observe, that as long as array contains at least 3 elements, we can move the smallest
        * element to the first index with a finite set of rotations:
        * A = [b,c,d,1,e,...] => [b,1,c,d,e,...] => [1,b,c,d,e,...] = [1,B], where B = [b,c,d,e,...]
        * so we can sort the initial array A if we can sort the subarray B,
        * i.e. array A without its smallest element.
        *
        *
        * So if repeat this procedure, we end up with an array of 3 elements.
        * There are six possible permutations of such an array:
        *
        * [1,2,3] (0 inversions); [1,3,2] (1 inversion);  [2,1,3] (1 inversion);
        * [2,3,1] (2 inversions); [3,1,2] (2 inversions); [3,2,1] (3 inversions);
        *
        * It's easy to see that with no more than 2 rotations, we can always sort an array
        * with an even number of inversions:
        * a) [1,2,3] is already sorted,
        * b) [2,3,1] => [3,1,2] => [1,2,3]
        * c) [3,1,2] => [1,2,3]
        * 
        * This concludes the proof that every array with an even number of inversions may be sorted.
        */
        
        long inversions = countInversions(A);

        return inversions % 2 == 0 ? "YES" : "NO";
    }



    /*
    * The following code is counting the inversions in an array using a merge sorting algorithm.
    * see: https://www.hackerrank.com/challenges/ctci-merge-sort/
    */

    public static long inversionCount = 0;

    public static long countInversions(List<int> arr)
    {
        inversionCount = 0;
        
        MergeSort(arr, 0, arr.Count - 1);
        
        return inversionCount;
    }
    
    
    public static void Merge(List<int> listToSort, int leftIndex, int middleIndex, int rightIndex)
    {
        // middleIndex is included in the LEFT list

        var tempLeft = new Queue<int>(listToSort.GetRange(leftIndex, middleIndex - leftIndex + 1));
        var tempRight = new Queue<int>(listToSort.GetRange(middleIndex + 1, rightIndex - middleIndex));

        int currentSortIndex = leftIndex;

        while (tempLeft.Count > 0 && tempRight.Count > 0)
        {
            if (tempLeft.Peek() <= tempRight.Peek())
            {
                listToSort[currentSortIndex] = tempLeft.Dequeue();
            }
            else
            {
                listToSort[currentSortIndex] = tempRight.Dequeue();
                inversionCount = inversionCount + tempLeft.Count;
            }
            currentSortIndex++;
        }

        if (tempLeft.Count == 0)
        {
            while (tempRight.Count > 0)
            {
                listToSort[currentSortIndex] = tempRight.Dequeue();
                currentSortIndex++;
            }
        }
        else
        {
            while (tempLeft.Count > 0)
            {
                listToSort[currentSortIndex] = tempLeft.Dequeue();
                currentSortIndex++;
            }
        }
    }

    public static void MergeSort(List<int> listToSort, int leftIndex, int rightIndex)
    {
        if (leftIndex == rightIndex)
        {
            return;
        }

        int middleIndex = (leftIndex + rightIndex) / 2;

        MergeSort(listToSort, leftIndex, middleIndex);
        MergeSort(listToSort, middleIndex + 1, rightIndex);

        Merge(listToSort, leftIndex, middleIndex, rightIndex);
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> A = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(ATemp => Convert.ToInt32(ATemp)).ToList();

            string result = Result.larrysArray(A);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
