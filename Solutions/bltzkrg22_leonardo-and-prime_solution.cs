using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    // https://oeis.org/A002110/ - Primorial numbers
    private static readonly List<long> A002110 = new List<long>() {
        1L, 2L, 6L, 30L, 210L,
        2310L, 30030L, 510510L, 9699690L, 223092870L,
        6469693230L, 200560490130L, 7420738134810L, 304250263527210L, 13082761331670030L,
        614889782588491410L 
    };

    /*
    * A002110[n] is a product of first n primes, and A002110[0] = 1.
    * 
    * A002110 sequence has at least exponential growth, and therefore has so few elements in Int64,
    * that we can just use a precomputed list.
    */
    public static int primeCount(long x)
    {
        if (x < 2L) return 0;
        if (x < 6L) return 1;
        if (x < 30L) return 2;
        if (x < 210L) return 3;
        if (x < 2310L) return 4;
        if (x < 30030L) return 5;
        if (x < 510510L) return 6;
        if (x < 9699690L) return 7;
        if (x < 223092870L) return 8;
        if (x < 6469693230L) return 9;
        if (x < 200560490130L) return 10;
        if (x < 7420738134810L) return 11;
        if (x < 304250263527210L) return 12;
        if (x < 13082761331670030L) return 13;
        if (x < 614889782588491410L) return 14;
        return 15;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            long n = Convert.ToInt64(Console.ReadLine().Trim());

            int result = Result.primeCount(n);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
