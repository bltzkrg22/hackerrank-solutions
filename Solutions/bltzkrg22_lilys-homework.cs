using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static int lilysHomework(List<int> arr)
    {
        /*
        * For a given input array there are two possible "beautiful" arrays:
        * a) input array sorted in ascending order,
        * b) input array sorted in descrending order.
        *
        * We will count how many swaps it takes to transform input array into each of two options,
        * and choose the smallest number as the answer.
        *
        * It is important to note that numbers is input array are distinct! This greatly simplifies the solution,
        * as we don't have to worry that there are multiple numbers in unsorted array that can potentially be swapped
        * into position with index i and still make an array sorted, and that choice could later make an impact
        * on total number of swaps required to make the array sorted.
        */

        List<int> sortedArray = new List<int>(arr);
        sortedArray.Sort();
        List<int> revsortedArray = new List<int>(sortedArray);
        revsortedArray.Reverse();

        int swapsToSorted = NumberOfSwaps(arr, sortedArray);
        int swapsToRevsorted = NumberOfSwaps(arr, revsortedArray);

        return Math.Min(swapsToSorted, swapsToRevsorted);
    }

    
    public static int NumberOfSwaps(List<int> initialArray, List<int> expectedArray)
    {
        //BitArray isPositionCorrect = new BitArray(initial.Count);

        var copyArray = new List<int>(initialArray); // We can't destroy initial list!
        var numberToPositionMap = new Dictionary<int, int>(){};

        // We will want to quicky look up on which index is a particular number. 
        for (int i = 0; i < copyArray.Count; i++)
        {
            numberToPositionMap[copyArray[i]] = i;
        }

        int swapCounter = 0;

        for (int i = 0; i < copyArray.Count; i++)
        {
            // numberAtPosition = copyArray[i];
            // expectedNumberAtPosition = expectedArray[i];

            // Is position at current index already correct?
            if (IsPositionCorrect(copyArray, expectedArray, i) == true)
            {
                //isPositionCorrect[i] = true;
                continue;
            }
            // if not, swap with the number that should be on this position. it's position can be easily found
            // through numberToPositionMap dictionary, which we created before
            else
            {
                // expectedArray[i] is the number expected at this position; numberToPositionMap[expectedArray[i]] is its actual position
                int swapPosition = numberToPositionMap[expectedArray[i]];

                // after the swap expectedNumberAtPosition will be at position i, and numberAtPosition will be at position swapPosition
                numberToPositionMap[ expectedArray[i] ] = i;
                numberToPositionMap[ copyArray[i] ] = swapPosition;

                // now swap and increment swap counter
                SwapPair(copyArray, i, swapPosition);
                swapCounter++;
            }
        }

        return swapCounter;
    }

    public static bool IsPositionCorrect(List<int> array, List<int> sortedArray, int position)
        => array[position] == sortedArray[position] ? true : false;

    public static void SwapPair(List<int> array, int indexA, int indexB)
    {
        int buffer = array[indexA];
        array[indexA] = array[indexB];
        array[indexB] = buffer;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        int result = Result.lilysHomework(arr);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
