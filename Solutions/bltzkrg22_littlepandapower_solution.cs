using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    public static int solve(int bas, int exp, int mod)
    {
        if (exp < 0)
        {
            bas = ModularMultiplicativeInverse(bas, mod);
            exp = -exp;
        }

        return FastExponentiationModulo(bas, exp, mod);
    }

    private static int FastExponentiationModulo(int input, int exponent, int mod)
    {   
        // long, since the input is at most 10^6, i.e. input squared may overflow int type;
        // initialized to 1 (multiplication neutral)
        long result = 1; 
        long currentPower = input;

        while (exponent > 0)
        {
            if ((exponent & 1) == 1)
            {
                result *= currentPower;
                result %= mod;
            }

            currentPower *= currentPower;
            currentPower %= mod;
            exponent >>= 1;
        }

        return (int)result;
    }

    private static int ModularMultiplicativeInverse(int a, int mod)
    {
        if (mod == 1) return 0;
        int m0 = mod;
        (int x, int y) = (1, 0);

        while (a > 1)
        {
            int q = a / mod;
            (a, mod) = (mod, a % mod);
            (x, y) = (y, x - q * y);
        }
        return x < 0 ? x + m0 : x;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int a = Convert.ToInt32(firstMultipleInput[0]);

            int b = Convert.ToInt32(firstMultipleInput[1]);

            int x = Convert.ToInt32(firstMultipleInput[2]);

            int result = Result.solve(a, b, x);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
