using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

/*
* We will use the following two facts:
* a) for every integer number x: 0 XOR x = x;
* b) for every integer number x: x XOR x = 0.
* 
* Therefore, if we start with 0 and calculate XOR of all numbers in the input
* list, the result will be the `lonely integer` we're looking for.
*/

class Result
{
    public static int lonelyinteger(List<int> a)
    {
        int answer = 0;
        foreach (var number in a)
        {
            answer ^= number;
        }
        return answer;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> a = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(aTemp => Convert.ToInt32(aTemp)).ToList();

        int result = Result.lonelyinteger(a);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
