using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    // area = 0.5 * base * height, so height = 2 * area / base
    public static int lowestTriangle(int trianglebase, int area)
    {
        // Using double type
        double height = 2.0 * area / trianglebase;
        return Convert.ToInt32(Math.Ceiling(height));
    }
    
    /*
    * The method below does not use double type, but instead uses
    * only integers and binary search to find the solution.
    */
    public static int lowestTriangleInteger(int trianglebase, int area)
    {
        // Using only integers
        int minRange = 1;
        int maxRange = area;

        bool endloop = false;
        while (!endloop)
        {
            int tryValue = (minRange + maxRange) / 2;          
            int tryOutcome = SuccessCondition(tryValue, h => trianglebase * h - 2 * area);
            if (tryOutcome == 0)
            {
                return tryValue;
            }
            else if (tryOutcome < 0)
            {
                minRange = tryValue;
            }
            else // if (tryOutcome > 0)
            {
                maxRange = tryValue;
            }

            // If at this point minRange == maxRange and solution was still not found,
            // then there is no solution in the specified initial range :(
            if (minRange == maxRange)
            {
                endloop = true;
            }
        }

        // If binary search did not find the 
        throw new ApplicationException("Solution was not found.");
    }

    /// <summary>
    /// Helper method to check if variable is the lowest possible value for which
    /// function is not negative. This method assumes that function is monotonously growing!
    /// </summary>
    /// <returns>0 if variable satisfies the condition. -1 if variable is too small. 1 if variable is too large.</returns>
    private static int SuccessCondition(int variable, Func<int, int> function)
    {
        if (function(variable) >= 0 && function(variable - 1) < 0)
        {
            return 0;
        }
        else if (function(variable) <= 0 && function(variable - 1) < 0)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int trianglebase = Convert.ToInt32(firstMultipleInput[0]);

        int area = Convert.ToInt32(firstMultipleInput[1]);

        int height = Result.lowestTriangle(trianglebase, area);

        textWriter.WriteLine(height);

        textWriter.Flush();
        textWriter.Close();
    }
}
