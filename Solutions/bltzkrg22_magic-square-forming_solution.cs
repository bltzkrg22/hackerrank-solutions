using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    public static int formingMagicSquare(List<List<int>> s)
    {
        /*
        * Instead of a list of lists, each matrix/magic square will be internally represented
        * as a 9-element int array.
        */

        var startMatrix = new int[9];

        startMatrix[0] = s[0][0];
        startMatrix[1] = s[0][1];
        startMatrix[2] = s[0][2];
        startMatrix[3] = s[1][0];
        startMatrix[4] = s[1][1];
        startMatrix[5] = s[1][2];
        startMatrix[6] = s[2][0];
        startMatrix[7] = s[2][1];
        startMatrix[8] = s[2][2];


        /*
        * It is possible to prove the following properties of a magic square that is
        * build from a permutations of numbers {1, 2, 3, ..., 9}
        * (1) The sum of each row, column and main diagonal must be equal to 15.
        * (2) The center number must be equal to 5.
        *
        * If we consider the following to properties, it turns out that a pair of numbers (a,d)
        * where a is a number in a corner of magic square, and d is a number from an adjacent
        * square (but not the center one) will create a unique magic square.
        *                 [a . .]
        *                 [d 5 .]
        *                 [. . .]
        * However it is not guaranteed that for each pair (a,d), all numbers will be
        * a permutation of {1, 2, 3, ..., 9}. For example, when (a,d)=(6,2), the obtained
        * square would be:
        *                 [6 6 3]
        *                 [2 5 8]
        *                 [7 4 4]
        * Therefore, we will generate a candidate square for each pair (a,d) and then
        * check if such square satisfies the permutation condition.
        * 
        * The above method will generate *EVERY* possible 3x3 magic square! There are 8*7=56
        * (a,d) pairs, so this is the upper bound for the number of possible squares.
        * As additional optimization, we could technically precompute all possible magic squares!
        */

        /*
        * The following LINQ query creates each (a,d) pair where a != d.
        */
        var possibleAD = new List<int>( ){1, 2, 3, 4, 6, 7, 8, 9};
        List<(int a, int d)> pairsAD = possibleAD.SelectMany(a => possibleAD.Where(d => a != d).Select(d => (a,d)) ).ToList();





        int minimalCost = int.MaxValue;

        foreach (var pair in pairsAD)
        {
            bool isMagic;
            var currentMatrix = CreateMagicsquareFromTwoVariables( pair.a, pair.d,  out isMagic);

            /*
            * Flag isMagic == true if and only if the square obtained from a particular
            * (a,d) pair is a permutation of {1,2,3,...,9}.
            */
            if (isMagic == true)
            {
                int cost = TransformationCost(startMatrix, currentMatrix);
                if (cost < minimalCost)
                {
                    minimalCost = cost;
                }
            }
        }

        return minimalCost;
    }

    public static int[] CreateMagicsquareFromTwoVariables(int a, int d, out bool isMagic)
    {
        var output = new int[9];

        output[0] = a;
        output[3] = d;
        output[4] = 5;

        output[1] = 20 - 2*a - d;
        output[2] = a + d - 5;
        output[5] = 10 - d;
        output[6] = 15 - a - d;
        output[7] = 2*a + d - 10;
        output[8] = 10 - a;

        /*
        * Now check if all numbers are a permutation of {1,2,3,4,5,6,7,8,9}. We will sort the
        * all values obtained from the above formulas, and check one by one.
        */

        var values = new List<int>( output );
        values.Sort();

        isMagic = true;

        for (int i = 0; i < 9; i++)
        {
            if (values[i] != i+1)
            {
                isMagic = false;
                break;
            }
        }

        return output;
    }


    public static int TransformationCost( int[] startingMatrix, int[] endingMatrix )
    {
        int cost = 0;

        for (int i = 0; i < 9; i++)
        {
            cost += Math.Abs(startingMatrix[i] - endingMatrix[i]);
        }

        return cost;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        List<List<int>> s = new List<List<int>>();

        for (int i = 0; i < 3; i++)
        {
            s.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(sTemp => Convert.ToInt32(sTemp)).ToList());
        }

        int result = Result.formingMagicSquare(s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
