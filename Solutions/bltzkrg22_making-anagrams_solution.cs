using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static int makingAnagrams(string s1, string s2)
    {
        var characterFrequencies = new Dictionary<char, int>();

        // If a character is present in the first word, we increment
        // its frequency in `characterFrequencies`
        foreach (var character in s1)
        {
            if (characterFrequencies.ContainsKey(character))
            {
                characterFrequencies[character] += 1;
            }
            else
            {
                 characterFrequencies[character] = 1;
            }
        }

        // If a character is present in the second word, we decrement
        // its frequency in `characterFrequencies`
        foreach (var character in s2)
        {
            if (characterFrequencies.ContainsKey(character))
            {
                characterFrequencies[character] += -1;
            }
            else
            {
                characterFrequencies[character] = -1;
            }
        }

        /*
        * A positive value in `characterFrequencies` means that a character is seen in word s1
        * more often than in word s2. Analogously, a negative value means that some character is
        * seen more often in word s2 than in word s1.
        * 
        * We obviously must delete all those characters.
        */

        return characterFrequencies.Values.Select(freq => Math.Abs(freq)).Sum();
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s1 = Console.ReadLine();

        string s2 = Console.ReadLine();

        int result = Result.makingAnagrams(s1, s2);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
