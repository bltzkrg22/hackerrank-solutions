using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static int MarsExploration(string s)
    {
        int counter = 0;

        for (var i = 0; i < s.Length; i++)
        {
            if (s[i] != ExpectedChar(i))
            {
                counter++;
            }
        }

        return counter;
    }

    private static char ExpectedChar(int position)
    {
        switch (position % 3)
        {
            case 0:
            case 2:
                return 'S';
            case 1:
                return 'O';
            default:
                throw new ApplicationException("The default case should never be reachable.");
        }
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s = Console.ReadLine();

        int result = Result.MarsExploration(s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
