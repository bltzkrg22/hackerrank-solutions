using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

// This is virtually the same problem as the one titled `Sherlock and Permutations`!

/*
* If n is matrix height and m is matrix width, then we *always* need to go down 
* (n - 1) times, and go right (m - 1) times. The answer will be the number of 
* distinct permutations of the required number of DOWN and RIGHT moves.
*
* The formula for distinct permutations of non-distinct objects is explained here:
* https://math.libretexts.org/@go/page/13571
*
* Additionally, dividing by some number a and taking the result modulo p
* (where a and p are coprime) is equivalent to calculating
* *modular multiplicative inverse* of a modulo p,
* https://en.wikipedia.org/wiki/Modular_multiplicative_inverse
*/



class Result
{

    private const int Modulo = 1000000007; // 10^9+7

    private static int ModInverse(int a, int mod)
    {
        if (mod == 1) return 0;
        int m0 = mod;
        (int x, int y) = (1, 0);

        while (a > 1)
        {
            int q = a / mod;
            (a, mod) = (mod, a % mod);
            (x, y) = (y, x - q * y);
        }
        return x < 0 ? x + m0 : x;
    }

    private static int FactorialMod(int n, int mod)
    {
        long answer = 1;
        for (int i = 1; i <= n; i++)
        {
            answer *= i;
            answer %= mod;
        }
        return (int)answer;
    }

    public static int MatrixTracing(int height, int width)
    {
        int nominator = FactorialMod((height - 1) + (width - 1), Modulo);
        int denominatorDown = FactorialMod((height - 1), Modulo);
        int denominatorRight = FactorialMod((width - 1), Modulo);

        int denominatorDownInverse = ModInverse(denominatorDown, Modulo);
        int denominatorRightInverse = ModInverse(denominatorRight, Modulo);

        var answerAsBiginteger = new BigInteger(nominator);
        answerAsBiginteger *= denominatorDownInverse;
        answerAsBiginteger %= Modulo;
        answerAsBiginteger *= denominatorRightInverse;
        answerAsBiginteger %= Modulo;

        return (int)answerAsBiginteger;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int n = Convert.ToInt32(firstMultipleInput[0]);

            int m = Convert.ToInt32(firstMultipleInput[1]);

            int result = Result.MatrixTracing(n, m);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
