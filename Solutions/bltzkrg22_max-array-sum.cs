using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution
{

    // Complete the maxSubsetSum function below.
    static int maxSubsetSum(int[] arr)
    {
        /*
        * Each non-positive number actually splits the input array! We only need to consider
        * subproblems, where each consecutive number is positive.
        */
        int sumOfPartitions = 0;

        int maxSumThreeBehind = 0;
        int maxSumTwoBehind = 0;
        int maxSumOneBehind = 0;
        int currentMax = 0;
        int previousMax = 0;


        foreach (var number in arr)
        {
            if (number > 0)
            {
                currentMax = Math.Max(number + maxSumThreeBehind, number + maxSumTwoBehind);
                previousMax = maxSumOneBehind;


                /*
                * Now prepare dynamic variables for the next iteration
                */
                maxSumThreeBehind = maxSumTwoBehind;
                maxSumTwoBehind = maxSumOneBehind;
                maxSumOneBehind = currentMax;
            }
            else
            {
                maxSumThreeBehind = 0;
                maxSumTwoBehind = 0;
                maxSumOneBehind = 0;

                sumOfPartitions += Math.Max(currentMax, previousMax);

                currentMax = 0;
                previousMax = 0;
            }
        }

        return sumOfPartitions + Math.Max(currentMax, previousMax);

    }

    static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine());

        int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp))
        ;
        int res = maxSubsetSum(arr);

        textWriter.WriteLine(res);

        textWriter.Flush();
        textWriter.Close();
    }
}
