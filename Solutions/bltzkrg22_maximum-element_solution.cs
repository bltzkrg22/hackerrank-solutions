using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static Stack<int> fullStack = new Stack<int>();

    // We push the number on `maxesStack` if its equal or greater than current max
    // - top of this stack will always be the max value in fullStack
    private static Stack<int> maxesStack = new Stack<int>();

    private enum OperationType { Push = 1, Pop = 2, PrintMax = 3 }

    public static List<int> getMax(List<string> operations)
    {
        var printList = new List<int>();

        foreach (var operation in operations)
        {
            // Ugly cast, but at least it works :/
            // Helps with readability later on
            OperationType operationType = (OperationType)Int32.Parse(operation.Substring(0, 1));

            switch (operationType)
            {
                case OperationType.Push:
                    int operationNumber = Int32.Parse(operation.Substring(2));
                    fullStack.Push(operationNumber);
                    // If maxesStack is empty, we always push
                    if (maxesStack.Count == 0)
                    {
                        maxesStack.Push(operationNumber);
                    }
                    // If maxesStack is not empty, we push if operationNumber is greater
                    else if (maxesStack.Peek() <= operationNumber)
                    {
                        maxesStack.Push(operationNumber);
                    }
                    break;

                case OperationType.Pop:
                    int removedNumber = fullStack.Pop();
                    // If removedNumber is equal to current max in stack,
                    // we remove it from maxesStack as well
                    if (removedNumber == maxesStack.Peek())
                    {
                        _ = maxesStack.Pop();
                    }
                    break;

                case OperationType.PrintMax:
                    printList.Add(maxesStack.Peek());
                    break;

                default:
                    throw new ApplicationException("Default case should be unreachable! Wrong operation id submitted.");
            }
        }

        return printList;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<string> ops = new List<string>();

        for (int i = 0; i < n; i++)
        {
            string opsItem = Console.ReadLine();
            ops.Add(opsItem);
        }

        List<int> res = Result.getMax(ops);

        textWriter.WriteLine(String.Join("\n", res));

        textWriter.Flush();
        textWriter.Close();
    }
}
