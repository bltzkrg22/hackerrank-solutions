using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static string InputString { get; set; }
    private const int Modulo = 1000000007;
    static int MaxInputLength = 100001;
    static int LettersInAlphabet = 26;
    static int[,] FrequencyMatrix = new int[MaxInputLength,LettersInAlphabet];
    //internal static Dictionary<char, int> pairFrequency = new Dictionary<char, int>();
    //internal static HashSet<char> singleCount = new HashSet<char>();

    // Table of factorials of n modulo 1_000_000_007, i.e. FactorialTable[n] = n! % p
    private static List<int> FactorialTable;

    // Table of modular inverses of factorial of n modulo 1_000_000_007. A modular inverse of x modulo p is such number y,
    // that (x * y) % p == 1. 
    private static List<int> ModularInverseTable;

    public static void initialize(string s)
    {
        InputString = s;

        FactorialTable = new List<int>(){1};
        ModularInverseTable = new List<int>(){1};

        for (int i = 1; i < InputString.Length; i++)
        {
            FactorialTable.Add( (int)(((long)FactorialTable[i-1] * i) % (long)Modulo) );
            ModularInverseTable.Add( ModInverse(FactorialTable[i], Modulo) );
        }

        /* Store occurrence of each character from the string. If character 'x' occurs in string InputString at index 2, like in "abx",
        * then we put 1 in row *3*, column 'x'-'a' (i.e. 24th column) in FrequencyMatrix.
        *
        * Note that the first row is intentionally left filled with zeroes!
        */ 
        for (int character = 0; character < InputString.Length; character++)
        {
            FrequencyMatrix[character + 1, InputString[character] - 'a'] += 1;
        }
 
        /* 
        * Now build the rest of the FrequencyMatrix. After this pass, the value of FrequencyMatrix[row,col] = n will mean, that 
        * from the beginning of the string to the character at index row-1, the letter associated with col ('a' is column 0, 'b'
        * is column 1 etc.) occurred exactly n times.
        *
        * Therefore, to count the ocurrences of character 'x' between index l (inclusive!) and index r (also inclusive!)
        * (note that those are 1-indexed because of the input constraints!), we only have to calculate the difference:
        * FrequencyMatrix[r,'x'-'a'] - FrequencyMatrix[l-1,'x'-'a']
        */
        for (int i = 1; i < InputString.Length; i++)
        {
            for (int character = 0; character < LettersInAlphabet; character++)
            {
                FrequencyMatrix[i + 1, character] += FrequencyMatrix[i, character];
            }
        }
    }

    public static int answerQuery(int l, int r)
    {
        /*
        * The expected palindrome must be of maximal length. Therefore, if there are r_k ocurrences of character k in
        * the string, then we *have to* use Floor(r_k / 2.0) pairs of this character to put them on both sides of palindrome.
        * If there are singular characters without a pair left, we *have to* be put one in the middle of our palindrome, to make
        * sure that it is of maximal length.
        *
        * The formula to calculate the number of palindromes will be therefore equal to:
        * (number of distinct permutations of all characters that form a pair, which are not unique!) * (number of characters that can be 
        * put in the middle of the palindrome, or 1 if there are no singular characters).
        *
        * Example: aaaaaabbbbbcde. The pairs are: aa, aa, aa, bb, bb. Singular characters are: b, c, d, e.
        * Therefore there are: 5! / 3! / 2! * 4 = 120 / 6 / 2 * 4 = 40 distinct palindromes of maximal width (i.e. 11) that we can create.
        * The formula for the distinct permutations of non-distinct objects is explained here: https://math.libretexts.org/@go/page/13571 
        */

     
        int singles = 0; 
        int[] pairs = new int[LettersInAlphabet];

        // We use the FrequencyMatrix to quickly calculate the number of occurences of each letter in InputString.
        for (int i = 0; i < LettersInAlphabet; i++)
        {
            singles += (FrequencyMatrix[r, i] - FrequencyMatrix[l - 1, i]) % 2;
            pairs[i] = (FrequencyMatrix[r, i] - FrequencyMatrix[l - 1, i]) / 2;
        }

        /*
        * The result of (a/b)%p, where a is a multiple of b, and p is a prime number, is *not* equal to (a%p)/(b%p). Instead, we must
        * calculate modular multiplicative inverse of b, denoted b', and compute ((a%p) * b')%p. More detailed explanation can be found here:
        * https://en.wikipedia.org/wiki/Modular_multiplicative_inverse
        */

        long formula = (long) FactorialTable[pairs.Sum()];

        foreach (var pair in pairs)
        {
            formula *= ModularInverseTable[pair];
            formula %= Modulo;
        }

        if (singles > 1)
        {
            formula *= singles;
            formula %= Modulo;
        }

        return (int)formula;
    }

    public static int ModInverse(int a, int m)
    {
        if (m == 1) return 0;
        int m0 = m;
        (int x, int y) = (1, 0);

        while (a > 1)
        {
            int q = a / m;
            (a, m) = (m, a % m);
            (x, y) = (y, x - q * y);
        }
        return x < 0 ? x + m0 : x;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s = Console.ReadLine();

        Result.initialize(s);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int l = Convert.ToInt32(firstMultipleInput[0]);

            int r = Convert.ToInt32(firstMultipleInput[1]);

            int result = Result.answerQuery(l, r);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
