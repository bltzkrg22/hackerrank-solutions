using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private class IntegerTriplet
    {
        public int Fst { get; set; }
        public int Snd { get; set; }
        public int Trd { get; set; }

        public IntegerTriplet(int a, int b, int c)
        {
            Fst = (new[] { a, b, c }).Max();
            Trd = (new[] { a, b, c }).Min();
            Snd = a + b + c - Fst - Trd;
        }

        public List<int> AsList()
        {
            return new List<int>() { Trd, Snd, Fst };
        }
    }

    public static List<int> maximumPerimeterTriangle(List<int> sticks)
    {
        /*
        * There are at most 50 sticks, so we will create all possible triplets, filter
        * those which don't satisfy the triangle criterium, order them by sum and then
        * order by the special tiebreaker criteria.
        */
        var candidateTriangles = new List<IntegerTriplet>();

        for (int fst = 0; fst < sticks.Count; fst++)
        {
            for (int snd = fst + 1; snd < sticks.Count; snd++)
            {
                for (int trd = snd + 1; trd < sticks.Count; trd++)
                {
                    if (TriangleCriterium(sticks[fst], sticks[snd], sticks[trd]))
                    {
                        candidateTriangles.Add(new IntegerTriplet(sticks[fst], sticks[snd], sticks[trd]));
                    }
                }
            }
        }

        if (candidateTriangles.Count == 0)
        {
            return new List<int>() { -1 };
        }

        // If there were much more elements in candidateTriangles, we should instead
        // use LINQ's Aggregate() to find the solution in O(n) time instead of O(n log n)
        // Full sort passes the time limits though
        candidateTriangles.Sort(new MyComparer());
        return candidateTriangles.First().AsList();
    }

    private static bool TriangleCriterium(int a, int b, int c)
    {
        int max = (new[] { a, b, c }).Max();

        // The triangle criterium is: min + mid > max, which can be equivalently
        // written as min + mid + max = a + b + c > 2 * max

        return ((long)a + b + c > 2L * max);
    }

    private class MyComparer : Comparer<IntegerTriplet>
    {
        public override int Compare(IntegerTriplet A, IntegerTriplet B)
        {
            // First order by sum descending
            // Sum of three sides might overflow an int
            if ((long)A.Fst + A.Snd + A.Trd - B.Fst - B.Snd - B.Trd != 0)
            {
                return (int)(((long)B.Fst + B.Snd + B.Trd) - ((long)A.Fst + A.Snd + A.Trd));
            }
            // Then order by max side descending
            else if (A.Fst != B.Fst)
            {
                return B.Fst - A.Fst;
            }
            // Then order by min side descending, and no more tiebreakers
            else
            {
                return B.Trd - A.Trd;
            }
        }
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> sticks = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(sticksTemp => Convert.ToInt32(sticksTemp)).ToList();

        List<int> result = Result.maximumPerimeterTriangle(sticks);

        textWriter.WriteLine(String.Join(" ", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
