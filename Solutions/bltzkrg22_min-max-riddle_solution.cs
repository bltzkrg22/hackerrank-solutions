using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution
{

    static long[] riddle(long[] arr)
    {
        /*
        * (Step 1) Create an array, that keeps a record how many indices to the left
        * there is a smaller number in the input array.
        *
        * For puprose of this problem, imagine that arr[-1] = MinValue 
        * and arr[arr.Length] = MinValue (it will become clear later on).
        */

        var distanceToLowerLeft = new int[arr.Length];

        var leftStack = new Stack<(long, int)>(new[] { (long.MinValue, -1) });

        for (int i = 0; i < arr.Length; i++)
        {
            bool endloop = false;
            while (endloop == false)
            {
                if (arr[i] > leftStack.Peek().Item1)
                {
                    distanceToLowerLeft[i] = i - leftStack.Peek().Item2;
                    leftStack.Push((arr[i], i));
                    endloop = true;
                }
                else if (arr[i] == leftStack.Peek().Item1)
                {
                    distanceToLowerLeft[i] = i - leftStack.Peek().Item2 + distanceToLowerLeft[leftStack.Peek().Item2];
                    _ = leftStack.Pop();
                    leftStack.Push((arr[i], i));
                    endloop = true;
                }
                else
                {
                    _ = leftStack.Pop();
                    //endloop = false;   
                }
            }
        }



        /*
        * (Step 2) Repeat Step 1, but this time travese the input array from right to left, i.e.
        * find how many indices to the *right* there is a smaller number in the input array.
        *
        * Again, imagine that arr[arr.Length] = MinValue.
        */


        var distanceToLowerRight = new int[arr.Length];

        var rightStack = new Stack<(long, int)>(new[] { (long.MinValue, arr.Length) });

        for (int i = arr.Length - 1; i >= 0; i--)
        {
            bool endloop = false;
            while (endloop == false)
            {
                if (arr[i] > rightStack.Peek().Item1)
                {
                    distanceToLowerRight[i] = rightStack.Peek().Item2 - i;
                    rightStack.Push((arr[i], i));
                    endloop = true;
                }
                else if (arr[i] == rightStack.Peek().Item1)
                {
                    distanceToLowerRight[i] = rightStack.Peek().Item2 - i + distanceToLowerRight[rightStack.Peek().Item2];
                    _ = rightStack.Pop();
                    rightStack.Push((arr[i], i));
                    endloop = true;
                }
                else
                {
                    _ = rightStack.Pop();
                    //endloop = false;   
                }
            }
        }


        /*
        * (Step 3) Now for each index i from input array, evaluate expression:
        * maxWindowWidthWithCurrentAsMinvalue[i] = (distanceToLowerLeft[i] - 1) + (distanceToLowerRight[i] - 1) + 1
        * This will be *the maximal width of a window, where arr[i] is the minimum value*.
        */

        var maxWindowWidthWithCurrentAsMinvalue = new int[arr.Length];

        for (int i = 0; i < arr.Length; i++)
        {
            maxWindowWidthWithCurrentAsMinvalue[i] = (distanceToLowerLeft[i] - 1) + (distanceToLowerRight[i] - 1) + 1;
        }



        /*
        * A clearer explanation of steps 1-3 combined.
        * Let's say that the input array is:                  [4 6 (4) 0]
        * then distanceToLowerLeft is:                        [1 1 (3) 4]
        * and distanceToLowerRight is:                        [3 1 (1) 1]
        * and finally maxWindowWidthWithCurrentAsMinvalue:    [3 1 (3) 4]
        *
        * Let's focus on number 4 at index 2. distanceToLowerLeft[2] = 3, so if we look (3-1)=2 indices to the left,
        * there are no lower values that 4 (and since a window must start inside input array, this is why we assumed that array
        * is surrounded with minus infinity, i.e. MinValue). distanceToLowerright[1], so if we look (1-1)=0 indices to the right,
        * there won't be any values lower than 4. (Actually 1-1=0, so the first value to the right is already lower).
        *
        * maxWindowWidthWithCurrentAsMinvalue[2] = (distanceToLowerLeft[2] - 1) + (distanceToLowerRight[2] - 1) + 1 = 2 + 0 + 1
        * means, that we can look at most 2 indices to the left, 0 indices to the right, then also include current index,
        * and build a window with width 3, so that arr[2] = 4 will be the minimum value in that window. In other words,
        * maxWindowWidthWithCurrentAsMinvalue[2] = 3 means, that there exists a window of width 3, that contains
        * the number at index 2, and arr[2] is its minimum - and it's the maximal possible width.
        */



        /*
        * (Step 4) maxWindowWidthWithCurrentAsMinvalue created in previous step "maps" value of arr[i] to the maximal window width, where
        * arr[i] is its minimum. But it is possible that the mapping is initially not unique.

        * Consider input array: [7 1 1 8 7], for which maxWindowWidthWithCurrentAsMinvalue is: [1 5 5 1 2]. Mapping 1 => 5
        * is equivalent, let's skip it for a moment, and focus on mappings 7 => 1 and 7 => 2.
        *
        * Existence of mappings 7 => 1 and 7 => 2 mean, that first instance of 7 can be a minimum of window no wider than 1, and the second
        * instance of 7 can be a minimum of window no wider than 2. In that case, we map 7 to the higher window width! Notice, that if
        * arr[i] = x and maxWindowWidthWithCurrentAsMinvalue[i] = w, than x can logially be the minimum of window with width w, w-1, w-2, ..., 1.
        * So we only need to remember the greates possible width!
        *
        * Let's create a dictionary, that will keep those mappings. The key of the dictionary is value of arr[i], which is provided as long type.
        * The maximal window width is on the other hand not larger than 10^6, so we will use int type to store it.
        */

        var mapArrayvalueToMaxwindowwidth = new Dictionary<long, int>();  // Key of dict is value at arr[i],
                                                                          // Value of dict is maxWindowWidthWithCurrentAsMinvalue[i]

        for (int i = 0; i < arr.Length; i++)
        {
            if (mapArrayvalueToMaxwindowwidth.ContainsKey(arr[i]) == false)
            {
                mapArrayvalueToMaxwindowwidth[arr[i]] = maxWindowWidthWithCurrentAsMinvalue[i];
            }
            else
            {
                /*
                * In case of tiebreak, i.e. when array contains a number multiple times, like here: [(2) 1 3 4 5 (2) 6]
                * where the first 2 can be a minimum of window of width 1, and the second 2 can be a minimum of window
                * of width 5, take higher value (here it would be 5).
                */
                if (mapArrayvalueToMaxwindowwidth[arr[i]] < maxWindowWidthWithCurrentAsMinvalue[i])
                {
                    mapArrayvalueToMaxwindowwidth[arr[i]] = maxWindowWidthWithCurrentAsMinvalue[i];
                }
            }
        }


        /*
        * (Step 5) Reverse the dictionary created in Step 4. Map each window width to the maximal value, that can be a minimum
        * of a window with that width. As in Step 4, mappings can initially be not unique. For example, consider array:
        * [5 3 4 1 2], in which we map: 5 => 1, 3 => 3, 4 => 1, 1 => 5, 2 => 2 (which means that 5 can be a minimum of window with
        * width 1 or smaller, 3 can be a minimum of window with width 3 or smaller, etc.)
        *
        * For a window with width 1, there are two mappings: 5 => 1 and 4 => 1. We are interesed in maximum of those values!
        * In this case, we map window of width 1 to value 5.
        *
        * There is one pitfall. Consider input array: [6 1 9 7 8]. Dict created in Step 4 would map 6 => 2 and 7 => 3, so after
        * the described reversion we would map 2 => 6 and 3 => 7. However, if 7 can be a minimum of a window with width 3, then
        * it can also be a minimum of a window with width 2! We will have to make a correction pass through the answer array.
        *  
        * Since the output data type is supposed to be an array, that for each window width provides some value from input array,
        * here instead of dictionary we will create an array and use its index as "dictionary" key, and value as "dictionary" value.
        * 
        * That way we will be able to just return this new array directly as output from the method.
        * Notice the offset! answer[i] will be the maximum of minimums of width = (i - 1).
        */

        var answer = new long[arr.Length];
        /*
        * 0 is a lowest valid number that can be encountered in input array. Later we will want to know which values in array
        * were not initialized, so we will set them to -1.
        */
        for (int i = 0; i < answer.Length; i++)
        {
            answer[i] = -1;
        }


        foreach (var pair_Arrayvalue_Windowwidth in mapArrayvalueToMaxwindowwidth)
        {
            long ArrayValue = pair_Arrayvalue_Windowwidth.Key;
            int WindowWidth = pair_Arrayvalue_Windowwidth.Value;

            if (answer[WindowWidth - 1] == -1)
            {
                answer[WindowWidth - 1] = ArrayValue;
            }
            else
            {
                if (answer[WindowWidth - 1] < ArrayValue)
                {
                    answer[WindowWidth - 1] = ArrayValue;
                }
            }
        }


        /*
        * (Step 6) Finally, let's make a correction pass. We are looking for two things.
        *
        * (6.1) Some values can be left at value -1.
        * That can happen when some mappigns in Steps 4-5 were not unique. Consider example input array: [7 3 3 3 3],
        * for which the answer array would initially be: [7 -1 -1 -1 3].
        * 7 can be a minimum of window with max width 1; 3 can be a minimum of window with max width 5.
        *
        * What about windows with width 2-4? We put there 3 as well. 3 can be a minimum of window with width 4 as well,
        * but 7 cannot be a minimum of window with width greater than 1.
        * So we traverse the output array from right to left, and replace -1 with the value directly to the right.
        *
        * (6.2) Considering the previously described "pitfall", the output array should must always be increasing when
        * lowering the window width, so when traversing from right to left. 
        *
        * Combining (6.1) and (6.2) if a value at index i is smaller than value at index i+1, we will overwrite
        * it with the value at index i+1.
        */


        /*
        * The following loop condition:        (answer[answer.Length - 1] == -1)
        * should actually never be true, as the algorithm should always find that
        * the minimal value in the array is the only possible value for window spanning whole input array.
        * But the cost of the check is minimal, and I don't want to have to prove the previous statement.
        */
        if (answer[answer.Length - 1] == -1)
        {
            answer[answer.Length - 1] = arr.Min();
        }



        for (int i = answer.Length - 2; i >= 0; i--)
        {
            if (answer[i + 1] > answer[i])
            {
                answer[i] = answer[i + 1];
            }
        }

        return answer;
    }

    static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine());

        long[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt64(arrTemp))
        ;
        long[] res = riddle(arr);

        textWriter.WriteLine(string.Join(" ", res));

        textWriter.Flush();
        textWriter.Close();
    }
}
