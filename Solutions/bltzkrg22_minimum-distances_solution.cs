using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{    
    public static int minimumDistances(List<int> a)
    {
        // This LINQ formula will create a Dictionary<int, List<int>>, where key = number from input array,
        // value = list of indexes where the number is encountered.
        // We also filter out numbers which are encountered only once!
        var positionDictionary = a.Select((number, index) => new {Number = number, Index = index} )
                                  .GroupBy(niPair => niPair.Number, niPair => niPair.Index) // niPair = (number,index) pair
                                  .Where(niPair => niPair.Count() > 1)
                                  .ToDictionary(niPair => niPair.Key, niPair => niPair.ToList());
                                                                                // niPair.ToList() is already sorted!

        int minDistance = int.MaxValue;

        foreach (var indexList in positionDictionary.Values)
        {
            for (int i = 1; i < indexList.Count; i++)
            {
                minDistance = Math.Min(minDistance, indexList[i] - indexList[i - 1]);
            }
        }

        return minDistance == int.MaxValue ? -1 : minDistance;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> a = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(aTemp => Convert.ToInt32(aTemp)).ToList();

        int result = Result.minimumDistances(a);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
