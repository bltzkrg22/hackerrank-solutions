using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    // .NET 5 allows for a cleaner declaration of longer numbers with underscore char
    //const long MaxPossiblePrice = 10_000_000_000_000_000;

    const long MaxPossiblePrice = 10000000000000000;
    public static int minimumLoss(List<long> price)
    {
        /*
        * We will traverse the input list once.
        *
        * We will create a sorted set, which will include all prices from previous years. This will make it easy to
        * find a set window prices from previous years, that were greater than the price from current year. If we take the
        * minimal value from that window, we will get the lowest possible loss, if we sold the house in current year.
        *
        * If the window is empty, this means that either (1) current price is the highest in history (and it is impossible to sell
        * at a loss), or (2) if we sold the house in current year, the loss would be guaranteed to be greater than a lowest loss
        * possible previously (so selling in current year does not satisfy the requirements).
        */
        var previousSorted = new SortedSet<long>( ){ price[0] };

        /*
        * Initialize minimumLoss to a value greater than the maximal possible result; leaving minimumLoss at 0 would break the check
        * if minimumLoss is less than currentMinimumLoss (i.e. in currently evaluated year).
        */
        long minimumLoss = MaxPossiblePrice + 1;

        for (int i = 1; i < price.Count; i++)
        {
            long currentPrice = price[i];
            long nextPrice = previousSorted.GetViewBetween( currentPrice + 1, currentPrice +  minimumLoss ).FirstOrDefault();
            previousSorted.Add(currentPrice);


            /*
            * 0 is the value returned by FirstOrDefault, if GetViewBetween returned an empty set. Empty set means no solution
            * or a solution worse (i.e. greater) than previous best.
            */
            if (nextPrice == 0)
            {
                continue;
            }

            long currentMinimumLoss = nextPrice - currentPrice;

            // If currentMinimumLoss is better than than previous best, store it.
            if (minimumLoss > currentMinimumLoss)
            {
                minimumLoss = currentMinimumLoss;
            }
        }

        // Casting only because the problem requirements state that the answer must be an int.
        return (int)minimumLoss;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<long> price = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(priceTemp => Convert.ToInt64(priceTemp)).ToList();

        int result = Result.minimumLoss(price);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
