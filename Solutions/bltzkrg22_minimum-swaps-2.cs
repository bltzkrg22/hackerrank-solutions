using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution {

    // Complete the minimumSwaps function below.
    static int minimumSwaps(int[] arr) {
        BitArray isPositionCorrect = new BitArray(arr.Length);
        int swapCounter = 0;
        int swapBuffer;

        for (int i = 1; i <= arr.Length; i++)
        {
            if ( arr[i - 1] == i )
            {
                isPositionCorrect[i - 1] = true;
            }

            while( !isPositionCorrect[i - 1])
            {
                swapCounter++;
                swapBuffer = arr[i - 1];
                arr[i - 1] = arr[swapBuffer - 1];
                arr[swapBuffer - 1] = swapBuffer;
                isPositionCorrect[swapBuffer - 1] = true;

                if (arr[i - 1] == i)
                {
                    isPositionCorrect[i - 1] = true;
                }
            }

        }


        return swapCounter;

    }

    static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine());

        int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp))
        ;
        int res = minimumSwaps(arr);

        textWriter.WriteLine(res);

        textWriter.Flush();
        textWriter.Close();
    }
}
