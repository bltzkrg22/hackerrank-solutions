using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution {

    // Complete the minTime function below.
    static long minTime(long[] machines, long goal)
    {
        long leftSearchBound = 0;
        long rightSearchBound = machines.Min() * goal;
        long midpointSearch = -1;

        bool searchSuccess = false;
        while ( !searchSuccess )
        {
            midpointSearch = (rightSearchBound + leftSearchBound) / 2;

            switch (IsDayCorrect(machines, goal, midpointSearch))
            {
                case 0:
                    searchSuccess = true;
                    break;
                case 1:
                    rightSearchBound = midpointSearch;
                    break;
                case -1:
                    leftSearchBound = midpointSearch;
                    break;
            }
        }

        return midpointSearch;
    }

    public static int IsDayCorrect( long[] machines, long goal, long day )
    {
        if (ProductionAfterDay(machines, day - 1) < goal && ProductionAfterDay(machines, day) >= goal)
        {
            return 0;
        }
        else if (ProductionAfterDay(machines, day) < goal )
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }


    public static long ProductionAfterDay( long[] machines, long day )
    {
        long productionCounter = 0;
        foreach (var machine in machines)
        {
            productionCounter += day / machine;
        }
        return productionCounter;
    }

    static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] nGoal = Console.ReadLine().Split(' ');

        int n = Convert.ToInt32(nGoal[0]);

        long goal = Convert.ToInt64(nGoal[1]);

        long[] machines = Array.ConvertAll(Console.ReadLine().Split(' '), machinesTemp => Convert.ToInt64(machinesTemp))
        ;
        long ans = minTime(machines, goal);

        textWriter.WriteLine(ans);

        textWriter.Flush();
        textWriter.Close();
    }
}
