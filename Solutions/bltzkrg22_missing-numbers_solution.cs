using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * It is not explicitly said what the program shoud do if a number is present in arr, but
    * not in brr. Here, such number will be simply igonored.
    */
    public static List<int> missingNumbers(List<int> arr, List<int> brr)
    {
        var frequencyTable = new SortedDictionary<int, int>();

        // Add the ocurrence of each number in brr
        foreach (var number in brr)
        {
            // .NET 5 generic collections have a nice extension method GetValueOrDefault() which
            // could simplify the if-else statement to just:
            // frequencyTable[number] = frequencyTable.GetValueOrDefault(number) + 1;
            if (!frequencyTable.ContainsKey(number))
            {
                frequencyTable[number] = 1;
            }
            else
            {
                frequencyTable[number] += 1;
            }
        }

        // "Subtract" the ocurrence of each number in arr
        foreach (var number in arr)
        {
            if (!frequencyTable.ContainsKey(number))
            {
                frequencyTable[number] = -1;
            }
            else
            {
                frequencyTable[number] -= 1;
            }
        }

        // At this point frequencyTable[x] == y means that the number of ocurrences of x
        // in brr minus the number of ocurrences in arr is equal to y. If y > 0, then this is
        // the "missing number" we are looking for

        var answer = new List<int>();
        foreach (var kvpair in frequencyTable)
        {
            if (kvpair.Value > 0)
            {
                answer.Add(kvpair.Key);      
            }
        }

        return answer;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        int m = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> brr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(brrTemp => Convert.ToInt32(brrTemp)).ToList();

        List<int> result = Result.missingNumbers(arr, brr);

        textWriter.WriteLine(String.Join(" ", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
