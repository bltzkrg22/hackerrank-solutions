using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

// The solution is *correct*, i.e. it is as precise as required by problem description
// (10^{-6}). There is something wonky with the test cases though, and in a few languages,
// C# included, there are fails due to some very distant precision errors
// (there was some talk on the discussion board about the 16th digit after decimal point?)


class Result
{

/*
* It can be proven that we only have to care about four most distant points on each axis!
* Those 4 points create at most 6 segments that we potentially have to check.
*/

    public static double solve(List<List<int>> coordinates)
    {
        int? furthestPositiveX = null;
        int? furthestNegativeX = null;
        int? furthestPositiveY = null;
        int? furthestNegativeY = null;

        foreach (var point in coordinates)
        {
            if (point[0] > 0)
            {
                furthestPositiveX = Math.Max(point[0], furthestPositiveX ?? 0);
            }
            else if (point[0] < 0)
            {
                furthestNegativeX = Math.Min(point[0], furthestNegativeX ?? 0);
            }
            else if (point[1] > 0)
            {
                furthestPositiveY = Math.Max(point[1], furthestPositiveY ?? 0);
            }
            else if (point[1] < 0)
            {
                furthestNegativeY = Math.Min(point[1], furthestNegativeY ?? 0);
            }
            else // if point = (0,0)
            {
                furthestPositiveX = Math.Max(point[0], furthestPositiveX ?? 0);
                furthestNegativeX = Math.Min(point[0], furthestNegativeX ?? 0);
                furthestPositiveY = Math.Max(point[1], furthestPositiveY ?? 0);
                furthestNegativeY = Math.Min(point[1], furthestNegativeY ?? 0);
            }
        }

        var potentialAnswers = new List<BigInteger>();

        if (furthestPositiveX != null && furthestNegativeX != null)
        {
            potentialAnswers.Add(DistanceParallelSquared(furthestPositiveX.Value, furthestNegativeX.Value));
        }
        if (furthestPositiveY != null && furthestNegativeY != null)
        {
            potentialAnswers.Add(DistanceParallelSquared(furthestPositiveY.Value, furthestNegativeY.Value));
        }
        if (furthestPositiveX != null && furthestPositiveY != null)
        {
            potentialAnswers.Add(DistanceDiagonalSquared(furthestPositiveX.Value, furthestPositiveY.Value));
        }
        if (furthestPositiveX != null && furthestNegativeY != null)
        {
            potentialAnswers.Add(DistanceDiagonalSquared(furthestPositiveX.Value, furthestNegativeY.Value));
        }
        if (furthestNegativeX != null && furthestPositiveY != null)
        {
            potentialAnswers.Add(DistanceDiagonalSquared(furthestNegativeX.Value, furthestPositiveY.Value));
        }
        if (furthestNegativeX != null && furthestNegativeY != null)
        {
            potentialAnswers.Add(DistanceDiagonalSquared(furthestNegativeX.Value, furthestNegativeY.Value));
        }

        var answerSquared = potentialAnswers.Max();
        return Math.Round(Math.Sqrt((double)answerSquared), 6);
    }

    private static BigInteger DistanceDiagonalSquared(int x, int y)
    {
        var horizontal = new BigInteger(x);
        var vertical = new BigInteger(y);
        return horizontal * horizontal + vertical * vertical;
    }

    private static BigInteger DistanceParallelSquared(int positive, int negative)
    {
        var answer = new BigInteger(positive - negative);
        return answer * answer;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<List<int>> coordinates = new List<List<int>>();

        for (int i = 0; i < n; i++)
        {
            coordinates.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(coordinatesTemp => Convert.ToInt32(coordinatesTemp)).ToList());
        }

        double result = Result.solve(coordinates);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
