using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

/*
* We will use the following property of binomial coefficients (which holds for k >= 1):
*
* ⎛n⎞   ⎛  n  ⎞   n - (k - 1)
* ⎜ ⎟ = ⎜     ⎟ * ───────────
* ⎝k⎠   ⎝k - 1⎠        k
*
* and of course (n over 0) is equal to 1 (for non-degenerative cases).
*/


class Result
{
    private const int Modulo = 1000000000;
    
    private static int TrimBigInteger(BigInteger n, int mod)
        => (int)( n % mod );
    
    public static List<int> BinomialCoefficients(int n)
    {
        BigInteger coefficient = new BigInteger(1);
        var answer = new List<int>(){TrimBigInteger(coefficient, Modulo)};

        for (int k = 1; k <= n; k++)
        {
            // Note that the following:
            //                  coefficient *= (n - k + 1) / k
            // actually produces an incorrect answer, since it evaluates the right
            // side first, and in half of the cases  (n - k + 1) < k and right side
            // calculated first produces 0! We need to assert this calculates first:
            //                  coefficient * (n - k + 1)
            coefficient = coefficient * (n - k + 1) / k;
            answer.Add(TrimBigInteger(coefficient, Modulo));
        }

        return answer;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> result = Result.BinomialCoefficients(n);

            textWriter.WriteLine(String.Join(" ", result));
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
