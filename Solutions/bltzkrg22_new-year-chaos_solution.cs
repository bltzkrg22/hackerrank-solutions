using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'minimumBribes' function below.
     *
     * The function accepts INTEGER_ARRAY q as parameter.
     */

    public static void minimumBribes(List<int> q)
    {
        int queueLength = q.Count;
        int totalBribeCounter = 0;
        int tmpSwap1;

        for (int currentIndex = queueLength - 1; currentIndex > 0; currentIndex--)
        {
            if ( q[currentIndex] != currentIndex + 1)
            {
                // Lookahead only two places up front, since there is
                // a limit of two bribes per person
                if (q[currentIndex - 1] == currentIndex + 1)
                {
                    totalBribeCounter += 1;
                    tmpSwap1 = q[currentIndex - 1];
                    q[currentIndex - 1] = q[currentIndex];
                    q[currentIndex] = tmpSwap1;
                }
                else if (q[currentIndex - 2] == currentIndex + 1)
                {
                    totalBribeCounter += 2;
                    tmpSwap1 = q[currentIndex - 2];
                    q[currentIndex - 2] = q[currentIndex - 1];
                    q[currentIndex - 1] = q[currentIndex];
                    q[currentIndex] = tmpSwap1;
                }
                else
                {
                    Console.WriteLine("Too chaotic");
                    return;
                }
            }

            
        }

        Console.WriteLine(totalBribeCounter);
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> q = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(qTemp => Convert.ToInt32(qTemp)).ToList();

            Result.minimumBribes(q);
        }
    }
}
