using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    public static int nonDivisibleSubset(int k, List<int> s)
    {
        /*
        * Sum of two numbers will be divisible by k if and only the sum of remainders
        * of those numbers modulo k is equal to k.
        *
        * Therefore if subset S' include a number with remainder r, it cannot include
        * any numbers with remainder k - r.
        */

        /*
        * Special cases when r = k - r (mod k):
        * (1) There can be only one number with remainer r = 0.
        * (2) If k is divisible by 2, then there can be only one number with remainder r = k/2.
        */

        var frequencyTable = new int[k];
        
        int remainder;
        for (int i = 0; i < s.Count; i++)
        {
            remainder = s[i] % k;
            frequencyTable[remainder] += 1;
        }

        int maxCount = 0;

        if (frequencyTable[0] > 0)
        {
            maxCount += 1;
        }

        if (k % 2 == 0 && frequencyTable[k/2] > 0)
        {
            maxCount += 1;
        }

        /*
        * Now for each pair (r, k-r), where 1 < r < k - r, check which remainder occurs in s greater frequency,
        * and include all numbers with that remainder in subarray S'.
        */

        int lowerRemainderCount;
        int greaterRemainderCount;
        for (int r = 1; r <= (k - 1)/2 ; r++)
        {
            lowerRemainderCount   = frequencyTable[r];
            greaterRemainderCount = frequencyTable[k-r];

            maxCount += lowerRemainderCount >= greaterRemainderCount ? lowerRemainderCount : greaterRemainderCount;
        }
        
        return maxCount;

    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int k = Convert.ToInt32(firstMultipleInput[1]);

        List<int> s = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(sTemp => Convert.ToInt32(sTemp)).ToList();

        int result = Result.nonDivisibleSubset(k, s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
