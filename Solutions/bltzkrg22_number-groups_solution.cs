using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{   
    // See https://math.stackexchange.com/questions/3760340
    
    public static long sumOfGroup(int k) => (long)k * k * k;
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int k = Convert.ToInt32(Console.ReadLine().Trim());

        long answer = Result.sumOfGroup(k);

        textWriter.WriteLine(answer);

        textWriter.Flush();
        textWriter.Close();
    }
}
