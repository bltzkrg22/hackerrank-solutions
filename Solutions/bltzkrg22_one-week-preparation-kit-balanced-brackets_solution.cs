using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * I did not realize that expression consists only of brackets, so the checks
    * while scanning the characters from input could be simplified.
    */

    private static readonly HashSet<char> OpeningBrackets = new HashSet<char>() { '(', '{', '[' };
    private static readonly HashSet<char> ClosingBrackets = new HashSet<char>() { ')', '}', ']' };

    private static readonly Dictionary<char, char> ClosingToOpeningLookup = new Dictionary<char, char>
    {
        [')'] = '(', ['}'] = '{', [']'] = '[',
    };

    public static string isBalanced(string s)
    {
        var openingBracketStack = new Stack<char>();

        for (int i = 0; i < s.Length; i++)
        {
            // If the next character is an opening bracket, push it onto the stack
            if (OpeningBrackets.Contains(s[i]))
            {
                openingBracketStack.Push(s[i]);
            }
            // If the next character is an closing bracket, check if there is a matching
            // opening bracket on top of the stack.
            else if (ClosingBrackets.Contains(s[i]))
            {
                // Why on Earth can't HackerRank update the runtime to .NET Core :(
                // char previousOpeningBracket;
                // if (openingBracketStack.TryPop(out previousOpeningBracket) == false)
                // {
                //     return "NO";
                // }
                // else if (previousOpeningBracket != ClosingToOpeningLookup[s[i]])
                // {
                //     return "NO";
                // }
                // // else => do nothing


                char previousOpeningBracket;
                try
                {
                    previousOpeningBracket = openingBracketStack.Pop();
                }
                catch (System.InvalidOperationException)
                {
                    return "NO";
                    // throw;
                }

                if (previousOpeningBracket != ClosingToOpeningLookup[s[i]])
                {
                    return "NO";
                }
                // else => do nothing

            }
            // else => do nothing again
        }

        // If we reached this point, there were no unmatched closing brackets - but there *could*
        // be some unmatched opening brackets still on the stack.
        return openingBracketStack.Count == 0 ? "YES" : "NO";
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string s = Console.ReadLine();

            string result = Result.isBalanced(s);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
