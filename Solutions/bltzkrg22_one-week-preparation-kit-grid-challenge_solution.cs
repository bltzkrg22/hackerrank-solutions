using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static string gridChallenge(List<string> grid)
    {
        List<List<char>> gridAsArray = new List<List<char>>();
        for (int i = 0; i < grid.Count; i++)
        {
            gridAsArray.Add(grid[i].OrderBy(character => character).ToList());
        }

        for (int i = 0; i < gridAsArray[0].Count; i++)
        {
            var column = new List<char>();
            for (int j = 0; j < gridAsArray.Count; j++)
            {
                column.Add(gridAsArray[j][i]);
            }
            if (!IsColumnOrdered(column))
            {
                return "NO";
            }
        }

        return "YES";
    }

    private static bool IsColumnOrdered(List<char> column)
    {
        for (int i = 1; i < column.Count; i++)
        {
            if (column[i] < column[i-1])
            {
                return false;
            }
        }
        return true;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<string> grid = new List<string>();

            for (int i = 0; i < n; i++)
            {
                string gridItem = Console.ReadLine();
                grid.Add(gridItem);
            }

            string result = Result.gridChallenge(grid);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
