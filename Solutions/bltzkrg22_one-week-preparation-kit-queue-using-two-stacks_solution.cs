using System;
using System.Collections.Generic;
using System.IO;


public class QuasiStack
{
    private Stack<int> stackToEnqueueTo;
    private Stack<int> stackToDequeueFrom;

    public QuasiStack()
    {
        stackToEnqueueTo = new Stack<int>();
        stackToDequeueFrom = new Stack<int>();
    }

    private void TransferStacks()
    {
        if (stackToDequeueFrom.Count > 0)
        {
            throw new ApplicationException($"Can't transfer stacks if {nameof(stackToDequeueFrom)} is not empty!");
        }

        while (stackToEnqueueTo.Count > 0)
        {
            stackToDequeueFrom.Push(stackToEnqueueTo.Pop());
        }
    }

    // Only three public methods are required for given problem!
    public void Enqueue(int input)
    {
        stackToEnqueueTo.Push(input);
    }

    public int Dequeue()
    {
        if (stackToDequeueFrom.Count == 0)
        {
            TransferStacks();
        }
        // Exception when stackToDequeueFrom is empty, but it's a good thing
        return stackToDequeueFrom.Pop();
    }

    public int Peek()
    {
        if (stackToDequeueFrom.Count == 0)
        {
            TransferStacks();
        }
        return stackToDequeueFrom.Peek();
    }
}



class Solution 
{
    private enum OperationType { Enqueue = 1, Dequeue = 2, PeekAndPrint = 3 }

    private static void HandleOperation(string operation, QuasiStack stack)
    {
        OperationType operationType = (OperationType)Int32.Parse(operation.Substring(0, 1));

        switch (operationType)
        {
            case OperationType.Enqueue:
                int operationNumber = Int32.Parse(operation.Substring(2));
                stack.Enqueue(operationNumber);
                break;

            case OperationType.Dequeue:
                _ = stack.Dequeue();
                break;

            case OperationType.PeekAndPrint:
                Console.WriteLine($"{stack.Peek()}");
                break;

            default:
                throw new ApplicationException("Default case should be unreachable! Wrong operation id submitted.");
        }
    }


    static void Main(String[] args)
    {
        int n = Int32.Parse(Console.ReadLine());

        var operationList = new List<string>();
        for (int i = 0; i < n; i++)
        {
            operationList.Add(Console.ReadLine());
        }

        var stack = new QuasiStack();

        for (int i = 0; i < operationList.Count; i++)
        {
            HandleOperation(operationList[i], stack);
        }
    }
}
