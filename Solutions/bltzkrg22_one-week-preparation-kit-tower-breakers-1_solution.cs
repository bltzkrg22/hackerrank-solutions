using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * Note that the final height of a tower is 1 - such tower cannot be reduced any longer,
    * and may be removed out of play.
    * 
    * Special case: if the initial tower height = 1, there are no possible moves to be made,
    * and the second player wins by default.
    * 
    * -----
    * 
    * The winning strategy is to mentally link towers of (height > 1) into pairs of the same height,
    * and then copy the move made by previous player. If in the initial state such pairs
    * already exist, then the starting player loses. Otherwise, such a state can be reached
    * in a single move, and then the starting player wins.
    * 
    * Example 1:
    * There are n=4 towers of height h=6. The second player links them into pairs: (1,2) (3,4).
    * Now, if the first player reduces the height of tower 3 by 3 units, then the second player reduces the
    * height of tower 4 also by 3 units, so that each tower has a pair of the same height:
    * 
    *  [#]  [#]   a    b        | a -> removed by player 1 in the first move
    *  [#]  [#]   a    b        | b -> removed by player 2 in the next move
    *  [#]  [#]   a    b        | 
    *  [#]  [#]  [#]  [#]       | 
    *  [#]  [#]  [#]  [#]       | 
    *  [#]  [#]  [#]  [#]       | 
    *  
    * Whatever move the first player makes, the second player copies on the paired tower. Since the first player
    * can never remove two towers at once, they can *never* make the final move and they must lose.
    * 
    * Example 2:
    * There are n=5 towers of height 3. In a single move, the first player can remove whole tower 5,
    * and leave 4 towers of the same height. Now the heights are "paired", and the first player can then copy
    * the moves made by the second player. The first player wins with optimal play.
    * 
    *  [#]  [#]  [#]  [#]   a        | a -> removed by player 1 in the first move
    *  [#]  [#]  [#]  [#]   a        |
    *  [#]  [#]  [#]  [#]   a        |
    */ 


    private const int Player1Won = 1;
    private const int Player2Won = 2;
    public static int towerBreakers(int numberOfTowers, int heightOfTower)
    {
        if (heightOfTower > 1)
        {
            return numberOfTowers % 2 == 0 ? Player2Won : Player1Won;
        }
        else
        {
            return Player2Won;
        }
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int n = Convert.ToInt32(firstMultipleInput[0]);

            int m = Convert.ToInt32(firstMultipleInput[1]);

            int result = Result.towerBreakers(n, m);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
