using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
    * Consider a single swap operation. We take a single ball from a container, and put
    * a single ball back. Therefore, after each swap number of balls in a single container
    * is constant. In terms of matrix M, it means that the sum of numbers in each row is constant.
    *
    * At the same time, the number of balls of each color also stays constant. In terms of matrix M,
    * it means that the sum of numbers in each column is constant.
    *
    * The balls will be considered sorted if and only if in each row and in each column of matrix M
    * there is only one non-zero element (not necessarily on main diagonal).
    *           [A 0 0 0] 
    *           [0 0 B 0] 
    *           [0 C 0 0] 
    *           [0 0 0 D] 
    * A necessary condition for sorting the balls is: if there are n rows in matrix M, where sum of
    * elements is equal to s, then there must be exactly n columns where sum is also equal to s.
    *
    * It is also very easy to see that a naive algorithm that:
    * a) will find a yet unsorted color X,
    * b) then find a container with the number of balls the same as number of balls of color X,
    * c) and swap all balls of that color into the container,
    * will sort the balls (if previously described condition is possible).
    *
    * So to solve the problem, we only need to construct a list A with sums of numbers in each row,
    * and a second list B with sums of numbers in each column. If A is a permutation of B, then
    * a sort is possible; otherwise, it is impossible.
    */


    public static string organizingContainers(List<List<int>> container)
    {
        int matrixSize = container.Count;

        var eachColumnSum = new List<long>();
        var eachRowSum = new List<long>();

        for (int i = 0; i < matrixSize; i++)
        {
            long sum = 0;
            for (int j = 0; j < matrixSize; j++)
            {
                sum += container[i][j];
            }
            eachRowSum.Add( sum );
        }


        for (int i = 0; i < matrixSize; i++)
        {
            long sum = 0;
            for (int j = 0; j < matrixSize; j++)
            {
                sum += container[j][i];
            }
            eachColumnSum.Add( sum );
        }

        eachRowSum.Sort();
        eachColumnSum.Sort();


        bool success = true;
        for (int i = 0; i < matrixSize; i++)
        {
            if( eachRowSum[i] != eachColumnSum[i] )
            {
                success = false;
                break;
            }
        }


        return success ? "Possible" : "Impossible";
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<List<int>> container = new List<List<int>>();

            for (int i = 0; i < n; i++)
            {
                container.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(containerTemp => Convert.ToInt32(containerTemp)).ToList());
            }

            string result = Result.organizingContainers(container);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
