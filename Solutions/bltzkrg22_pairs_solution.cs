using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    // The soultion does not assume that numbers are unique, so it’s suboptimal
    public static int pairs(int k, List<int> arr)
    {
        int pairCount = 0;

        var frequencyDictionary = arr.GroupBy(value => value)
                                     .ToDictionary(grping => grping.Key, grping => grping.Count());

        // Note that the lookup is always done from the point of view of smaller number,
        // so there is no double counting!                        
        foreach (var number in frequencyDictionary.Keys)
        {
            int numberCount = frequencyDictionary[number];
            int matchedCount = GetValOrDefault(key: number + k, dict: frequencyDictionary);

            pairCount += numberCount * matchedCount;
        }

        return pairCount;
    }

    // The following code naively simulates the extension method GetValueOrDefault available in .NET 5
    private static int GetValOrDefault(Dictionary<int, int> dict, int key, int def = default(int))
    {
        if (dict.ContainsKey(key))
        {
            return dict[key];
        }
        else
        {
            return def;
        }
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int k = Convert.ToInt32(firstMultipleInput[1]);

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        int result = Result.pairs(k, arr);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
