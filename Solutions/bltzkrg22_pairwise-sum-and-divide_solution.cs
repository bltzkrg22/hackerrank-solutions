using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * Formula Floor((a + b)/(a * b)) may return a non-zero value if and only if
    * a + b >= a * b, which is equivalent to (a - 1) * (b - 1) <= 1
    * 
    * Since (a,b) are positive integers, then the condition is satisfied for
    * pairs where one number is 1, or for pairs (2,2).
    * 
    * So we only need to count how many such pairs there are, and multiply the count
    * by appropriate multiplayer:
    * 
    * Floor((1 + 1)/(1 * 1)) = 2
    * Floor((1 + x)/(1 * x)) = 1 (when x > 1)
    * Floor((2 + 2)/(2 * 2)) = 1
    */
    public static long solve(List<int> a)
    {
        int ones = a.Where(value => value == 1).Count();
        int nonOnes = a.Count - ones;
        int twos = a.Where(value => value == 2).Count();

        long answer = 0;

        answer += NOverTwo(ones) * 2; 
        answer += NOverTwo(twos);
        answer += ones * nonOnes;

        return answer;
    }


    private static long NOverTwo(long n) => n * (n-1) / 2; 
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int aCount = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> a = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(aTemp => Convert.ToInt32(aTemp)).ToList();

            long result = Result.solve(a);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
