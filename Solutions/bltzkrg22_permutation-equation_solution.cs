using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

// This may seem hard to follow, I just wanted to practice LINQ a little bit :/

class Result
{
    public static List<int> permutationEquation(List<int> p)
    {
        // First Select creates a new "dictionary", where Value = p[p[Key]]. 
        // Unfortunately, we must handle the fact that input is logically one-indexed, while
        // the underlaying data structure (List) is zero-indexed, hence (number - 1) & (index + 1)
        return p.Select((number, index) => new { Key = p[number - 1], Value = (index + 1) })
        // Then we order by the the "Key", and select only the "Value"
                              .OrderBy(pair => pair.Key)
                              .Select(pair => pair.Value)
                              .ToList();
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> p = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(pTemp => Convert.ToInt32(pTemp)).ToList();

        List<int> result = Result.permutationEquation(p);

        textWriter.WriteLine(String.Join("\n", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
