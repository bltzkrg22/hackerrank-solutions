using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;



class Result
{
    public static string ArePointsCollinear(List<(int, int)> points)
    {
        const string SolutionExists = "YES";
        const string NoSolution = "NO";
        if (points.Count < 2)
        {
            throw new ApplicationException("Too few points given.");
        }

        int verticalLineCount = points.Select(p => p.Item1).Distinct().Count();
        if (verticalLineCount == 1)
        {
            return SolutionExists;
        }

        int horizontalLineCount = points.Select(p => p.Item2).Distinct().Count();
        if (horizontalLineCount == 1)
        {
            return SolutionExists;
        }

        return NoSolution;
    }
}

class Solution
{

    static void Main(string[] args) 
    {
        int n = Convert.ToInt32(Console.ReadLine());

        var points = new List<(int, int)>();

        for (int nItr = 0; nItr < n; nItr++) {
            string[] xy = Console.ReadLine().Split(' ');

            int x = Convert.ToInt32(xy[0]);

            int y = Convert.ToInt32(xy[1]);

            points.Add((x, y));
        }

        var answer = Result.ArePointsCollinear(points);

        Console.WriteLine(answer);
    }
}
