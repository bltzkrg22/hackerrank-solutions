using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

/*
* Using atan2 directly inside the custom comparer would be more efficient than using
* complex coordinates as intermediate values  just to calculate the polar angle 
* (argument of complex number) and magnitude.
* 
* Still, the solution satisfies the time and memory limits.
*/

class Result
{
    public class ComplexWithIndex
    {
        public Complex Coords { get; set; }
        public int Index { get; set; }
    }

    public static List<List<int>> solve(List<List<int>> coordinates)
    {
        var complexCoords = new List<ComplexWithIndex>();

        for (int i = 0; i < coordinates.Count; i++)
        {
            var coords = new Complex(coordinates[i][0], coordinates[i][1]);
            complexCoords.Add( new ComplexWithIndex() { Coords = coords, Index = i } );
        }

        complexCoords.Sort(new MyComparer());

        List<int> sortedIndexes = complexCoords.Select(z => z.Index).ToList();

        var answer = new List<List<int>>();
        for (int i = 0; i < sortedIndexes.Count; i++)
        {
            answer.Add(coordinates[sortedIndexes[i]]);
        }
        return answer;
    }

    private class MyComparer : Comparer<ComplexWithIndex>
    {
        public override int Compare(ComplexWithIndex A, ComplexWithIndex B)
        {
            // First order by argument ascending
            if ( A.Coords.Phase != B.Coords.Phase)
            {
                double wrappedAPhase = A.Coords.Phase >= 0 ? A.Coords.Phase : A.Coords.Phase + 2 * Math.PI;
                double wrappedBPhase = B.Coords.Phase >= 0 ? B.Coords.Phase : B.Coords.Phase + 2 * Math.PI;
                return Math.Sign(wrappedAPhase - wrappedBPhase);
            }
            // Then order by magnitude (absolute value) ascending. No more tiebreakers
            else
            {
                return Math.Sign(A.Coords.Magnitude - B.Coords.Magnitude);
            }
        }
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<List<int>> coordinates = new List<List<int>>();

        for (int i = 0; i < n; i++)
        {
            coordinates.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(coordinatesTemp => Convert.ToInt32(coordinatesTemp)).ToList());
        }

        List<List<int>> result = Result.solve(coordinates);

        textWriter.WriteLine(String.Join("\n", result.Select(x => String.Join(" ", x))));

        textWriter.Flush();
        textWriter.Close();
    }
}
