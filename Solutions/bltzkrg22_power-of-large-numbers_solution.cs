using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

/*
* Because p=10^9+7 is a prime number, then from Fermat's Little Theorem it follows that for
* every positive integer x that is *not divisible by p* the following congruence is true:
*                       x^{p-1} = 1 (mod p)
* Therefore x^y (mod p) = x^{ y % (p-1) } (mod p).
*/

class Result
{
    private const int Modulo = 1000000007;

    public static int solve(string b, string e)
    {
        var @base = StringToIntegerModulo(b, Modulo);

        if (@base == 0)
        {
            return 0;
        }

        var exponent = StringToIntegerModulo(e, Modulo - 1);

        if (exponent == 0)
        {
            return 1;
        }

        return FastExponentiationModulo((int)@base, (int)exponent, Modulo);
    }


    private static int FastExponentiationModulo(int input, int exponent, int mod)
    {   
        long result = 1; 
        long currentPower = input;

        while (exponent > 0)
        {
            if ((exponent & 1) == 1)
            {
                result *= currentPower;
                result %= mod;
            }

            currentPower *= currentPower;
            currentPower %= mod;
            exponent >>= 1;
        }

        return (int)result;
    }

    // It would be more efficient to read in batches larger than a single digit, but
    // I just couldn't debug that approach. Single digit at a time still passes under
    // the time limits
    private static int StringToIntegerModulo(string numberAsString, int mod)
    {
        long answer = 0L;
        long tenFactor = 1L;

        // Scan the string from right to left, single digit at a time
        for (int digitIdx = numberAsString.Length - 1; digitIdx >= 0; digitIdx--)
        {
            answer += tenFactor * (int)Char.GetNumericValue(numberAsString[digitIdx]);
            answer %= mod;

            tenFactor *= 10;
            tenFactor %= mod;
        }

        return (int)answer;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            string a = firstMultipleInput[0];

            string b = firstMultipleInput[1];

            int result = Result.solve(a, b);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
