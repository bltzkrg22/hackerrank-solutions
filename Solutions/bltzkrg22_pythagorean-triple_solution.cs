using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * Let us define a triplet (x,y,z) for a pair of integers (m,n), where m > n.
    * 
    * x = m^2 - n^2
    * y = 2mn
    * z = m^2 + n^2
    * 
    * Then it is easy to check that:
    * x^2 + y^2 = z^2
    */

    // Note that from problem constraints we know that a >= 5
    public static List<long> pythagoreanTriple(int a)
    {
        /*
        * Case 1 - a is an odd number.
        * Then we choose such (m,n), that m = (a+1)/2 and n = (a-1)/2.
        * Easy to check that m^2 - n^2 = (m+n)*(m-n) = a * 1 = a
        */
        if (a % 2 == 1)
        {
            long m = (a + 1) / 2;
            long n = (a - 1) / 2;

            long b = 2 * m * n;
            long c = m * m + n * n;

            return new List<long>() { a, b, c };
        }
        /*
        * Case 2 - a is an even number.
        * Then we choose such (m,n), that m = a/2 and n = 1.
        */
        else // if (a % 2 == 0)
        {
            long m = a / 2;
            long n = 1;

            long b = m * m - n * n;
            long c = m * m + n * n;

            return new List<long>() { a, b, c };
        }
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int a = Convert.ToInt32(Console.ReadLine().Trim());

        List<long> triple = Result.pythagoreanTriple(a);

        textWriter.WriteLine(String.Join(" ", triple));

        textWriter.Flush();
        textWriter.Close();
    }
}
