using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'queensAttack' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER n
     *  2. INTEGER k
     *  3. INTEGER r_q
     *  4. INTEGER c_q
     *  5. 2D_INTEGER_ARRAY obstacles
     */

    public static int queensAttack(int n, int k, int r_q, int c_q, List<List<int>> obstacles)
    {
        /*
        * Let's create a quick lookup table whether for a given pair of coordinates a square contains an obstacle.
        * Notice that we consider the standard math order of coordinates, (x,y) or (column,row), while the
        * input to the method provides those coordinates in the opposide order, obstacle[i] = (row[i], column[i])
        */

        var obstacleTable = new HashSet<(int, int)>{};

        foreach (var obstacle in obstacles)
        {
            obstacleTable.Add( ( obstacle[1], obstacle[0] ) ); //obstacle[1] = x coordinate, i.e. column of the obstacle
        }


        /*
        * From the square with the Queen we will construct 8 rays, each in one of the directions that any chess queen can move.
        */
        var directions = new List<(int x, int y)>(){ (1,0), (1,1), (0,1), (-1,1), (-1,0), (-1,-1), (0,-1), (1,-1) };


        /*
        * Now in each direction, move one square and check if a stopper was found (obstacle or end of board). Count the number
        * of steps. 
        */

        int answer = 0;

        foreach (var direction in directions)
        {
            (int x, int y) currentSquare = ( c_q, r_q );
            (int x, int y) nextSquare = (  currentSquare.x +  direction.x, currentSquare.y +  direction.y );


            while (SquareIsStopper(nextSquare, n, obstacleTable) == false)
            {
                answer++;
                nextSquare = (  nextSquare.x +  direction.x, nextSquare.y +  direction.y );
            }
        }

        return answer;

    }


    static bool SquareIsStopper( (int x, int y) square, int n,  HashSet<(int, int)> obstacleTable )
    {
        /*
        * A square is a stopper for a ray, if one of the conditions is satisfied:
        * a) there is an obstacle on the square
        * b) the square is outside of the board
        */

        if ( square.x < 1 || square.x > n || square.y < 1 || square.y > n )
        {
            return true;
        }
        else if ( obstacleTable.Contains( square ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int k = Convert.ToInt32(firstMultipleInput[1]);

        string[] secondMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int r_q = Convert.ToInt32(secondMultipleInput[0]);

        int c_q = Convert.ToInt32(secondMultipleInput[1]);

        List<List<int>> obstacles = new List<List<int>>();

        for (int i = 0; i < k; i++)
        {
            obstacles.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(obstaclesTemp => Convert.ToInt32(obstaclesTemp)).ToList());
        }

        int result = Result.queensAttack(n, k, r_q, c_q, obstacles);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
