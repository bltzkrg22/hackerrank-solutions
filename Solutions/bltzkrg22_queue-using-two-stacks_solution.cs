using System;
using System.Collections.Generic;
using System.IO;

public class Solver
{
    /*
    * If current operation is Enqueue, we push the object to `stackToEnqueueTo`.
    * 
    * If current operation is Dequeue or Peek, we use the first object from `stackToDequeueFrom`.
    * 
    * If `stackToDequeueFrom` is empty while we need to use it, we transfer all elements from
    * `stackToEnqueueTo` - this will preserve the expected order of operations, since the oldest
    * element from `stackToEnqueueTo` will end up on top of `stackToDequeueFrom` and will be
    * processed first!
    */
    private Stack<int> stackToEnqueueTo = new Stack<int>();
    private Stack<int> stackToDequeueFrom = new Stack<int>();

    private enum OperationType { Enqueue = 1, Dequeue = 2, PeekAndPrint = 3 }

    private void TransferStacks()
    {
        if (stackToDequeueFrom.Count > 0)
        {
            throw new ApplicationException($"Can't transfer stacks if {nameof(stackToDequeueFrom)} is not empty!");
        }

        while (stackToEnqueueTo.Count > 0)
        {
            stackToDequeueFrom.Push(stackToEnqueueTo.Pop());
        }
    }

    public void HandleOperation(string operation)
    {
        OperationType operationType = (OperationType)Int32.Parse(operation.Substring(0, 1));

        switch (operationType)
        {
            case OperationType.Enqueue:
                int operationNumber = Int32.Parse(operation.Substring(2));
                stackToEnqueueTo.Push(operationNumber);
                break;

            case OperationType.Dequeue:
                if (stackToDequeueFrom.Count == 0)
                {
                    TransferStacks();
                }
                _ = stackToDequeueFrom.Pop();
                break;

            case OperationType.PeekAndPrint:
                if (stackToDequeueFrom.Count == 0)
                {
                    TransferStacks();
                }
                Console.WriteLine($"{stackToDequeueFrom.Peek()}");
                break;

            default:
                throw new ApplicationException("Default case should be unreachable! Wrong operation id submitted.");
        }
    }
}

class Solution {

    static void Main(String[] args)
    {
        int n = Int32.Parse(Console.ReadLine());

        var operationList = new List<string>();
        for (int i = 0; i < n; i++)
        {
            operationList.Add(Console.ReadLine());
        }

        var solver = new Solver();
        for (int i = 0; i < operationList.Count; i++)
        {
            solver.HandleOperation(operationList[i]);
        }

        //_ = Console.ReadKey();
    }
}
