using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    // This is not a quicksort! It uses much, much more memory than necessary.
    // Real quicksort is implemented in different excercises.
    public static List<int> quickSort(List<int> arr)
    {
        int pivot = arr[0];

        var left = new List<int>();
        var right = new List<int>();
        var equal = new List<int>();

        for (int i = 0; i < arr.Count; i++)
        {
            switch (Math.Sign(arr[i] - pivot))
            {
                case 0:
                    equal.Add(arr[i]);
                    break;
                case 1:
                    right.Add(arr[i]);
                    break;
                case -1:
                    left.Add(arr[i]);
                    break;
                default:
                    throw new ApplicationException("Default case should be unreachable.");
            }
        }

        left.AddRange(equal);
        left.AddRange(right);
        return left;
    }

    private static void SwapIndices<T>(IList<T> list, int first, int second)
    {
        T buffer;

        buffer = list[first];
        list[first] = list[second];
        list[second] = buffer;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        List<int> result = Result.quickSort(arr);

        textWriter.WriteLine(String.Join(" ", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
