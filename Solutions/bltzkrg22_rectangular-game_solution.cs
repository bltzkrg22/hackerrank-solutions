using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

public class IntegerPair
{
    public int Horz { get; private set; }
    public int Vert { get; private set; }
    
    public IntegerPair(int x, int y)
    {
        Horz = x;
        Vert = y;
    }

    public IntegerPair(IReadOnlyList<int> list)
    {
        if (list.Count != 2)
        {
            throw new ApplicationException($"Constructor of method {nameof(IntegerPair)} accepts only a list with two integers.");
        }
        Horz = list[0];
        Vert = list[1];
    }

    // Aggregator to find the smallest Horz and smallest Vert coordinate from
    // the input pairs
    public void MinPair(IntegerPair next)
    {
        this.Horz = Math.Min(this.Horz, next.Horz);
        this.Vert = Math.Min(this.Vert, next.Vert);
    }
}



class Result
{
    public static long RectangularGame(List<List<int>> steps)
    {
        // Since we want to find the smallest of values, we initialize to int.MaxValue
        IntegerPair minCoords = new IntegerPair(int.MaxValue, int.MaxValue);

        foreach (var pair in steps)
        {
            minCoords.MinPair(new IntegerPair(pair));
        }

        return (long)minCoords.Horz * minCoords.Vert;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<List<int>> steps = new List<List<int>>();

        for (int i = 0; i < n; i++)
        {
            steps.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(stepsTemp => Convert.ToInt32(stepsTemp)).ToList());
        }

        long result = Result.RectangularGame(steps);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
