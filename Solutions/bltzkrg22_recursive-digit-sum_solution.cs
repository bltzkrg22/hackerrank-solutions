using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'superDigit' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. STRING n
     *  2. INTEGER k
     */

    public static int superDigit(string n, int k)
    {
        long stringDigitSum = 0;

        // Parse string to a sum of numeric values of its characters
        foreach (var character in n)
        {
            stringDigitSum+= Convert.ToInt32(char.GetNumericValue(character));
        }

        stringDigitSum *= k;

        while (stringDigitSum > 9)
        {
            stringDigitSum = superDigitSubroutine(stringDigitSum);
        }

        return (int) stringDigitSum;
    }

    public static long superDigitSubroutine(long n)
    {
        long sumOfDigits = 0;
        long numberAtCurrentStep = n;

        while (numberAtCurrentStep > 0)
        {
            sumOfDigits += numberAtCurrentStep % 10;
            numberAtCurrentStep /= 10;
        }

        return sumOfDigits;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        string n = firstMultipleInput[0];

        int k = Convert.ToInt32(firstMultipleInput[1]);

        int result = Result.superDigit(n, k);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
