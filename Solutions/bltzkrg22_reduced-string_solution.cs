using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private const string EmptyAnswer = "Empty String";
    
    public static string SuperReducedString(string s)
    {
        var letterStack = new Stack<char>() { };

        foreach (var letter in s)
        {
            // If stack is empty, we always put the letter on the top
            if (letterStack.Count == 0)
            {
                letterStack.Push(letter);
                continue;
            }
            // If stack is not empty, we check if the current letter read from input
            // is equal to one on top of stack
            if (letter == letterStack.Peek())
            {
                _ = letterStack.Pop();
            }
            else
            {
                letterStack.Push(letter);
            }
        }

        string answer = String.Join(String.Empty, letterStack.Reverse());

        return String.IsNullOrEmpty(answer) ? EmptyAnswer : answer;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s = Console.ReadLine();

        string result = Result.SuperReducedString(s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
