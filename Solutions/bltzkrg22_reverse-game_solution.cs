using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;



/*
* If we name the greatest and smallest elements from input array by max and min
* respectively, then the final array after all reversin operations will be:
* [max, min, max - 1, min + 1, ...]
*/
class Result
{
    public static int ReverseGame(int ballCount, int ballNumber)
    {
        // First we "divide" the numbers in the upper and lower sections:
        // [min, min + 1, ..., mid - 1 | mid, mid + 1, ..., max - 1, max]
        // If ballCount is odd, the upper sections will have more numbers.

        // Because min = 0 and max = ballCount - 1, therefore:
        int mid = ballCount / 2;

        // Upper section
        if (ballNumber >= mid)
        {
            return ((ballCount - 1) - ballNumber) * 2;
        }
        // Lower section
        else
        {
            return 1 + (ballNumber * 2);
        }
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int i = 0; i < t; i++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int n = Convert.ToInt32(firstMultipleInput[0]);

            int k = Convert.ToInt32(firstMultipleInput[1]);

            Console.WriteLine(Result.ReverseGame(n, k));
        }        
    }
}
