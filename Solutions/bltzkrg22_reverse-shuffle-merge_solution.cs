using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    public static string reverseShuffleMerge(string s)
    {
        var occurencesStillInString = new SortedList<char, int>();
        var instancesStillToInsert = new SortedList<char, int>();
       
        StringBuilder answer = new StringBuilder(s.Length / 2);


        // Populate a frequency table for characters
        foreach (var character in s)
        {
            if (occurencesStillInString.ContainsKey(character))
            {
                occurencesStillInString[character] += 1;
            }
            else
            {
                occurencesStillInString[character] = 1;
            }
        }

        foreach (var item in occurencesStillInString)
        {
            instancesStillToInsert[item.Key] = item.Value / 2;
        }

        int currentIndex = s.Length - 1;
        int lastInsertionIndex = s.Length;
        int forcedInsertionIndex = -1;
        while ( answer.Length < s.Length / 2 )
        {
            // Scan string s right-to-left
            char currentChar = s[currentIndex];

            /*
            * If we already inserted a required number of currentChar into the answer,
            * then move one letter left. 
            */
            if (!instancesStillToInsert.ContainsKey(currentChar))
            {
                SafeRemoveFromSortedList(currentChar, occurencesStillInString);
                currentIndex--;
                continue;
            }
            /*
            * Is currentChar the smallest character which is left to insert?
            * If so, we willingy append it to the answer.
            */
            else if (instancesStillToInsert.IndexOfKey(currentChar) == 0)
            {
                answer.Append(currentChar);
                SafeRemoveFromSortedList(currentChar, instancesStillToInsert);
                SafeRemoveFromSortedList(currentChar, occurencesStillInString);
                lastInsertionIndex = currentIndex;
            }
            /*
            * Are we forced to insert currentChar, because the number of currentChar
            * we still have to insert is equal to the number of currentChar left in string s?
            * If so, scan the range between the last insertion and currentIndex, maybe there
            * are some smaller characters that we still may insert.
            */
            else if (occurencesStillInString[currentChar] == instancesStillToInsert[currentChar])
            {
                forcedInsertionIndex = currentIndex;

                int smallestIndex;
                char smallestChar;

                /*
                * Repeat until lastInsertionIndex = forcedInsertionIndex, mayber there is more than one ocurrence!
                * Check must be performed at least once!
                */
                bool breakoutCondition = false;
                while (lastInsertionIndex > forcedInsertionIndex && breakoutCondition == false)
                {
                    (smallestIndex, smallestChar) = FindSmallestCharInRange( s,  forcedInsertionIndex, lastInsertionIndex - 1, instancesStillToInsert);

                    /*
                    * It is possible that the smallest character in range is actually **equal** to currentChar.
                    * In that case we don't backtrack! We insert only the currentChar and move on.
                    * However, we set lastInsertionIndex equal to previous occurence of currentChar. It is possible that we will later want to reuse
                    * current occurence of currentChar, when we are backtracking somewhen in the future,
                    * from a character that was greater than currentChar!
                    */
                    if (smallestChar == currentChar)
                    {
                        answer.Append(currentChar);
                        lastInsertionIndex = smallestIndex;
                        SafeRemoveFromSortedList(currentChar, instancesStillToInsert);
                        SafeRemoveFromSortedList(currentChar, occurencesStillInString);
                        breakoutCondition = true;
                    }
                    else
                    {                   
                        answer.Append(smallestChar);
                        SafeRemoveFromSortedList(smallestChar, instancesStillToInsert);
                        lastInsertionIndex = smallestIndex;
                    }

                }

                /*
                * At this point we should have inserted character that we were forced to insert.
                * We can move on, one character to the left in string s. 
                */
            }
            /*
            * If none of the previous checks were true, then we don't want to insert currentChar, and we
            * are not currently forced to do so. Therefore we skip over this particular ocurrence of currentChar,
            * but decreasing the counter of available ocurrences.
            */
            else
            {
                SafeRemoveFromSortedList(currentChar, occurencesStillInString);
            }


            // Move one charter to left in string s
            currentIndex--;
        }

        return answer.ToString();
    }


    public static void SafeRemoveFromSortedList(char c, SortedList<char, int> freq)
    {
        freq[c] -= 1;

        if (freq[c] == 0)
        {
            freq.Remove(c);
        }
    }

    public static (int, char) FindSmallestCharInRange(string s, int firstIndex, int lastIndex, SortedList<char, int> instancesStillToInsert)
    {
        int lower = firstIndex <= lastIndex ? firstIndex : lastIndex;
        int higher = firstIndex + lastIndex - lower;

        string range = s.Substring(lower,  higher-lower+1);

        char smallestCharacter = (char)('z' + 1);
        int smallestIndex = -1;

        for (int i = range.Length - 1; i >= 0; i--)
        {
            /*
            * It is possible that the smallest character in range is a character that we can no longer append,
            * because we already used it the required number of times. We only choose characters that are still
            * in the instancesStillToInsert dictionary.
            */
            if (range[i] < smallestCharacter && instancesStillToInsert.ContainsKey(range[i]))
            {
                smallestCharacter = range[i];
                smallestIndex = firstIndex + i;
            }
        }
        
        return (smallestIndex, smallestCharacter);
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s = Console.ReadLine();

        string result = Result.reverseShuffleMerge(s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
