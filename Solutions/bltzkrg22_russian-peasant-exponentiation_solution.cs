using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public class Complex
    {
        public int RealPart { get; set; } = 0;
        public int ImagPart { get; set; } = 0;


        // Empty default constructor
        public Complex()
        {

        }

        // Parametrized constructor
        public Complex(int real, int imag)
        {
            this.RealPart = real;
            this.ImagPart = imag;
        }

        // Copy constructor
        public Complex(Complex copy)
        {
            this.RealPart = copy.RealPart;
            this.ImagPart = copy.ImagPart;
        }

        public void MultiplicationModulo(Complex b, int mod)
        {
            long newReal = ((long)this.RealPart * b.RealPart) % mod - ((long)this.ImagPart * b.ImagPart) % mod;
            newReal %= mod;

            long newImag = ((long)this.RealPart * b.ImagPart) % mod + ((long)this.ImagPart * b.RealPart) % mod;
            newImag %= mod;

            this.RealPart = (int)newReal;
            this.ImagPart = (int)newImag;
        }

        public List<int> AsList()
            => new List<int>() { this.RealPart, this.ImagPart };

    }

    private static Complex FastExponentiationModulo(Complex input, long exponent, int mod)
    {
        Complex result = new Complex(1,0); // initialize to 1 (multiplication neutral)

        Complex currentPower = new Complex(input);

        while (exponent > 0L)
        {
            if ((exponent & 1L) == 1L)
            {
                result.MultiplicationModulo(currentPower, mod);
            }

            currentPower.MultiplicationModulo(currentPower, mod);
            exponent >>= 1;
        }

        return result;
    }

    public static List<int> solve(int a, int b, long k, int m)
    {
        var result = FastExponentiationModulo(input: new Complex(a, b), exponent: k, mod: m);

        // Wrap the result to [0, m-1] range (if negative)
        if (result.RealPart < 0)
        {
            result.RealPart += m;
        }
        if (result.ImagPart < 0)
        {
            result.ImagPart += m;
        }
        
        return result.AsList();
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int a = Convert.ToInt32(firstMultipleInput[0]);

            int b = Convert.ToInt32(firstMultipleInput[1]);

            long k = Convert.ToInt64(firstMultipleInput[2]);

            int m = Convert.ToInt32(firstMultipleInput[3]);

            List<int> result = Result.solve(a, b, k, m);

            textWriter.WriteLine(String.Join(" ", result));
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
