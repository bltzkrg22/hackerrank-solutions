using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static int saveThePrisoner(int prisoners, int sweets, int start)
    {
        // We start from prisoner `start`. Each sweet with the exception of the first
        // increments the currentPrisoner index. If we wrap the index modulo number
        // of prisoners.
        int currentPrisoner = start;
        currentPrisoner += sweets - 1;
        currentPrisoner %= prisoners;
        //Finally, x modulo x == 0, and prisoner are "one-indexed".
        if (currentPrisoner == 0)
        {
            currentPrisoner = prisoners;
        }
        return currentPrisoner;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int n = Convert.ToInt32(firstMultipleInput[0]);

            int m = Convert.ToInt32(firstMultipleInput[1]);

            int s = Convert.ToInt32(firstMultipleInput[2]);

            int result = Result.saveThePrisoner(n, m, s);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
