using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static void separateNumbers(string s)
    {
        // We will try to read 1, 2, 3, ... digits from input, and then
        // check if the next digits of string s match
        for (int tryDigits = 1; tryDigits <= s.Length / 2; tryDigits++)
        {
            bool solutionStillPossible = true;
            int currentStringIndex = 0;

            // We read tryDigits number of digits from string s and parse to integer
            long firstNumber = Int64.Parse(s.Substring(currentStringIndex, tryDigits));

            // Remember which characters were already read
            currentStringIndex += DigitCount(firstNumber);

            long nextNumberExpected = firstNumber;

            // Solution exists if we stop exactly when currentStringIndex == s.Length - 1
            while (currentStringIndex < s.Length)
            {
                // If number is "beautiful", we expect previous number + 1 in string s
                nextNumberExpected++;

                // We will check if the expected number is in the string by reading
                // the next k digits, where k is the number of digits in the expected number
                int digitsCountExpected = DigitCount(nextNumberExpected);
                long nextNumberReality;

                // Entirely possible that there aren't enough digits in string s, so if we 
                // catch ArgumentOutOfRangeException exception, there is no solution
                try
                {
                    nextNumberReality = Int64.Parse(s.Substring(currentStringIndex, digitsCountExpected));
                }
                catch (ArgumentOutOfRangeException)
                {
                    solutionStillPossible = false;
                    break;
                    //throw;
                }

                if (nextNumberReality != nextNumberExpected)
                {
                    solutionStillPossible = false;
                    break;
                }

                // Remember which characters from string s we just read
                currentStringIndex += DigitCount(nextNumberReality);
            }

            if (solutionStillPossible && currentStringIndex == s.Length)
            {
                System.Console.WriteLine($"YES {firstNumber}");
                return;
            }
        }

        System.Console.WriteLine("NO");
    }

    public static int DigitCount(long n)
    {
        // Look silly, but this is the fasters method
        if (n < 10) return 1;
        if (n < 100) return 2;
        if (n < 1000) return 3;
        if (n < 10000) return 4;
        if (n < 100000) return 5;
        if (n < 1000000) return 6;
        if (n < 10000000) return 7;
        if (n < 100000000) return 8;
        if (n < 1000000000) return 9;
        if (n < 10000000000) return 10;
        if (n < 100000000000) return 11;
        if (n < 1000000000000) return 12;
        if (n < 10000000000000) return 13;
        if (n < 100000000000000) return 14;
        if (n < 1000000000000000) return 15;
        if (n < 10000000000000000) return 16;
        if (n < 100000000000000000) return 17;
        if (n < 1000000000000000000) return 18;
        return 19;
        // Int64.MaxValue = 9_223_372_036_854_775_807
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            string s = Console.ReadLine();

            Result.separateNumbers(s);
        }
    }
}
