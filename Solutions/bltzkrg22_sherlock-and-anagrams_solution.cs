using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'sherlockAndAnagrams' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING s as parameter.
     */

    public static int sherlockAndAnagrams(string s)
    {
        int anagramCounter = 0;
        var substringDict = new Dictionary<string, int>();

        for (int i = 0; i < s.Length; i++)
        {
            //This loop adds the next character every iteration for the substring
            for (int j = 0; j < s.Length - i; j++)
            {
                string sortedSubstring = SortString(s.Substring(i, j + 1));
                if (substringDict.ContainsKey(sortedSubstring))
                {
                    substringDict[sortedSubstring] += 1;
                }
                else
                {
                    substringDict[sortedSubstring] = 1;
                }
            }
        }

        foreach (var pairKeyValue in substringDict)
        {
            if (pairKeyValue.Value >= 2)
            {
                anagramCounter += pairKeyValue.Value * (pairKeyValue.Value - 1) / 2;
            }
        }

        return anagramCounter;
    }

    public static string SortString(string input)
    {
        var characterArray = input.ToCharArray();
        Array.Sort(characterArray);
        return new string(characterArray);
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            string s = Console.ReadLine();

            int result = Result.sherlockAndAnagrams(s);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
