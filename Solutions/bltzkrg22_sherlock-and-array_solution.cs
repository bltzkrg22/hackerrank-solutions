using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static string balancedSums(List<int> arr)
    {
        // https://en.wikipedia.org/wiki/Prefix_sum
        // prefixSums[0] = 0; prefixSums[3] = arr[0] + arr[1] + arr[2]; ...
        var prefixSums = new List<int>();

        prefixSums.Add(0);
        for (int i = 1; i <= arr.Count; i++)
        {
            prefixSums.Add(prefixSums[i - 1] + arr[i - 1]);
        }

        int totalSum = prefixSums[arr.Count];

        // Sum of all numbers left of some given index i in arr is prefixSums[i]:
        // arr[0] + arr[1] + arr[2] = prefixSum[3]

        // Analogously, sum of all numbers right of some given index i is
        // totalSum - prefixSums[i + 1]
        // arr[4] + arr[5] + arr[6] = totalSum - prefixSum[4]


        for (int i = 0; i < arr.Count; i++)
        {
            int leftSum = prefixSums[i];
            int rightSum = totalSum - prefixSums[i + 1];
            if (leftSum == rightSum)
            {
                return "YES";
            }
        }
        return "NO";
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int T = Convert.ToInt32(Console.ReadLine().Trim());

        for (int TItr = 0; TItr < T; TItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

            string result = Result.balancedSums(arr);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
