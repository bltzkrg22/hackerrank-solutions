using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * It is possible to prove that we always want to take either B[i], or 1;
    * it is not completely trivial though, and I won't do it here - so either prove
    * it yourself or treat it as a heuristic.
    */
    public static int cost(List<int> B)
    {
        // Nomenclature: Take means A[i] = B[i], we take the maximum possible value.
        // Skip means A[i] = 1, we "skip" B[i] and choose the minimum possible A[i] value.

        // `bestCostIfTake[i]` will hold the maximal possible value cost (see the problem description
        // for `cost` definition), if we choose to take B[i], and analogously `bestCostIfSkip[i]` will
        // hold the maximal possible value cost if we skip B[i] (i.e. choose 1).
        var bestCostIfTake = new int[B.Count];
        var bestCostIfSkip = new int[B.Count];


        // We do the first step manually to kickstart the loop. 
        bestCostIfTake[1] = Math.Max(Math.Abs(B[1] - B[0]), Math.Abs(B[1] - 1));
        bestCostIfSkip[1] = Math.Max(Math.Abs(1 - B[0]), Math.Abs(1 - 1));

        /*
        * We don't need to remember whole history! It would be sufficient to have to dynamic variables,
        * `previousBestCostIfTake` and `previousBestCostIfSkip`. We could do that if memory was ever
        * a concern.
        */

        for (int i = 2; i < B.Count; i++)
        {
            // I believe we could optimize a little here - for example taking two times in a row
            // (or skipping two times in a row) seems to happen in rare exceptions, but I don't see
            // how to implement that without overcomplicating the solution.
            bestCostIfTake[i] = Math.Max
            (
                Math.Abs( B[i] - 1 ) + bestCostIfSkip[i-1],
                Math.Abs( B[i] - B[i-1] ) + bestCostIfTake[i-1]
            );

            bestCostIfSkip[i] = Math.Max
            (
                Math.Abs( 1 - 1 ) + bestCostIfSkip[i-1],
                Math.Abs( 1 - B[i-1] ) + bestCostIfTake[i-1]
            );
        }

        return Math.Max(bestCostIfTake.Last(), bestCostIfSkip.Last());
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> B = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(BTemp => Convert.ToInt32(BTemp)).ToList();

            int result = Result.cost(B);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
