using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

/*
* Let us calculate the prime decomposition of n:
*
* n = 2^q * {p_1}^{c_1} * {p_2}^{c_2} * ... * {p_k}^{c_k}
*
* where p_i are odd primes.
* The number of even divisors of n can be calculated through following formula:
* x = q * (c_1 + 1) * (c_2 + 1) * ... * (c_k + 1)
*
* -----
*
* Explanation.
* If number d is a divisor of n, then its prime decomposition must be:
* d = 2^q' * {p_1}^{c'_1} * {p_2}^{c'_2} * ... * {p_k}^{c'_k}
* where 0 <= q' <= q, and 0 <= c'_i <= c_i for each i.
*
* To satisfy the condition that d is even, we must have q' >= 1.
* So to construct an even divisor, we multiply:
* a) factor 2: 1 time, 2 times, ..., or q times;
* b) factor p_1: 0 times, 1 time, ..., or c_1 times;
* b) factor p_2: 0 times, 1 time, ..., or c_2 times;
* ... etc.
*
* Every possible combination of (q', c'_1, ..., c'_k) gives a different result,
* because prime decomposition is unique.
* Therefore the number of even divisors is:
* x = q * (c_1 + 1) * (c_2 + 1) * ... * (c_k + 1).
*/

class Result
{
    public static int Divisors(int n)
    {
        int answer = 1;

        // If n is odd, there are zero even divisors.
        if ((n & 1) == 1)
        {
            return 0;
        }

        // The routine for factor 2 is special:
        int twoCount = 0;
        while ((n & 1) == 0)
        {
            twoCount++;
            n >>= 1;
        }
        answer *= twoCount;

        // The routine for odd factors is generalized
        OddPrimeFactorRoutine(ref n, 3, ref answer);

        // All primes greater than 3 are of form 6k-1 or 6k+1
        // Note that we don't actually need to explicitly test if candidate number
        // *is* a prime! We test all numbers in increasing order.
        // If we test some composite number x = p*q, then we already fully divided
        // by p and q, since: p, q < x.
        int k = 1;
        int lesserCandidate = 6 * k - 1;
        int greaterCandidate = 6 * k + 1;

        // If we start checking candidate numbers greater than sqrt(n), then it
        // means than n is a prime. (Note that at this point we already removed
        // all 2 and 3 factors from n)! So we stop calculations at sqrt(n), and
        // if n > 1, then the count of prime factor p_k = n is equal to c_k = 1.
        int lastCheck = IntegerSquareRoot(n);

        while (lesserCandidate <= lastCheck)
        {
            OddPrimeFactorRoutine(ref n, lesserCandidate, ref answer);
            OddPrimeFactorRoutine(ref n, greaterCandidate, ref answer);

            k++;
            lesserCandidate = 6 * k - 1;
            greaterCandidate = 6 * k + 1;
        }
        
        if (n > 1)
        {
            // answer *= 2;
            answer <<= 1;
        }

        return answer;
    }

    private static void OddPrimeFactorRoutine(ref int n, int prime, ref int answer)
    {
        int count = 0;
        while (n % prime == 0)
        {
            count++;
            n /= prime;
        }
        answer *= (count + 1);
    }
    
    // Borrowed from https://rosettacode.org/wiki/Isqrt_(integer_square_root)_of_X#C.23
    // Calculates the largest integer that is equal to or lower than square root of number
    private static int IntegerSquareRoot(int number)
    {
        int q = 1;
        int r = 0;
        int t;

        while (q <= number)
        { 
            q <<= 2; 
        }

        while (q > 1)
        {
            q >>= 2;
            t = number - r - q;
            r >>= 1;
            if (t >= 0)
            { 
                number = t;
                r += q;
            }
        }
        return r;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Int32.Parse(Console.ReadLine().Trim());

        var answer = new StringBuilder();
        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Int32.Parse(Console.ReadLine().Trim());

            int result = Result.Divisors(n);

            answer.Append(result);
            answer.Append('\n');
        }

        textWriter.Write(answer.ToString());

        textWriter.Flush();
        textWriter.Close();
    }
}
