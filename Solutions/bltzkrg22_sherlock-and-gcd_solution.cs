using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

/*
* Subset B does not exist if and only if there is a prime factor p that divides
* *all* numbers from set A.
* 
* So we calculate the prime decomposition of each number from subset A, and then
* intersect those decompositions and check if the intersection set is empty.
*/


// Class that precomputes all prime numbers in range from 1 to 10^5.
public static class Prime
{
    private const int MaxInput = 100000;
    private static List<int> GetAllPrimesLessThan(int maxPrime)
    {
        var primes = new List<int>();
        var maxSquareRoot = (int)Math.Sqrt(maxPrime);
        var eliminated = new BitArray(maxPrime + 1);

        for (int i = 2; i <= maxPrime; ++i)
        {
            if (!eliminated[i])
            {
                primes.Add(i);
                if (i <= maxSquareRoot)
                {
                    for (int j = i * i; j <= maxPrime; j += i)
                    {
                        eliminated[j] = true;
                    }
                }
            }
        }
        return primes;
    }

    public readonly static List<int> FirstPrimesList = GetAllPrimesLessThan(MaxInput);
    public readonly static HashSet<int> FirstPrimesSet = new HashSet<int>(FirstPrimesList);

    public static HashSet<int> GetPrimeFactors(int n)
    {
        var answer = new HashSet<int>();
        if (n < 2)
        {
            // If n < 2, it has no prime decomposition and we return an empty set
            return answer;
        }

        for (int i = 0; FirstPrimesList[i] * FirstPrimesList[i] <= n; i++)
        {
            if (n % FirstPrimesList[i] == 0)
            {
                while (n % FirstPrimesList[i] == 0)
                {
                    n /= FirstPrimesList[i];
                }
                answer.Add(FirstPrimesList[i]);
            }
        }

        // The FirstPrimesSet.Contains(n) check *should* be redundant, i.e. at this
        // point n should either be a prime or equal to 1. I add this to be safe.
        if (FirstPrimesSet.Contains(n))
        {
            answer.Add(n);
        }

        return answer;
    }
}


class Result
{
    private const string SolutionExists = "YES";
    private const string NoSolution = "NO";
    public static string SherlockAndGcd(List<int> inputArray)
    {
        var commonPrimeFactors = new HashSet<int>(Prime.FirstPrimesSet);

        foreach (var number in inputArray)
        {
            commonPrimeFactors.IntersectWith(Prime.GetPrimeFactors(number));
        }

        return commonPrimeFactors.Count == 0 ? SolutionExists : NoSolution;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int aCount = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> a = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(aTemp => Convert.ToInt32(aTemp)).ToList();

            string result = Result.SherlockAndGcd(a);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
