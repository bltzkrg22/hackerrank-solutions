using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    /*
    * To find the requested timestamp, we need to solve the following equation:
    * 
    * CommonArea = ( SideLength - ( FasterXCoordinate(t) - SlowerXCoordinate(t) ) )^2
    * CommonArea = ( SideLength - t * ( FasterXVelocity - SlowerXVelocity ) )^2
    * 
    * t = ( SideLength - sqrt(CommonArea) ) / ( FasterXVelocity - SlowerXVelocity )
    * 
    * Obviously, CommonArea must be in range from (0 to SideLength^2).
    */

    public static List<double> movingTiles(int sideLength, int velocity1, int velocity2, List<long> queries)
    {
        var answer = new List<double>();

        double timestamp;

        // Velocity from input is given along the [1,1] vector, or in other words, along
        // the y = x line! But in our calculations, we use only the x component.
        // Therefore we need to divide velocity by sin(45 deg) = sqrt(2)
        int fasterVelocity = Math.Max(velocity1, velocity2);
        int slowerVelocity = Math.Min(velocity1, velocity2);
        double differenceVelocity = (double)(fasterVelocity - slowerVelocity) / Math.Sqrt(2);

        foreach (var query in queries)
        {
            if (query > (long)sideLength * sideLength || query < 0)
            {
                throw new ApplicationException("No solution for given inputs.");
            }

            double commonAreaSqrt = Math.Sqrt(Convert.ToDouble(query));

            timestamp = ((double)sideLength - commonAreaSqrt) / differenceVelocity;

            answer.Add(Math.Round(timestamp, 4));
        }

        return answer;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int l = Convert.ToInt32(firstMultipleInput[0]);

        int s1 = Convert.ToInt32(firstMultipleInput[1]);

        int s2 = Convert.ToInt32(firstMultipleInput[2]);

        int queriesCount = Convert.ToInt32(Console.ReadLine().Trim());

        List<long> queries = new List<long>();

        for (int i = 0; i < queriesCount; i++)
        {
            long queriesItem = Convert.ToInt64(Console.ReadLine().Trim());
            queries.Add(queriesItem);
        }

        List<double> result = Result.movingTiles(l, s1, s2, queries);

        textWriter.WriteLine(String.Join("\n", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
