using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;
using System.Numerics;

/*
* The formula for the distinct permutations of non-distinct objects is explained here:
* https://math.libretexts.org/@go/page/13571
* Here, we want to calculate: ((ones - 1) + zeroes)!/(ones-1)!/(zeroes)! (mod 10^9+7)
*
* Additionally, dividing by some number a modulo p (where a and p are coprime)
* is equivalent to calculating *modular multiplicative inverse* of a modulo p,
* https://en.wikipedia.org/wiki/Modular_multiplicative_inverse
*/

class Result
{
    private const int Modulo = 1000000007; // 10^9+7

    private static int ModInverse(int a, int mod)
    {
        if (mod == 1) return 0;
        int m0 = mod;
        (int x, int y) = (1, 0);

        while (a > 1)
        {
            int q = a / mod;
            (a, mod) = (mod, a % mod);
            (x, y) = (y, x - q * y);
        }
        return x < 0 ? x + m0 : x;
    }

    private static int FactorialMod(int n, int mod)
    {
        long answer = 1;
        for (int i = 1; i <= n; i++)
        {
            answer *= i;
            answer %= mod;
        }
        return (int)answer;
    }

    public static int SherlockAndPermutations(int zeroes, int ones)
    {
        int nominator = FactorialMod(zeroes + (ones - 1), Modulo);
        int denominatorZeroes = FactorialMod(zeroes, Modulo);
        int denominatorOnes = FactorialMod(ones - 1, Modulo);

        int denominatorZeroesInverse = ModInverse(denominatorZeroes, Modulo);
        int denominatorOnesInverse = ModInverse(denominatorOnes, Modulo);

        var answerAsBiginteger = new BigInteger(nominator);
        answerAsBiginteger *= denominatorZeroesInverse;
        answerAsBiginteger %= Modulo;
        answerAsBiginteger *= denominatorOnesInverse;
        answerAsBiginteger %= Modulo;

        return (int)answerAsBiginteger;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int n = Convert.ToInt32(firstMultipleInput[0]);

            int m = Convert.ToInt32(firstMultipleInput[1]);

            int result = Result.SherlockAndPermutations(n, m);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
