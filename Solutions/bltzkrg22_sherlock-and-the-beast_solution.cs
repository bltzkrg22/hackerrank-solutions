using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;
using System.Numerics;

class Result
{
    private static readonly HashSet<int> noSolution = new HashSet<int>() { 1, 2, 4, 7 };
    public static void decentNumber(int n)
    {
        // It is impossible to represent 1, 2, 4, 7 as a sum 3 * a + 5 * b, where (a,b)
        // are non-negative integers. Therefore we print -1
        if (noSolution.Contains(n))
        {
            Console.WriteLine(-1);
            return;
        }

        // threes = count of digit three in the solution number
        int threes;
        int fives;

        /*
        * We want to maximize the solution, so the count of threes must be minimal.
        * We take the smallest possible multiple of 5 (because count of threes is per
        * requirements a multiple of 5), so that the count of remaining digits is divisible
        * by 3.
        */
        switch (n % 3)
        {
            case 0:
                threes = 0;
                break;
            case 1:
                threes = 10;
                break;
            default:
                threes = 5;
                break;
        }

        fives = n - threes;


        /*
        * To maximize the answer, we push all digits three to the right (as least significant),
        * and digits five to the left (most significant), e.g.: 5 5 ... 5 3 3 ... 3
        */

        string answer = DecimalRepDigit(digit: 5, count: fives) + DecimalRepDigit(digit: 3, count: threes);

        Console.WriteLine(answer);
    }


    private static string DecimalRepDigit(int digit, int count)
    {
        var answer = new StringBuilder();
        for (int i = 1; i <= count; i++)
        {
            answer.Append(digit);
        }
        return answer.ToString();
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            Result.decentNumber(n);
        }
    }
}
