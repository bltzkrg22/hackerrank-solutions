using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'isValid' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING s as parameter.
     */

    public static string isValid(string s)
    {
        if(s.Length <= 3)
        {
            return "YES";
        }

        var frequencyDictionary = new Dictionary<char, int>();

        foreach (var character in s)
        {
            if (frequencyDictionary.ContainsKey(character))
            {
                frequencyDictionary[character] += 1;
            }
            else
            {
                frequencyDictionary[character] = 1;
            }
        }

        var frequencies = frequencyDictionary.Values.ToList();

        var test = frequencies.GroupBy(l => l)
                   .Select(g => new { Freq = g.Key, Count = g.Select(l => l).Count() } )
                   .OrderByDescending(x => x.Count)
                   .Take(3).ToList();

        int mostCommonFrequency = test[0].Freq;

        if (test.Count >= 3)
        {
            return "NO";
        }
        else if( test.Count == 2 && test[1].Count > 1 )
        {
            return "NO";
        }
        else if( test.Count == 2 && test[1].Count == 1 && test[1].Freq - mostCommonFrequency > 1 )
        {
            return "NO";
        }
        else
        {
            return "YES";
        }
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s = Console.ReadLine();

        string result = Result.isValid(s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
