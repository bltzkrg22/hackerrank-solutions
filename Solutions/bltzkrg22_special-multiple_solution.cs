using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

using System.Numerics;

/*
* As the maximal count of queries is 10000, and maximal single query number is 500,
* I will precompute the `Lookup` table for every number from 1 to 500, and use it 
* to return the answer.
*/

class Result
{
    private const int MaxQueryValue = 500;
    
    #region Precomputing
    
    private static Dictionary<int, BigInteger> Lookup;
    private static bool _isLookupPrecomputed = false;
    private static void PrecomputeLookup()
    {
        if(_isLookupPrecomputed)
        {
            return;
        }
        
        Lookup = new Dictionary<int, BigInteger>();
        int alreadyFoundCount = 0;
        int index = 1;
        
        while (alreadyFoundCount < MaxQueryValue)
        {
            BigInteger nextSpecial = SpecialAtIndex(index);
            List<int> divisors = Divisors(nextSpecial);
            foreach (var divisor in divisors)
            {
                if (!Lookup.ContainsKey(divisor))
                {
                    alreadyFoundCount++;
                    Lookup[divisor] = nextSpecial;
                }
            }
            index++;
        }
        
        _isLookupPrecomputed = true;
    }
    
    #endregion Precomputing
    
    
    /*
    * Method `SpecialAtIndex` calculates n-th smallest integer "made up of one or more
    * occurences of 9 and zero or more occurences of 0", so for example:
    * SpecialAtIndex(1) = 9
    * SpecialAtIndex(2) = 90
    * SpecialAtIndex(3) = 99 etc.
    */
    private static BigInteger SpecialAtIndex(int index)
    {
        var answer = new BigInteger();
        var tenFactor = new BigInteger(1);

        while (index > 0)
        {
            if ((index & 1) == 1)
            {
                answer += tenFactor * 9;
            }
            index >>= 1;
            tenFactor *= 10;
        }
        return answer;
    }

    // Returns a list of all numbers in range from 1 to MaxQueryValue (by default 500)
    // which are divisors of the input parameter
    private static List<int> Divisors(BigInteger number)
    {
        return Enumerable.Range(1, MaxQueryValue)
                         .Where(divisor => number % divisor == 0)
                         .ToList();
    }
    
    public static string SpecialMultiple(int n)
    {
        // flag _isLookupPrecomputed guards that we precompute only once
        PrecomputeLookup();
        return Lookup[n].ToString();
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            string result = Result.SpecialMultiple(n);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
