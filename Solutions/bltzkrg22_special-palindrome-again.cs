using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution {

    // Complete the substrCount function below.
    static long substrCount(int n, string s) {

// Every singular letter is a valid special substring
        long specialCounter = s.Length;

        // Check two-letter strings. i will be the index of the first letter
        for (int i = 0; i < s.Length - 1; i++)
        {
            if (s[i] == s[i+1])
            {
                char special = s[i];
                specialCounter++;
                bool stopLookahead = false;

                // Now lookahead one letter, and lookbehind one letter. Check, if new string with two more
                // characters is still special. Continue looking ahead and behing until string is not special
                // anymore or array boundary is found.
                int lookaheadIndices = 1;
                while(!stopLookahead)
                {
                    try
                    {
                        if (s[i - lookaheadIndices] != special || s[i+1+lookaheadIndices] != special)
                        {
                            stopLookahead = true;
                        }
                        else
                        {
                            specialCounter++;
                            lookaheadIndices++;
                        }
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        stopLookahead = true;
                        //throw;
                    }
                }

            }
        }

        // Now perform a similar check, but for three-character strings. This time i will be
        // the middle index (so the one which is not checked for equality)!
        for (int i = 1; i < s.Length - 1; i++)
        {
            if (s[i-1] == s[i+1])
            {
                char special = s[i-1];
                specialCounter++;
                bool stopLookahead = false;

                // Now lookahead one letter, and lookbehind one letter. Check, if new string with two more
                // characters is still special. Continue looking ahead and behing until string is not special
                // anymore or array boundary is found.
                int lookaheadIndices = 1;
                while(!stopLookahead)
                {
                    try
                    {
                        if (s[i - lookaheadIndices - 1] != special || s[i+1+lookaheadIndices] != special)
                        {
                            stopLookahead = true;
                        }
                        else
                        {
                            specialCounter++;
                            lookaheadIndices++;
                        }
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        stopLookahead = true;
                        //throw;
                    }
                }

            }
        }
        return specialCounter;
    }

    static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine());

        string s = Console.ReadLine();

        long result = substrCount(n, s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
