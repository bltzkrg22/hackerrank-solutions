using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private const int spikeBase = 3;
    private const int spikeMultiplyer = 2;
    public static long strangeCounter(long t)
    {
        long spikeValue = spikeBase;
        long spikeTime = 1;

        (long nextSpikeValue, long nextSpikeTime) = FullSpike(spikeValue, spikeTime);

        while (t >= nextSpikeTime)
        {
            spikeValue = nextSpikeValue;
            spikeTime = nextSpikeTime;
            (nextSpikeValue, nextSpikeTime) = FullSpike(spikeValue, spikeTime);
        }

        return spikeValue - (t - spikeTime);
    }

    private static (long nextSpike, long nextTime) FullSpike(long previousSpike, long previousTime)
        => (previousSpike * spikeMultiplyer, previousTime + previousSpike);

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        long t = Convert.ToInt64(Console.ReadLine().Trim());

        long result = Result.strangeCounter(t);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
