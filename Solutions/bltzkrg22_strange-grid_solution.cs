using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    // The default signature from template is int strangeGrid,
    // while the expected answer for some test cases is larger than int.MaxValue :/
    public static long strangeGrid(long r, long c)
    {
        // We will calculate the answer through calculating the offsets
        // from the bottom left square - so for clarity I will explicitly
        // define it, even if it's equal to 0
        long firstNumber = 0;
                
        // Every square right, the number is 20 greater
        long columnOffset = (c - 1) * 2;

        // Every two squares up, the number is 10 greater
        long doubleRowOffset = ((r - 1) / 2) * 10;

        // Number in odd row is one greater then number that
        // lies one square lower in the grid
        long singleRowOffset = (r - 1) % 2;


        return firstNumber + columnOffset + doubleRowOffset + singleRowOffset;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        long r = Convert.ToInt32(firstMultipleInput[0]);

        long c = Convert.ToInt32(firstMultipleInput[1]);

        long result = Result.strangeGrid(r, c);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
