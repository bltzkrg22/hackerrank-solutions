using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

/*
* We will "append" characters always one by one.
* (1) If a character X was not previously appended, we will need to pay 1 unit.
* (2) If a character X *was* already appended before, it is a substring of p after all.
* There is no limit on the second type of operation.
*
* So the total cost of copying a string will be the number of unique characters in
* the initial string.
*/

class Result
{
    public static int stringConstruction(string s) => s.Distinct().Count();
    // alternative
    // public static int stringConstruction(string s) => s.ToHashSet().Count;
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            string s = Console.ReadLine();

            int result = Result.stringConstruction(s);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
