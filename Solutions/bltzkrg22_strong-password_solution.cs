using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private static readonly List<string> patterns = new List<string>()
    {
        @"[0-9]{1}", // numbers
        @"[a-z]{1}", // lower_case
        @"[A-Z]{1}", // upper_case
        @"[!@#$%\^&*()\-+]{1}" // special_characters
        // note that `\-` is an escaped `-` character!
    };

    private const int MinimalPasswordLength = 6;

    public static int minimumNumber(int n, string password)
    {
        int initialLength = password.Length;
        int invalidRuleCount = 0;

        
        foreach (var rule in patterns)
        {
            Regex pattern = new Regex(rule);

            if (!pattern.IsMatch(password))
            {
                invalidRuleCount++;
            }
        }

        // If a rule is not satisfied, then it will be after adding at most
        // one character. Also rules check for different characters, so they
        // are independent of each other.
        int lengthAfterSatifyingRules = initialLength + invalidRuleCount;

        // If after adding all required characters the password is still not long
        // enough, then we need to add even more characters.
        return lengthAfterSatifyingRules >= MinimalPasswordLength ?
            invalidRuleCount : MinimalPasswordLength - initialLength;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        string password = Console.ReadLine();

        int answer = Result.minimumNumber(n, password);

        textWriter.WriteLine(answer);

        textWriter.Flush();
        textWriter.Close();
    }
}
