using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static long sumXor(long n)
    {
        /*
        * Observe the following property of addition and bitwise XOR:
        * a) 0 + 0 = 0                     b) 0 XOR 0 = 0
        *    0 + 1 = 1                        0 XOR 1 = 1
        *    1 + 1 = 10 (2 in base 10)        1 XOR 1 = 0
        *
        * If we add (1) two zeroes or (2) a zero and a one, then the result of addition and bitwise XOR is the same. If we add two ones,
        * that the result addition is greater than the result of bitwise XOR.
        *
        * Second observation is: the result of both addition and bitwise XOR will be the same, if we calculate the result for each bit
        * separately, and then just add the result for each bit; examples:
        * 010 + 011 = (000 + 000) + (10 + 10) + (0 + 1) = 000 + 100 + 1 = 101
        * 010 XOR 011 = (000 XOR 000) + (10 XOR 10) + (0 XOR 1) = 000 + 00 + 1 = 001
        * 
        * If we combine both observations, we can see that if even once numbers n and x both have ones in the same index, then n + x will be
        * greater than n XOR x, or in other words - it is impossible that n + x == n XOR x.
        *
        * At the same time, if x has a zero bit at all positions where n has a one, then all other bits can have any value.
        * Therefore, if number n has z zeros (remember than x < n, so leading zeros are invalid!) in its binaty representation, then
        * there will be 2^z such numbers x that fulfill the condition n + x = n XOR x.
        *
        * Example: n = 10101011
        *          x = 0z0z0z00
        * where each z can be replaced either by 0 or 1, and still n + x = n XOR x. There are three zeros in n, so there are 2^3 = 8 possibilities.
        */

        return Convert.ToInt64(Math.Pow(2, CountZeroes(n)));

    }

    private static int CountZeroes(long n)
    {
        int counter = 0;
        while (n > 0)
        {
            if(n % 2 == 0)
            {
                counter++;
            }

            n >>= 1;
        }

        return counter;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        long n = Convert.ToInt64(Console.ReadLine().Trim());

        long result = Result.sumXor(n);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
