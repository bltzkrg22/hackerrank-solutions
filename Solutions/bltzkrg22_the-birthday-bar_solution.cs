using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static int birthday(List<int> list, int expectedSum, int sliceLength)
    {
        int counter = 0;

        // Sliding window aproach.
        int windowSum = list.Take(sliceLength).Sum();
        CheckWindowSum(windowSum, expectedSum, ref counter);

        // If we start the window with length `sliceLength` at index
        // `list.Count - sliceLength`, then it will end on the last list index.
        for (int startIndex = 1; startIndex <= list.Count - sliceLength; startIndex++)
        {
            int outNumber = list[startIndex - 1];
            int inNumber = list[startIndex + (sliceLength) - 1];

            windowSum = windowSum + inNumber - outNumber;
            CheckWindowSum(windowSum, expectedSum, ref counter);
        }

        return counter;
    }

    private static void CheckWindowSum(int windowSum, int expectedSum, ref int counter)
    {
        if (windowSum == expectedSum)
        {
            counter++;
        }
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> s = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(sTemp => Convert.ToInt32(sTemp)).ToList();

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int d = Convert.ToInt32(firstMultipleInput[0]);

        int m = Convert.ToInt32(firstMultipleInput[1]);

        int result = Result.birthday(s, d, m);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
