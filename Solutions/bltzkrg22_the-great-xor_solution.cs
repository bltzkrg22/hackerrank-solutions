using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static long theGreatXor(long x)
    {
        /*
        * Algorithm that will construct all numbers a that satisfy the conditions.
        *
        * 1) Scan x from left, i.e. from the most significat bit.
        * 2) If current bit of x is a one, that a must have a zero bit on this position.
        * 3) If current bit of x is a zero, then ANY number a that has a one on this position satisfies condition x XOR a > x.
        *    Example: x = 1010 => first zero at position 2^2
        *             a = 01zz => each number from list: [0111, 0110, 0101, 0100] satisfy the condition, there are 2^2 such numbers
        * 4) Repeat the loop; each time a zero is encountered in x at position 2^i, then we can construct 2^i new numbers a.
        *    So for x = 1010 possible values of a are: [0111, 0110, 0101, 0100, 0001]
        *
        * Answer = Sum of 2^i for all i, where x has a zero bit on position 2^i.
        */
        
        long counter = 0;
        long currentDigit = 1;
        while (x > 1)
        {
            if (x % 2 == 0)
            {
                counter += currentDigit;
            }
            x >>= 1;
            currentDigit <<= 1;
        }

        return counter;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++)
        {
            long x = Convert.ToInt64(Console.ReadLine().Trim());

            long result = Result.theGreatXor(x);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
