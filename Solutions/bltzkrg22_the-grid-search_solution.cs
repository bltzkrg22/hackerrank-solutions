using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    public static string gridSearch(List<string> G, List<string> P)
    {
        /*
        * We create the pattern for regular expression with a lookahead, to catch all
        * overlapping matches. For example, when looking for pattern ABA in string ABABABA,
        * by default we obtain only non-overlapping instances:
        * (ABA)BABA, at index 0,
        * ABAB(ABA), at index 4.
        * With lookahead, so with pattern A(?=BA) instead of ABA, we can capture overlapping
        * groups as well:
        * (ABA)BABA, at index 0,
        * AB(ABA)BA, at index 2,
        * ABAB(ABA), at index 4.
        */

        string pattern;
        int patternLength = P[0].Length;

        if (P[0].Length > 1)
        {
            string firstLetter = P[0].Substring(0,1);
            string otherLetters = P[0].Substring(1);

            pattern = firstLetter + @"(?=" + otherLetters + @")";
        }
        // If the pattern includes only a single character, than it makes no sense to use an empty lookahead.
        else
        {
            pattern = P[0].Substring(0,1);
        }
        


        int gridColumns = G.Count;
        int patternColumns = P.Count;
        /*
        * As long as number of rows left in grid is high enough, so that all pattern rows may still
        * fit, check the current grid row for the first patter row.
        */
        for (int i = 0; i <=  (gridColumns - patternColumns); i++)
        {
            /*
            * We are using regular expressions to easily get *all* matches, in case the first line of
            * the pattern is found more than once in current grid row.
            */
            MatchCollection matches = Regex.Matches(G[i], pattern);

            /*
            * If first row of pattern is found, then check grid lines directly against the rest of
            * the pattern. The outer foreach loop iterates over every instance of first pattern line
            * found in current grid line. The inner for loop iterates over the rest of the pattern rows.
            * `break` stops the inner for loop on first mismatch.
            *
            * If no mismatch is found, then whole pattern was found in grid. Since it is enough to find
            * one instance of pattern in the grid, we may return success and finish the function call.
            */
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    int startIndex = match.Index;
                    bool success = true;

                    for (int j = 1; j < patternColumns; j++)
                    {
                        if ( G[i+j].Substring(startIndex, patternLength) != P[j] )
                        {
                            success = false;
                            break;
                        }
                    }

                    if (success == true)
                    {
                        return "YES";
                    }
                }    
            }
        }

        /*
        * If method reaches this point, we iterated over whole grid and pattern was not found.
        */
        return "NO";
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int R = Convert.ToInt32(firstMultipleInput[0]);

            int C = Convert.ToInt32(firstMultipleInput[1]);

            List<string> G = new List<string>();

            for (int i = 0; i < R; i++)
            {
                string GItem = Console.ReadLine();
                G.Add(GItem);
            }

            string[] secondMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

            int r = Convert.ToInt32(secondMultipleInput[0]);

            int c = Convert.ToInt32(secondMultipleInput[1]);

            List<string> P = new List<string>();

            for (int i = 0; i < r; i++)
            {
                string PItem = Console.ReadLine();
                P.Add(PItem);
            }

            string result = Result.gridSearch(G, P);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
