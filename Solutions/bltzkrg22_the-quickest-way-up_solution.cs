using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    private static Dictionary<int, int> LaddersAndSnakes = new Dictionary<int, int>();

    private static readonly List<int> DiceRoll = new List<int>(){1, 2, 3, 4, 5, 6};

    private const int StartSquare = 1;
    private const int EndSquare = 100;

    public static int quickestWayUp(List<List<int>> ladders, List<List<int>> snakes)
    {
        /*
        * From input constraints we know that a square can only be a beginning of a single ladder
        * or a single snake. Since we will be calling the *static* method multiple times, we need to actually
        * clear this dictionary first!
        */

        LaddersAndSnakes = new Dictionary<int, int>();

        foreach (var startend in ladders)
        {
            LaddersAndSnakes[ startend[0] ] = startend[1];
        }
        foreach (var startend in snakes)
        {
            LaddersAndSnakes[ startend[0] ] = startend[1];
        }

        /*
        * There are 100 squares on the board, numbered from 1 to 100. We will create an array stepsRequired, which
        * at index i will hold a minimal number of dice rolls it takes to reach it from the first square.
        *
        * If a square is the beginning or ending of a ladder/snake, than it is "unreachable", i.e. the square can
        * actually never be reached!
        */

        var stepsRequired = new int[EndSquare + 1];

        /*
        * While we could track visitedSquares by checking which elements in stepsRequired have uninitialized value,
        * having a separated hashset is easier to understand.
        */

        var visitedSquares = new HashSet<int>();
        var currentTurnSquares = new HashSet<int>(){ StartSquare }; // We start from square StartSquare == 1.
        var nextTurnSquares = new HashSet<int>();

        stepsRequired[0] = -1; // We can never reach square 0.
        stepsRequired[StartSquare] = 0; // We "reach" StartSquare in 0 dice rolls.

        while (currentTurnSquares.Count > 0)
        {
            /*
            * Clear the hashset at the beginning of each loop. This particular loop enumerates over all squares
            * that can be reach in n dice rolls, and finds squares that can be reached in n+1 dice rolls.
            * This could be made faster and more clever if we considered whole range of dice rolls at once instead
            * of trying to roll each value separately, but this works too.
            */
            nextTurnSquares = new HashSet<int>();

            foreach (var square in currentTurnSquares)
            {
                visitedSquares.Add(square);

                // From current square try to roll each value and check where it leads to.
                foreach (var roll in DiceRoll)
                {
                    // GetNextSquare checks for any snakes and ladders and returns the actual end square
                    var nextSquare = GetNextSquare(square + roll);

                    /*
                    * If square was already visited, then we have found shorter path somewhere in the past.
                    * Don't evaluate this square further. Also if square is in currentTurnSquares hashset,
                    * then it can be reached in the same number of dice rolls.
                    */
                    if (visitedSquares.Contains(nextSquare) || currentTurnSquares.Contains(nextSquare))
                    {
                        continue;
                    }
                    nextTurnSquares.Add(nextSquare);
                    stepsRequired[nextSquare] = stepsRequired[square] + 1;
                    if (nextSquare == EndSquare)
                    {
                        return stepsRequired[EndSquare];
                    }
                }
            }

            currentTurnSquares = nextTurnSquares;
        }


        /*
        * If there are no more points to evaluate, we got into an infinite loop and we can't reach the end square.
        * In that case we return -1.
        */
        return -1;
    }

    private static int GetNextSquare( int initially )
        => LaddersAndSnakes.ContainsKey(initially) ? LaddersAndSnakes[initially] : initially;

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<List<int>> ladders = new List<List<int>>();

            for (int i = 0; i < n; i++)
            {
                ladders.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(laddersTemp => Convert.ToInt32(laddersTemp)).ToList());
            }

            int m = Convert.ToInt32(Console.ReadLine().Trim());

            List<List<int>> snakes = new List<List<int>>();

            for (int i = 0; i < m; i++)
            {
                snakes.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(snakesTemp => Convert.ToInt32(snakesTemp)).ToList());
            }

            int result = Result.quickestWayUp(ladders, snakes);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
