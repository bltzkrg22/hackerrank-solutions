using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution {

    // Complete the triplets function below.
    static long triplets(int[] a, int[] b, int[] c) {

        long tripletCounter = 0;

        var aSet = new List<int>(a.Distinct());
        var bSet = new List<int>(b.Distinct());
        var cSet = new List<int>(c.Distinct());

        aSet.Sort();
        bSet.Sort();
        cSet.Sort();

        int currentAindex = 0;
        int currentCindex = 0;

        for (int currentBindex = 0; currentBindex < bSet.Count; currentBindex++)
        {
            /*
            * Find the smallest index in sorted set aSet, where aSet[currentAindex] > bSet[currentAindex].
            * aSet will then contain currentAindex numbers smaller than bSet[currentAindex]
            * Then we do the same for cSet.
            *
            * Remember that we don't want to get out of aSet and cSet ranges!
            */

            while (currentAindex < aSet.Count && aSet[currentAindex] <= bSet[currentBindex])
            {
                currentAindex++;
            }
            while (currentCindex < cSet.Count && cSet[currentCindex] <= bSet[currentBindex])
            {
                currentCindex++;
            }

            /*
            * Number of triplets that inlcude bSet[currentBindex] as the middle number (p, bSet[currentBindex], r)
            * is equal to currentAindex * currentCindex, i.e. (elements in aSet smaler than bSet[currentBindex]) *
            * (elements in cSet smaler than bSet[currentBindex])
            */

            tripletCounter += (long)currentAindex * (long)currentCindex;
        }

        return tripletCounter;
    }

    static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] lenaLenbLenc = Console.ReadLine().Split(' ');

        int lena = Convert.ToInt32(lenaLenbLenc[0]);

        int lenb = Convert.ToInt32(lenaLenbLenc[1]);

        int lenc = Convert.ToInt32(lenaLenbLenc[2]);

        int[] arra = Array.ConvertAll(Console.ReadLine().Split(' '), arraTemp => Convert.ToInt32(arraTemp))
        ;

        int[] arrb = Array.ConvertAll(Console.ReadLine().Split(' '), arrbTemp => Convert.ToInt32(arrbTemp))
        ;

        int[] arrc = Array.ConvertAll(Console.ReadLine().Split(' '), arrcTemp => Convert.ToInt32(arrcTemp))
        ;
        long ans = triplets(arra, arrb, arrc);

        textWriter.WriteLine(ans);

        textWriter.Flush();
        textWriter.Close();
    }
}
