using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    // Simple binary search, as per problem constraints `arr` is sorted
    public static int introTutorial(int V, List<int> arr)
    {
        int minIndex = 0;
        int maxIndex = arr.Count - 1;
        int midIndex = (maxIndex + minIndex) / 2;

        while (maxIndex != minIndex)
        {
            switch (Math.Sign(V - arr[midIndex]))
            {
                case 1:
                    // V is larger than arr[midIndex], we continue looking in the right segment
                    minIndex = midIndex + 1;
                    midIndex = (maxIndex + minIndex) / 2;
                    break;
                case -1:
                    // V is smaller than arr[midIndex], we keep looking in the left segment
                    maxIndex = midIndex - 1;
                    midIndex = (maxIndex + minIndex) / 2;
                    break;
                case 0:
                    return midIndex;
                    // break;
                default:
                    throw new ApplicationException("Default case is supposed to be unreachable.");
            }
        }
        
        // If we reached this point, then maxIndex == minIndex. Either the last check
        // will succeed, or the number is not present in the array.

        if (V == arr[maxIndex])
        {
            return maxIndex;
        }
        else
        {
            throw new ApplicationException("Number is no present in provided array.");
        }
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int V = Convert.ToInt32(Console.ReadLine().Trim());

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        int result = Result.introTutorial(V, arr);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
