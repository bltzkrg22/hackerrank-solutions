using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    // List of all possible characters
    private static readonly List<char> fullSet = new List<char>
        ( Enumerable.Range('a', 26).Select(x => (char)x) );

    // List of all possible character pairs (c, d), where c < d
    private static readonly List<(char, char)> letterPairs =
        ( fullSet ).SelectMany(first =>
            ( fullSet ).Select(second => (first, second)))
                .Where(pair => pair.Item1 < pair.Item2).ToList();

    // Remove all characters from a string other than specified in `pair`
    private static StringBuilder FilterOtherLetters(string input, (char first, char second) pair)
    {
        var inputAsStringBuilder = new StringBuilder(input);

        foreach (var character in fullSet)
        {
            if (character != pair.first && character != pair.second)
            {
                inputAsStringBuilder.Replace(character.ToString(), String.Empty);
            }
        }

        return inputAsStringBuilder;
    }

    // Input is valid, if it consists of *two alternating characters*, like: xyxyxyx or xyxyxy; 
    // but not: xxxxxx or x
    private static bool IsInputValid(StringBuilder input)
    {
        if (input.Length < 2)
        {
            return false;
        }

        char firstLetter = input[0];
        char secondLetter = input[1];

        if (firstLetter == secondLetter)
        {
            return false;
        }

        for (int i = 2; i < input.Length; i += 2)
        {
            if (input[i] != firstLetter)
            {
                return false;
            }
        }

        for (int i = 3; i < input.Length; i += 2)
        {
            if (input[i] != secondLetter)
            {
                return false;
            }
        }

        return true;
    }
    
    public static int alternate(string s)
    {
        int maxLength = 0;

        foreach (var pair in letterPairs)
        {
            var filteredInput = FilterOtherLetters(s, pair);
            if (IsInputValid(filteredInput))
            {
                maxLength = Math.Max(maxLength, filteredInput.Length);
            }
        }

        return maxLength;
    }
}


class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int l = Convert.ToInt32(Console.ReadLine().Trim());

        string s = Console.ReadLine();

        int result = Result.alternate(s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
