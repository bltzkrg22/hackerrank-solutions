using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    static List<(int, int)> directions = new List<(int, int)>(){ (1,0), (0,1), (-1,0), (0,-1) };
    public static int twoPluses(List<string> grid)
    {
        int numberOfRows = grid.Count;
        int numberOfCols = grid[0].Length;

        /*
        * Step 1. Find all possible crosses. 
        */

        var possibleCrosses = new List<((int, int), int)>(){}; // ((center.x, center.y), area)

        for (int y = 0; y < numberOfRows; y++)
        {
            for (int x = 0; x < numberOfCols; x++)
            {
                if (GetBitAtXyCoordinates(x,y,grid) == true)
                {
                    possibleCrosses.Add( ( (x,y), 1 ) );

                    int checkSize = 2;
                    bool canGrow = true;
                    while (canGrow)
                    {
                        foreach (var direction in directions)
                        {
                            try
                            {
                                canGrow &= GetBitAtXyCoordinates(x + (checkSize - 1) * direction.Item1, y + (checkSize - 1) * direction.Item2, grid);
                            }
                            catch (Exception ex) when (ex is IndexOutOfRangeException || ex is ArgumentOutOfRangeException)
                            {
                                canGrow = false;
                                break;
                                //throw;
                            }
                        }

                        if (canGrow)
                        {
                            int area = 1 + 4 * (checkSize - 1);
                            possibleCrosses.Add( ( (x,y), area ) );
                            checkSize++;
                        }
                    }
                }
            }
        }

        /*
        * (Step 2) Precompute all possible products of areas.
        */

        var areaProduct = new List<(int, int, int)>(){}; // (area, index i, index j)

        for (int i = 0; i < possibleCrosses.Count; i++)
        {
            for (int j = i+1; j < possibleCrosses.Count; j++)
            {   
                // If crosses have the same origin then there is no point is storing the product
                // Older versions of .NET unfortunately don't allow for easy tuple comparison, so we need to check each cordinate separately

                // possibleCrosses[i].Item1 = (origin.x, origin.y), so possibleCrosses[i].Item1.Item1 = origin.x, etc.
                // possibleCrosses[i].Item2 = area
                if (possibleCrosses[i].Item1.Item1 != possibleCrosses[j].Item1.Item1 || possibleCrosses[i].Item1.Item2 != possibleCrosses[j].Item1.Item2)
                {
                    areaProduct.Add( (possibleCrosses[i].Item2 * possibleCrosses[j].Item2, i, j));
                }
            }
        }

        areaProduct.Sort(new InvertedComparer());

        /*
        * (Step 3) Find the first product from the sorted list where there's no overlap.
        */

        for (int i = 0; i < areaProduct.Count; i++)
        {
            if (!IsThereOverlap(areaProduct[i].Item2,areaProduct[i].Item3,possibleCrosses))
            {
                return areaProduct[i].Item1;
            }
        }

        return -1; 
    }

    internal static bool GetBitAtXyCoordinates(int x, int y, List<string> grid) => grid[y][x] == 'G' ? true : false;

    internal static bool IsThereOverlap(int crossAIndex, int crossBIndex, List<((int, int), int)> possibleCrosses)
    {
        int crossARadius = (possibleCrosses[crossAIndex].Item2 - 1) / 4;
        int crossBRadius = (possibleCrosses[crossBIndex].Item2 - 1) / 4;

        int crossAOriginX = possibleCrosses[crossAIndex].Item1.Item1;
        int crossAOriginY = possibleCrosses[crossAIndex].Item1.Item2;
        int crossBOriginX = possibleCrosses[crossBIndex].Item1.Item1;
        int crossBOriginY = possibleCrosses[crossBIndex].Item1.Item2;

        int distanceX = Math.Abs(crossAOriginX - crossBOriginX);
        int distanceY = Math.Abs(crossAOriginY - crossBOriginY);

        int greaterDistance = Math.Max(distanceX, distanceY);
        int smallerDistance = Math.Min(distanceX, distanceY);

        int greaterRadius = Math.Max(crossARadius, crossBRadius);
        int smallerRadius = Math.Min(crossARadius, crossBRadius);

        // If the origins do not lie in the same row or the same column
        if (smallerDistance > 0)
        {
            return (greaterRadius >= greaterDistance && smallerRadius >= smallerDistance) ? true : false;
        }
        // If the origins lie on the same row or the same column
        else
        {
            return (greaterRadius + smallerRadius > greaterDistance - 1) ? true : false;
        }
    }

    private class InvertedComparer : IComparer<(int, int, int)> 
    {
        public int Compare((int, int, int) x, (int, int, int) y)
        {
            return (y.Item1).CompareTo(x.Item1);
        }
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int n = Convert.ToInt32(firstMultipleInput[0]);

        int m = Convert.ToInt32(firstMultipleInput[1]);

        List<string> grid = new List<string>();

        for (int i = 0; i < n; i++)
        {
            string gridItem = Console.ReadLine();
            grid.Add(gridItem);
        }

        int result = Result.twoPluses(grid);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
