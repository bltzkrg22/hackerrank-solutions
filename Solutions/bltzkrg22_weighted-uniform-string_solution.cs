using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{
    public static List<string> weightedUniformStrings(string s, List<int> queries)
    {
        // We initialize lastCharacter to a random char that cannot be seen in string s
        char lastCharacter = 'X'; 

        // Ucsw = "Uniform Continuous Substring Weight" - see problem description
        var setOfAllUcsw = new HashSet<int>();
        var potentialUcsw = 0;

        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == lastCharacter)
            {
                potentialUcsw += CharToAsciiValue(s[i]);
                setOfAllUcsw.Add(potentialUcsw);
            }
            else
            {
                lastCharacter = s[i];
                potentialUcsw = CharToAsciiValue(s[i]);
                setOfAllUcsw.Add(potentialUcsw);
            }
        }


        var answer = new List<string>();

        foreach (var query in queries)
        {
            if (setOfAllUcsw.Contains(query))
            {
                answer.Add("Yes");
            }
            else
            {
                answer.Add("No");
            }
        }

        return answer;
    }

    private static int CharToAsciiValue(char c) => c - 'a' + 1;
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s = Console.ReadLine();

        int queriesCount = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> queries = new List<int>();

        for (int i = 0; i < queriesCount; i++)
        {
            int queriesItem = Convert.ToInt32(Console.ReadLine().Trim());
            queries.Add(queriesItem);
        }

        List<string> result = Result.weightedUniformStrings(s, queries);

        textWriter.WriteLine(String.Join("\n", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
