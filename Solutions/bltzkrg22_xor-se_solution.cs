using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution 
{

static long xorSequence(long l, long r)
    {
        /*
        * Let's construct something similar to a prefix sum array, S[n], which at n-th element includes a XOR of
        * all A[i] from 1 to n, example:
        * S[5] = A[1] XOR A[2] XOR A[3] XOR A[4] XOR A[5]
        *
        * Because XOR is:
        * (1) commutative,
        * (2) associative,
        * (3) B XOR B is always equal to 0,
        * (4) and B XOR 0 is always equal to B,
        * then it's easy to see that:
        * S[r] XOR S[l-1] = (A[1] XOR A[2] XOR ... XOR A[r]) XOR (A[1] XOR A[2] XOR ... XOR A[l-1]) =
        *                 = (A[1] XOR A[1]) XOR ... XOR (A[l-1] XOR A[l-1]) XOR A[l] XOR A[l+1] XOR ... XOR A[r] =
        *                 = 0 XOR ... XOR 0 XOR A[l] XOR A[l+1] XOR ... XOR A[r] = 
        *                 = A[l] XOR A[l+1] XOR ... XOR A[r]
        * i.e. S[r] XOR S[l-1] is equal to the answer we are looking for.
        *
        * Let's write down some first elements of A[n] and S[n]
        *
        * index:  0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 ...
        *     A:  0  1  3  0  4  1  7  0  8  1  11  0 12  1 15  0 16  1 19  0 20  1 23  0 24  1 27 ...
        *     S:  0  1  2  2  6  7  0  0  8  9   2  2 14 15  0  0 16 17  2  2 22 23  0  0 24 25  2 ...
        *
        * A[n] follows the formula that is described in method GetA(), and S[n] follows the formula described in GetS().
        * Both are mathematically provable, but I won't do that here.
        */

        return GetS(r) ^ GetS(l-1);
    }

    private static long GetA(long i)
    {
        var remainder = i % 4;

        switch (remainder)
        {
            case 0:
                return i;
            case 1:
                return 1;
            case 2:
                return i+1;
            case 3:
                return 0;
            default:
                throw new ApplicationException($"The default case in method {nameof(GetA)} should never be reachable.");
        }
    }

    private static long GetS(long i)
    {
        var remainder = i % 8;

        switch (remainder)
        {
            case 0:
                return i;
            case 1:
                return i;
            case 2:
                return 2;
            case 3:
                return 2;
            case 4:
                return i+2;
            case 5:
                return i+2;
            case 6:
                return 0;
            case 7:
                return 0;
            default:
                throw new ApplicationException($"The default case in method {nameof(GetS)} should never be reachable.");
        }
    }

    static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine());

        for (int qItr = 0; qItr < q; qItr++) {
            string[] lr = Console.ReadLine().Split(' ');

            long l = Convert.ToInt64(lr[0]);

            long r = Convert.ToInt64(lr[1]);

            long result = xorSequence(l, r);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
